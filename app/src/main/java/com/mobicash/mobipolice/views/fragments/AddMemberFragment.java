package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.MemberAdapter;
import com.mobicash.mobipolice.model.dataaccess.adapters.SearchMemberAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.MemberModel;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.MemberPresenter;
import com.mobicash.mobipolice.presenter.UpdateMembersPresenter;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddMemberFragment extends BaseFragment implements
        UpdateMembersPresenter.IUpdateMemberListener, MemberPresenter.IGetMemberListener {

    private static final String TAG = "AddMemberFragment";

    @BindView(R.id.parentadd)
    LinearLayout addMemeber;
    @BindView(R.id.addm)
    ImageButton add;
    @BindView(R.id.memberslist)
    RecyclerView rvMembers;
    @BindView(R.id.autoComplete_add_member)
    AutoCompleteTextView mACAddMember;

    ArrayList<MemberModel> list;
    MemberAdapter adapter;

    private UpdateMembersPresenter mUpdateMemberPresenter;

    public AddMemberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_member, container, false);
    }


    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);

        mUpdateMemberPresenter = new UpdateMembersPresenter(getActivity(), this);
        initiateViews();
        setAutoCompleteAdapter();

    }

    @Override
    public String getTagFragment() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void memberUpdated() {

    }

    private void setAutoCompleteAdapter() {
        mACAddMember.setThreshold(2);
        final SearchMemberAdapter adapter = new SearchMemberAdapter(getActivity());
        mACAddMember.setAdapter(adapter); // 'this' is Activity instance
        mACAddMember.setOnItemClickListener((adapterView, view, position, id) -> {
            addMember(adapter.getItem(position).getId());
            mACAddMember.setText("");
        });
    }

    public void addMember(int id) {
        boolean isToAdd = true;
        if (SessionClass.getInstance().getOldMembers() != null) {
            for (MemberModel member : SessionClass.getInstance().getOldMembers()) {
                if (member.getId() == id) {
                    isToAdd = false;
                    ToastUtility.showToastForLongTime(getContext(), getString
                            (R.string.message_already_added_member));
                }

            }
        }
        if (isToAdd) {
            mUpdateMemberPresenter.addMembers(id);
            mUpdateMemberPresenter.updateMembers();
        }
    }

    public void removeMember(int id) {
        mUpdateMemberPresenter.removeMembers(id);
        mUpdateMemberPresenter.updateMembers();
    }


    private void initiateViews() {
        addMemeber.setVisibility(View.GONE);
        rvMembers.setLayoutManager(new LinearLayoutManager(getContext()));
        list = new ArrayList<>();

        new MemberPresenter(getActivity(),this).getMembers();
    }




    @Override
    public void onMemberFetched(ArrayList<MemberModel> members) {
        list.clear();
        list.addAll(members);

        if (list.size() == 0) {
            addMemeber.setVisibility(View.VISIBLE);
            rvMembers.setVisibility(View.GONE);
        } else {
            adapter = new MemberAdapter(list, AddMemberFragment.this, getActivity());
            rvMembers.setAdapter(adapter);
            SessionClass.getInstance().setOldMembers(list);
            addMemeber.setVisibility(View.GONE);
            rvMembers.setVisibility(View.VISIBLE);
        }
    }
}
