package com.mobicash.mobipolice.views.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IMessageReceived;
import com.mobicash.mobipolice.model.essentials.App;
import com.mobicash.mobipolice.model.essentials.BeaconReferenceApplication;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.LogUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.CrimeTipPresenter;
import com.mobicash.mobipolice.presenter.services.FileUploadService;
import com.mobicash.mobipolice.views.dialogs.GenericDialog;
import com.mobicash.mobipolice.views.dialogs.IDialogDouble;
import com.mobicash.mobipolice.views.fragments.BaseFragment;

import java.util.Locale;

import butterknife.ButterKnife;

import static com.mobicash.mobipolice.presenter.services.FileUploadServiceKt.ACTION_FILE_UPLOAD;
import static com.mobicash.mobipolice.presenter.services.FileUploadServiceKt.KEY_IDENTIFIER;
import static com.mobicash.mobipolice.presenter.services.FileUploadServiceKt.KEY_PROGRESS;
import static com.mobicash.mobipolice.presenter.services.FileUploadServiceKt.KEY_SERVER_PATH;

public class BaseActivity extends AppCompatActivity implements IMessageReceived {

    public static final String ACTION_MESSAGE_RECEIVED = "action_got_message";
    public static final String KEY_USER_ID = "userid";
    public static final String KEY_MESSAGE_TYPE = "key_message_type";
    public static final String CROWD_POLICE_BEACON_UUID = "ffffffff1234aaaa1a2ba1b2c3d4e5f6";

    @IntDef({MESSAGE_TYPE.GROUP,MESSAGE_TYPE.ONE_TO_ONE})
    public @interface MESSAGE_TYPE{
        int ONE_TO_ONE = 0xf01;
        int GROUP = 0xf02;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(ACTION_MESSAGE_RECEIVED));
        LocalBroadcastManager.getInstance(this).registerReceiver(uploadListener,
                new IntentFilter(ACTION_FILE_UPLOAD));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(uploadListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }


    public BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                int userId = extras.getInt(KEY_USER_ID);
                @MESSAGE_TYPE
                int type = extras.getInt(KEY_MESSAGE_TYPE);
                switch (type) {
                    case MESSAGE_TYPE.GROUP:
                        onGroupMessageReceived(userId);
                        break;
                    case MESSAGE_TYPE.ONE_TO_ONE:
                        onMessageReceived(userId);
                        break;
                }
            }

        }
    };

    public BroadcastReceiver uploadListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                int progress = extras.getInt(KEY_PROGRESS);
                String filePath = extras.getString(KEY_IDENTIFIER);
                if(progress==100){
                    String serverPath = extras.getString(KEY_SERVER_PATH);
                    new CrimeTipPresenter(BaseActivity.this,null).
                            updateCrimeOnUpload(serverPath,filePath);
                }

            }
        }
    };

    public void performUploadAction(int progress,String serverPath,String localPath){

    }



    @Override
    public void onMessageReceived(@Nullable Integer userId) {
        BaseFragment currentFragment = FragmentUtility.getCurrentFragment(this, R.id.fl_fragmentContainer);
        if (currentFragment instanceof IMessageReceived) {
            ((IMessageReceived) currentFragment).onMessageReceived(userId);
        }
    }

    @Override
    public void onGroupMessageReceived(@Nullable Integer groupId) {
        BaseFragment currentFragment = FragmentUtility.getCurrentFragment(this, R.id.fl_fragmentContainer);
        if (currentFragment instanceof IMessageReceived) {
            ((IMessageReceived) currentFragment).onGroupMessageReceived(groupId);
        }
    }

    @Override
    public void onBackPressed() {

        if (this instanceof MainActivity&&getSupportFragmentManager().getBackStackEntryCount() == 0) {
            showExitDialog();
        }else{
            super.onBackPressed();
        }
    }

    public void showExitDialog(){
        new GenericDialog(this, getString(R.string.ph_warning), getString(R.string.message_exit_application)
                , getString(R.string.ph_ok),getString( R.string.cancel),
                new IDialogDouble() {
            @Override
            public void onRightClick(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }

            @Override
            public void onLeftClick(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
                finishAffinity();
            }
        }).setLogo(R.drawable.dialog_warning).show();
    }

    public void logToDisplay(final String line) {
        LogUtility.debugLog("range logging",line);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BeaconReferenceApplication) this.getApplicationContext()).setMonitoringActivity(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((BeaconReferenceApplication) this.getApplicationContext()).setMonitoringActivity(null);
    }

}
