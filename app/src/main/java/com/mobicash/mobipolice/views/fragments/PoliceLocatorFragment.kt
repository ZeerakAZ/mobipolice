package com.mobicash.mobipolice.views.fragments


import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.mobicash.mobipolice.R
import com.mobicash.mobipolice.model.dataaccess.entities.PoliceLocationModel
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated
import com.mobicash.mobipolice.model.utilities.MapUtility
import com.mobicash.mobipolice.presenter.PoliceLocationPresenter
import com.mobicash.mobipolice.views.dialogs.PoliceOfficerDetailDialog
import kotlinx.android.synthetic.main.fragment_police_locator.*
import java.util.*

class PoliceLocatorFragment : BaseFragment(),
        PoliceLocationPresenter.ILocationFetchListener, ILocationUpdated, OnMapReadyCallback {


    companion object {
        private const val TAG = "PoliceLocator"
    }

    private var mLocatorPresenter: PoliceLocationPresenter? = null
    private var mMap: GoogleMap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_police_locator, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mLocatorPresenter = PoliceLocationPresenter(context!!,this)
        MapUtility.loadMapFragment(this, this)
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
        mMap?.setOnMarkerClickListener {marker ->  policeOfficerSelected(marker) }
        mLocatorPresenter?.getLocations(24.91757,67.072036)
    }

    private fun policeOfficerSelected(marker: Marker): Boolean {

        (marker.tag as? PoliceLocationModel)?.let {
            PoliceOfficerDetailDialog(activity!!,it).show()
            return true
        }
        return false
    }


    override fun getTagFragment(): String {
        return TAG
    }

    override fun getTitle(): String {
        return getString(R.string.title_traffic_mapping)
    }

    override fun onLocationFetched(officersLocations: ArrayList<PoliceLocationModel>) {
        pb_wait.visibility = View.GONE
        val builder = LatLngBounds.Builder()
        for (location in officersLocations){
            val marker =MapUtility.addMarker(mMap, LatLng(location.lat,
                    location.lng),R.drawable.marker_police,location)
            marker.tag = location
            builder.include(marker.position)
        }

        val bounds = builder.build()
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, 70)
        mMap?.animateCamera(cu)
    }

    override fun onUpdate(location: Location?) {
    }
}
