package com.mobicash.mobipolice.views.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonModel;
import com.mobicash.mobipolice.model.dataaccess.entities.PlaceObject;
import com.mobicash.mobipolice.model.dataaccess.validations.EmptyValidation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validator;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.DateUtility;
import com.mobicash.mobipolice.model.utilities.ImageUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.MissingPersonPresenter;
import com.mobicash.mobipolice.views.activities.LocationActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddPersonFragment extends BaseFragment implements MissingPersonPresenter.IMissingPersonPostListener{
    public static final int CODE_CAMERA_PIC = 0xf00;
    public static final int CODE_IMAGE_PIC = 0xf01;
    public static final String TAG = "add_person_frag";

    private final int REQUEST_PLACE = 0xf02;
    private PlaceObject mPlace;

    @BindView(R.id.et_person_name)
    AppCompatEditText mEtPersonName;
    @BindView(R.id.et_person_desc)
    AppCompatEditText mEtPersonDesc;
    @BindView(R.id.et_missing_date)
    AppCompatEditText mEtMissingDate;
    @BindView(R.id.et_missing_time)
    AppCompatEditText mEtMissingTime;
    @BindView(R.id.et_missing_location)
    AppCompatEditText mEtMissingLocation;
    @BindView(R.id.et_contact_name)
    AppCompatEditText mEtContactName;
    @BindView(R.id.et_contact_number)
    AppCompatEditText mEtContactNumber;
    @BindView(R.id.et_contact_address)
    AppCompatEditText mEtContactAddress;
    @BindView(R.id.iv_person_image)
    ImageView mIvMissingPersonImage;

    private ArrayList<Validation> mValidations;

    private ImageUtility mImageHelper;
    private boolean mImageSet;



    public AddPersonFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_person, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setValidations();
        mImageHelper = new ImageUtility(this, mIvMissingPersonImage, CODE_CAMERA_PIC, CODE_IMAGE_PIC);

    }

    private void setValidations() {
        mValidations = new ArrayList<>();
        mValidations.add(new EmptyValidation(getString(R.string.error_missing_person_name), mEtPersonName));
        mValidations.add(new EmptyValidation(getString(R.string.error_missing_person_desc), mEtPersonDesc));
        mValidations.add(new EmptyValidation(getString(R.string.error_missing_person_date), mEtMissingDate));
        mValidations.add(new EmptyValidation(getString(R.string.error_missing_person_time), mEtMissingTime));
        mValidations.add(new EmptyValidation(getString(R.string.error_missing_person_location), mEtMissingLocation));
        mValidations.add(new EmptyValidation(getString(R.string.error_contact_person_name), mEtContactName));
        mValidations.add(new EmptyValidation(getString(R.string.error_contact_person_number), mEtContactNumber));
        mValidations.add(new EmptyValidation(getString(R.string.error_contact_person_address), mEtContactAddress));
    }

    @OnClick(R.id.et_missing_date)
    public void showDateDialog(){
        AppUtility.showDatePickerDialog(getActivity().
                getFragmentManager(),mEtMissingDate,getContext());
    }

    @OnClick(R.id.et_missing_time)
    public void showTimeDialog(){
        AppUtility.showTimePickerDialog(getContext(),mEtMissingTime);
    }

    @OnClick(R.id.et_missing_location)
    public void showLocationUi(){
        Intent i = new Intent(getActivity(), LocationActivity.class);
        startActivityForResult(i, REQUEST_PLACE);
    }

    @OnClick(R.id.iv_person_image)
    public void showImageDialog(){
        mImageHelper.checkCameraAndExternalStoragePermissions();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CODE_CAMERA_PIC:
                    mImageSet = true;
                    mImageHelper.setImageFromCameraResult(data);
                    break;
                case CODE_IMAGE_PIC:
                    mImageSet = true;
                    mImageHelper.convertAndSetImageToUI(data);
                    break;
                case REQUEST_PLACE:
                    mPlace = data.getExtras().getParcelable(LocationActivity.PLACE_KEY);
                    if (mPlace != null) {
                        mEtMissingLocation.setText(mPlace.getAddress());
                    }
                    break;
            }
        }
    }

    private boolean validateFields() {
        if (!mImageSet) {
            showToast(getString(R.string.error_no_missing_person_image));
            return false;
        }
        return new Validator(getContext()).validates(mValidations);
    }

    @OnClick(R.id.btn_submit)
    public void reportPerson() {

        if (validateFields()) {
            MissingPersonModel missingPersonModel = new MissingPersonModel(getText(mEtPersonName), mImageHelper.getImageBase64(),
                    getText(mEtPersonDesc), DateUtility.getServerPostTime(getText(mEtMissingDate) + " " + getText(mEtMissingTime))
                    , mPlace.getAddress(), mPlace.getLat() + "", mPlace.getLng() + "", getText(mEtContactNumber),
                    getText(mEtContactName), getText(mEtContactAddress), SessionClass.getInstance().getUserId(getContext()));
            new MissingPersonPresenter(getActivity(),this).postMissingPerson(missingPersonModel);
        }
    }

    @Override
    public void onMissingPersonPost() {
        if(getActivity()!=null){
            ToastUtility.showToastForLongTime(getContext(),getString(R.string.message_missing_person_posted));
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private String getText(EditText editText){
        return editText.getText().toString();
    }


    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
