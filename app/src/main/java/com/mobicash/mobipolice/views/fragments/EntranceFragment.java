package com.mobicash.mobipolice.views.fragments;


import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.VideoPagerAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.CountryUserModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.ActivityNavigationUtility;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.SpannableHelper;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.ActiveUserPresenter;
import com.mobicash.mobipolice.views.activities.JoinActivity;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.mobicash.mobipolice.views.customviews.CustomSlider;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EntranceFragment extends BaseFragment  {


    private static final int REQUEST_SIGNUP = 0xf01;
    private static final String TAG = "entranceFrag";

    @BindView(R.id.ll_btn_join)
    LinearLayout llJoin;
    @BindView(R.id.ll_login)
    LinearLayout mLlLogin;
    @BindView(R.id.btn_sponsor)
    Button mBtnSponsor;
    @BindView(R.id.btn_share)
    Button btnShare;
    @BindView(R.id.tv_share_message)
    TextView mTvShareMesssage;
    @BindView(R.id.sv_ads)
    CustomSlider mSvAds;

    private Location mLocation;
    private VideoPagerAdapter mVideoPagerAdapter;


    public EntranceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_entrance, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        updateButtons();
        setAds();
    }

    public void updateButtons() {
        llJoin.setVisibility(SessionClass.getInstance().isUserLogin(getContext()) ?
                View.GONE : View.VISIBLE);
        mLlLogin.setVisibility(SessionClass.getInstance().isUserLogin(getContext()) ?
                View.VISIBLE : View.GONE);
        getActiveUsers();
    }



    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    private void setAds() {
        if (mVideoPagerAdapter == null)
            mVideoPagerAdapter = new VideoPagerAdapter(getChildFragmentManager());
        mSvAds.setAdapter(mVideoPagerAdapter);
    }


    @OnClick({R.id.btn_sponsor, R.id.btn_share, R.id.tv_website,
            R.id.tv_privacy_policy, R.id.btn_login, R.id.iv_sos,
            R.id.btn_join, R.id.iv_emergency,R.id.iv_ice,R.id.iv_safe})
    public void onOptionSelected(View view) {
        FragmentUtility utility = FragmentUtility.withManager(getFragmentManager())
                .addToBackStack(TAG)
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM);
        boolean userLogin = SessionClass.getInstance().isUserLogin(getContext());

        switch (view.getId()) {
            case R.id.btn_sponsor:
                //utility.addFragment(new SponsorFragment());
                break;
            case R.id.btn_share:
                AppUtility.shareText(getContext(), getString(R.string.message_share));
                break;
            case R.id.btn_login:
                utility.replaceToFragment(new LoginFragment());
                break;
            case R.id.btn_join:
                ActivityNavigationUtility.navigateWith(getActivity())
                        .navigateTo(JoinActivity.class);
               // ToastUtility.showToastForLongTime(getContext(), getString(R.string.message_development));
                break;
            case R.id.tv_website:
                AppUtility.launchUrl(getActivity(), "https://glacial-dawn-45140.herokuapp.com/");
                break;
            case R.id.tv_privacy_policy:
                utility.addFragment(new PrivacyPolicyFragment());
                break;
            case R.id.iv_sos:
                if (userLogin)
                    utility.addFragment(SosFragment.newInstance(SosFragment.TYPE_SOS));
                else
                    AppUtility.showLoginDialog(getContext());
                break;
            case R.id.iv_emergency:
                if (userLogin)
                    utility.addFragment(EmergencyFragment.newInstance());
                else
                    AppUtility.showLoginDialog(getContext());
                break;
            case R.id.iv_safe:
                if (userLogin)
                    utility.addFragment(SosFragment.newInstance(SosFragment.TYPE_SAFE));
                else
                    AppUtility.showLoginDialog(getContext());
                break;
            case R.id.iv_ice:
                if (userLogin)
                    utility.addFragment(new IceFragment());
                else
                    AppUtility.showLoginDialog(getContext());
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mVideoPagerAdapter != null)
            mSvAds.startSlider();
    }

    @Override
    public void onPause() {
        super.onPause();
        mSvAds.stopSlider();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_SIGNUP:
                     FragmentUtility.withManager(getFragmentManager())
                            .addToBackStack(TAG)
                            .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM).
                             addFragment(new LoginFragment());
                    break;
            }
        }
    }

    private void getActiveUsers() {
        new ActiveUserPresenter(getActivity(),
                (ActiveUserPresenter.IActiveUserFetchListener) (users, count) -> {
            setShareMessage(count);
        }).getActiveUser();
    }

    private void setShareMessage(int memberCount) {
        if (getContext() == null)
            return;
        mTvShareMesssage.setVisibility(View.VISIBLE);
        String joinMessage = getString(R.string.message_join, memberCount);
        SpannableHelper spannableHelper = new SpannableHelper(joinMessage, 17, 17 + String.valueOf(memberCount).length())
                .makeBold()
                .changeTextColor(ContextCompat.getColor(getContext(), R.color.blue_color_app))
                .underline()
                .setClickSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        FragmentUtility.withManager(getFragmentManager())
                                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                                .addToBackStack(TAG)
                                .addFragment(new UserListFragment());
                    }
                });
        mTvShareMesssage.setMovementMethod(LinkMovementMethod.getInstance());
        mTvShareMesssage.setText(spannableHelper.getStyledString());
    }


    public void stopSlider() {
        mSvAds.stopSlider();
    }
}
