package com.mobicash.mobipolice.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.widget.AppCompatEditText;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioGroup;


import com.mobicash.mobipolice.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Zeera on 7/27/2017 bt ${File}
 */

public class ReasonDialog extends Dialog {
    @BindView(R.id.btn_ok)
    ImageButton mBtnOk;
    @BindView(R.id.btn_close)
    ImageButton mBtnClose;
    @BindView(R.id.rg_reasons)
    RadioGroup mRgReasons;
    @BindView(R.id.et_reason)
    AppCompatEditText mEtOtherReason;
    private IReasonDialog mListener;

    public ReasonDialog(@NonNull Context context) {
        super(context);
    }

    public ReasonDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    public ReasonDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void setmListener(IReasonDialog mListener) {
        this.mListener = mListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_reason);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ButterKnife.bind(this, getWindow().getDecorView());
        }
    }
    @OnClick(R.id.btn_ok)
     void onOk(){
        if(mListener!=null){
            String reason = mEtOtherReason.getText().toString();
            if(reason.trim().isEmpty()){
                reason = ((Button) findViewById(mRgReasons.getCheckedRadioButtonId())).getText().toString();
            }
            mListener.onOk(reason);
        }
        dismiss();
    }

    @OnClick(R.id.btn_close)
     void onClose(){
        dismiss();
    }

    public interface IReasonDialog{
        void onOk(String reason);
    }
}
