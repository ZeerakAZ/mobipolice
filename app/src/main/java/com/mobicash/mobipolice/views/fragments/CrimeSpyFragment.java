package com.mobicash.mobipolice.views.fragments;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTypeModel;
import com.mobicash.mobipolice.model.dataaccess.entities.PlaceObject;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.dataaccess.validations.EmptyValidation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validator;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.CrimeTipPresenter;
import com.mobicash.mobipolice.presenter.UploadVideoPresenter;
import com.mobicash.mobipolice.presenter.services.FileUploadService;
import com.mobicash.mobipolice.views.activities.LocationActivity;
import com.mobicash.mobipolice.views.activities.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrimeSpyFragment extends BaseFragment implements
        ILocationUpdated, CrimeTipPresenter.ICrimeTypeListener, CrimeTipPresenter.ICrimeUpdateListener {

    private static final String TAG ="crime_spy";

    @BindView(R.id.spinner_ct_categories)
    Spinner mSpCategories;
    @BindView(R.id.et_ct_crime_info)
    EditText mEtCrimeInfo;
    @BindView(R.id.tv_ct_crime_address)
    TextView mtvCrimeAddress;
    @BindView(R.id.et_ct_suspect_desc)
    EditText mEtSuspectDesc;
    @BindView(R.id.et_ct_witness)
    EditText mEtWitnessWhere;
    @BindView(R.id.et_ct_vehicle)
    EditText mEtVehicle;
    @BindView(R.id.et_ct_crime_desc)
    EditText mEtCrimeDesc;
    @BindView(R.id.audio)
    ImageButton audioButton;
    @BindView(R.id.video)
    ImageButton videoButton;
    @BindView(R.id.audiolink)
    TextView audioLink;
    @BindView(R.id.videolink)
    TextView videolink;


    private final int REQUEST_PLACE = 100;


    public static final int DIALOG_AUDIO = 0xF01;
    public static final int DIALOG_VIDEO = 0xF02;
    public static int mOpenDialog;

    private CrimeModel mCrimeModel;
    private UploadVideoPresenter mUploadPresenter;
    private CrimeTipPresenter mCrimeTipPresenter;

    private FileUploadService mUploadingService;
    private boolean mBound;

    public CrimeSpyFragment() {
    }

    public static CrimeSpyFragment newInstance() {

        Bundle args = new Bundle();

        CrimeSpyFragment fragment = new CrimeSpyFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_spy, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mCrimeModel = new CrimeModel();
        mCrimeTipPresenter = new CrimeTipPresenter(getActivity(), this);
        mUploadPresenter = new UploadVideoPresenter(getActivity(), this, this);

        getLocation();
        showDialog();

        mCrimeTipPresenter.getCrimesTypes();
        getContext().bindService(new Intent(getContext(),FileUploadService.class),
                mConnection,Context.BIND_AUTO_CREATE);
    }


    @Override
    public void onCrimeTypesFetched(ArrayList<CrimeTypeModel> categories) {
        if (getActivity() == null)
            return;
        ArrayAdapter<CrimeTypeModel> adapter = new ArrayAdapter<>(getActivity(), R.layout.row_spinner,
                R.id.text1, categories);
        adapter.setDropDownViewResource(R.layout.row_spinner_dropdown);
        mSpCategories.setAdapter(adapter);
    }

    private void showDialog() {
        switch (mOpenDialog) {
            case DIALOG_AUDIO:
                mUploadPresenter.openAudioSelectionDialog();
                break;
            case DIALOG_VIDEO:
                mUploadPresenter.openVideoSelectionDialog();
                break;
        }
        mOpenDialog = 0;
    }

    private void setCrimeToPost() {
        if (mCrimeModel == null)
            mCrimeModel = new CrimeModel();
        mCrimeModel.setCrimeTitle(mEtCrimeInfo.getText().toString()) ;
        mCrimeModel.crimeDescription = mEtCrimeDesc.getText().toString();
        mCrimeModel.setSuspectDescription(mEtSuspectDesc.getText().toString());
        mCrimeModel.setVehicleDescription(mEtVehicle.getText().toString()) ;
        mCrimeModel.setWitness(mEtWitnessWhere.getText().toString());
        mCrimeModel.crimeTypeId = ((CrimeTypeModel) mSpCategories.getSelectedItem()).getCrimeTypeId();
        mCrimeModel.entUserId = SessionClass.getInstance().getUserId(getContext());
        mCrimeModel.isActive = true;

    }

    private boolean validateFields() {
        ArrayList<Validation> validations = new ArrayList<>();
        validations.add(new EmptyValidation(getString(R.string.error_crime_info), mEtCrimeInfo, null));
        validations.add(new EmptyValidation(getString(R.string.error_crime_desc), mEtCrimeDesc, null));

        return new Validator(getContext()).validates(validations);
    }


    @OnClick({R.id.audiolink, R.id.videolink})
    public void startAudio(View view) {
        if (mUploadPresenter.getmFilePath() != null) {
            switch (view.getId()) {
                case R.id.audiolink:
                    mUploadPresenter.audioPlayer();
                    break;
                case R.id.videolink:
                    mUploadPresenter.startVideo();
                    break;
            }

        }
    }


    private void getLocation() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).requestLocation();
        }
    }

    @OnClick(R.id.tv_ct_crime_address)
    public void toLocationSelect() {
        Intent i = new Intent(getActivity(), LocationActivity.class);
        startActivityForResult(i, REQUEST_PLACE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PLACE:
                    PlaceObject place = data.getExtras().getParcelable(LocationActivity.PLACE_KEY);
                    if (place != null) {
                        mtvCrimeAddress.setText(place.getAddress());
                        mCrimeModel.setCrimeLocation( place.getAddress());
                        mCrimeModel.crimeGPSLatitude = String.valueOf(place.getLat());
                        mCrimeModel.crimeGPSLongitude = String.valueOf(place.getLng());
                    }
                    break;

                case UploadVideoPresenter.SELECT_FILE_AUDIO: {
                    if (data != null) {
                        mUploadPresenter.setmFilePath(data.getData());

                        videoButton.setVisibility(View.INVISIBLE);
                        videolink.setVisibility(View.INVISIBLE);
                        audioLink.setVisibility(View.VISIBLE);
                    }

                }
                break;

                case UploadVideoPresenter.SELECT_FILE_VIDEO: {
                    if (data != null) {
                        audioButton.setVisibility(View.INVISIBLE);
                        audioLink.setVisibility(View.INVISIBLE);

                        mUploadPresenter.setmFilePath(data.getData());
                        videolink.setVisibility(View.VISIBLE);
                    }
                }
                break;

                case UploadVideoPresenter.CAPTURE_AUDIO: {
                    if (data != null) {
                        videoButton.setVisibility(View.INVISIBLE);
                        videolink.setVisibility(View.INVISIBLE);

                        mUploadPresenter.setmFilePath(data.getData());
                        audioLink.setVisibility(View.VISIBLE);
                    }
                }
                break;

                case UploadVideoPresenter.CAPTURE_VIDEO: {
                    if (data != null) {
                        audioLink.setVisibility(View.INVISIBLE);
                        audioButton.setVisibility(View.INVISIBLE);

                        mUploadPresenter.setmFilePath(data.getData());
                        videolink.setVisibility(View.VISIBLE);
                    }
                    //  uploadAudio(audioVideoRecorder.getCurrentAudioPath());
                }
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case UploadVideoPresenter.REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    mUploadPresenter.startVideoRecording();
                break;
        }
    }

    @Override
    public void onUpdate(Location location) {
        if (mCrimeModel != null) {
            mCrimeModel.userGPSLatitude = String.valueOf(location.getLatitude());
            mCrimeModel.userGPSLongitude = String.valueOf(location.getLongitude());
            mCrimeModel.crimeGPSLatitude = String.valueOf(location.getLatitude());
            mCrimeModel.crimeGPSLongitude = String.valueOf(location.getLongitude());
            mCrimeModel.setCrimeLocation("location") ;
        }
    }

    @OnClick(R.id.audio)
    public void openAudioSelectionDialog() {
        mUploadPresenter.openAudioSelectionDialog();
    }

    @OnClick(R.id.video)
    public void openVideoSelectionDialog() {
        mUploadPresenter.openVideoSelectionDialog();
    }


    @OnClick(R.id.btn_ct_Submit)
    public void handleUploadSequence() {

        AppUtility.hideKeyboard(getActivity().getCurrentFocus(), getContext());

        if (validateFields()) {
            setCrimeToPost();

            if (mUploadPresenter.getmFilePath() != null) {
                if(SessionClass.getInstance().getCrimeModel()==null){
                    ToastUtility.showToastForLongTime(getContext(),getString(R.string.message_crime_uploading));

                    SessionClass.getInstance().setCrimeModel(mCrimeModel);
                    mUploadPresenter.startUploadingService(mConnection);
                    getActivity().onBackPressed();
                }else{
                    ToastUtility.showToastForLongTime(getContext(),getString(R.string.message_crime_already_uploading));
                }

            } else {
                mCrimeTipPresenter.postCrime(mCrimeModel);
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() != null&&mBound) {

            getContext().unbindService(mConnection);
        }
    }

    public ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mUploadingService = ((FileUploadService.LocalBinder) service).getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };


    @Override
    public void onCrimePosted() {

        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
