package com.mobicash.mobipolice.views.fragments;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTipModel;
import com.mobicash.mobipolice.model.dataaccess.entities.WantedModel;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IRightActionOption;
import com.mobicash.mobipolice.model.dataaccess.validations.EmptyValidation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validator;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.presenter.WantedPresenter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrimeDetailsFragment extends BaseFragment implements IRightActionOption,WantedPresenter.ICrimeTipListener {
    private static final String ARG_MODEL = "wanted";
    private static final String TAG = "CrimeDetailsFragment";

    private WantedModel mCurrentCriminal;

    @BindView(R.id.iv_criminal_image)
    ImageView mIvCriminalImage;
    @BindView(R.id.tv_wanted_Status)
    TextView mTvWantedStatus;
    @BindView(R.id.tv_criminal_name)
    TextView mTvCriminalName;
    @BindView(R.id.tv_criminal_age)
    TextView mTvCriminlaAge;
    @BindView(R.id.tv_criminal_height)
    TextView mTvCriminalHeight;
    @BindView(R.id.tv_wanted)
    TextView mTvWantedFor;
    @BindView(R.id.tv_details_name)
    TextView mTvDetailsName;
    @BindView(R.id.tv_details_dob)
    TextView mTvDetailsDob;
    @BindView(R.id.tv_details_height)
    TextView mTvDeatilsHeight;
    @BindView(R.id.tv_details_wanted_For)
    TextView mTvDetailsWWantedFor;
    @BindView(R.id.tv_details_weight)
    TextView mTvdeatilsWeight;

    public static CrimeDetailsFragment newInstance(WantedModel wanted) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_MODEL, wanted);
        CrimeDetailsFragment fragment = new CrimeDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public CrimeDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mCurrentCriminal = ((WantedModel) getArguments().getSerializable(ARG_MODEL));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setViews();
    }

    public void setViews() {
        mTvCriminalName.setText(mCurrentCriminal.getCompeleteName());
        mTvCriminalHeight.setText(mCurrentCriminal.getHeigth());
        mTvCriminlaAge.setText(mCurrentCriminal.getAge());

        mTvDetailsName.setText(getString(R.string.value_suspect, mCurrentCriminal.getCompeleteName()));
        mTvDetailsDob.setText(getString(R.string.value_dob,getString(R.string.ph_not_mentioned)));
        mTvDeatilsHeight.setText(getString(R.string.value_height, mCurrentCriminal.getHeigth()));
        mTvdeatilsWeight.setText(getString(R.string.value_weight, mCurrentCriminal.getWeigth()));

        Picasso.with(getContext()).load(mCurrentCriminal.getWantedImageURL())
                .placeholder(R.drawable.profile)
                .into(mIvCriminalImage);
    }



    private void showTips() {
        if(getContext()!=null){
            final Dialog cdd = new Dialog(getContext());
            cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            cdd.setContentView(R.layout.layout_dialog_tip);
            cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            final EditText etTip = cdd.findViewById(R.id.et_tip);
            cdd.findViewById(R.id.btn_done).setOnClickListener(v -> {
                cdd.dismiss();
                submitTip(etTip);
            });
            cdd.findViewById(R.id.btn_cancel).setOnClickListener(view -> cdd.dismiss());
            cdd.show();
        }
    }

    private void submitTip(EditText etTip){
        if (new Validator(getContext()).validates(
                new EmptyValidation(getString(R.string.error_no_crime_tip)
                        ,etTip,null))){
            new WantedPresenter(getActivity(),this).
                    submitTip(getTip(etTip.getText().toString()));
        }
    }

    private CrimeTipModel getTip(String tip) {
        CrimeTipModel tipModel = new CrimeTipModel();
        tipModel.setTip(tip);
        tipModel.setUserId(SessionClass.getInstance().getUserId(getContext()));
        tipModel.setWantedId(mCurrentCriminal.getId());
        return tipModel;
    }

    @Override
    public void onTipSubmitted() {
        Toast.makeText(getContext(), R.string.message_submitting_tip, Toast.LENGTH_LONG).show();
    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.ic_tip;
    }

    @Override
    public void performRightAction() {
        showTips();
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
