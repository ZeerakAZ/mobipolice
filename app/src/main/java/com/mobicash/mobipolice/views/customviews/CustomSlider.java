package com.mobicash.mobipolice.views.customviews;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.utilities.LogUtility;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.concurrent.TimeUnit;

/**
 * Created by Zeera on 3/11/2018 bt ${File}
 */

public class CustomSlider extends FrameLayout {
    public static final long SLIDE_TIME = TimeUnit.SECONDS.toMillis(5);

    private Handler mSliderHandler;
    private Runnable mSliderRunnable;

    private ViewPager mVpSlider;
    private CirclePageIndicator mCpIndicator;
    private int mCurrentPosition = 0;
    private int mMax = -1;

    public CustomSlider(@NonNull Context context) {
        super(context);
        init(null);
    }

    public CustomSlider(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomSlider(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }


    private void init(AttributeSet attributeSet) {
        LayoutInflater.from(getContext()).inflate(R.layout.custom_viewslider, this, true);
        mVpSlider = findViewById(R.id.vp_videos);
        mCpIndicator = findViewById(R.id.pager_indicator);
        mSliderRunnable = () -> {
            if (mVpSlider != null) {
                int next = mCurrentPosition + 1;
                if (next >= mMax)
                    next = 0;
                mVpSlider.setCurrentItem(next);
            }
        };

    }

    public void setAdapter(PagerAdapter adapter){
        mVpSlider.setAdapter(adapter);
        mMax = adapter.getCount();
        mCpIndicator.setViewPager(mVpSlider);
        mVpSlider.setOffscreenPageLimit(0);
        mVpSlider.setPageTransformer(true,new PagerTransformer());
        CustomViewPagerScroller.changePagerScroller(mVpSlider);
        final float density = getResources().getDisplayMetrics().density;
        mCpIndicator.setRadius(5 * density);

        mCpIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                mCurrentPosition = position;

                stopSlider();
                startSlider();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void startSlider() {
        LogUtility.debugLog("Slider start");
        if (mSliderHandler == null)
            mSliderHandler = new Handler();
        mSliderHandler.postDelayed(mSliderRunnable, SLIDE_TIME);
    }

    public void stopSlider() {
        if (mSliderHandler != null) {
            mSliderHandler.removeCallbacks(mSliderRunnable);
        }
    }
}
