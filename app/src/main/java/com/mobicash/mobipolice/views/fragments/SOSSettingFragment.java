package com.mobicash.mobipolice.views.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.enums.SosType;
import com.mobicash.mobipolice.model.essentials.SharedPrefManager;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.presenter.AccountPresenter;
import com.mobicash.mobipolice.presenter.FacebookPresenter;
import com.mobicash.mobipolice.presenter.SosPresenter;
import com.mobicash.mobipolice.presenter.TwitterPresenter;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * A simple {@link Fragment} subclass.
 */
public class SOSSettingFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {


    public static final String SOS_MESSAGE = "sos_message";
    private static final String TAG = "sosSettingFragment";


    @BindView(R.id.wv_useApp)
    WebView mWVUseApp;
    @BindView(R.id.wv_disclaimer)
    WebView mWVDisclaimer;
    @BindView(R.id.iv_arrow_useApp)
    ImageView mIvUseApp;
    @BindView(R.id.iv_arrow_disclaimer)
    ImageView mIvDisclaimer;
    @BindView(R.id.iv_add_members)
    ImageView mIvAddMembers;
    @BindView(R.id.cb_family)
    CheckBox mCbFamily;
    @BindView(R.id.cb_sms)
    CheckBox mCbSms;
    @BindView(R.id.cb_email)
    CheckBox mCbEmail;
    @BindView(R.id.cb_facebook)
    CheckBox mCbFacebook;
    @BindView(R.id.cb_twitter)
    CheckBox mCbTwitter;
    @BindView(R.id.cb_police)
    CheckBox mCbPolice;
    @BindView(R.id.cb_policing)
    CheckBox mCbPoicing;
    @BindView(R.id.et_alert_message)
    EditText etSosMessage;
    @BindView(R.id.ll_sos_setting)
    LinearLayout llSosSetting;
    @BindView(R.id.ll_sos_setting_tab)
    LinearLayout llSosTab;
    @BindView(R.id.iv_arrow_alert_setting)
    ImageView mIvSosArrow;
    @BindView(R.id.ll_profile_update)
    LinearLayout mLlProfileUpdate;


    private FacebookPresenter mFbBusiness;
    private TwitterLoginButton mTwitterLoginButton;


    public SOSSettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sossetting, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AppUtility.loadHtml(getContext(), mWVUseApp, R.raw.howtouseapp);
        AppUtility.loadHtml(getContext(), mWVDisclaimer, R.raw.disclaimer);


        mFbBusiness = new FacebookPresenter(this);
        mTwitterLoginButton = new TwitterLoginButton(getContext());


        etSosMessage.setText(SharedPrefManager.getInstance(getContext())
                .getStringByKey(SOS_MESSAGE));

        mCbFamily.setChecked(SosType.SOS_FAMILY.getCheck(getContext()));
        mCbFacebook.setChecked(SosType.SOS_FACEBOOK.getCheck(getContext()));
        mCbTwitter.setChecked(SosType.SOS_TWITTER.getCheck(getContext()));
        setCheckedListener();

    }

    private void setCheckedListener() {
        mCbFamily.setOnCheckedChangeListener(this);
        mCbSms.setOnCheckedChangeListener(this);
        mCbEmail.setOnCheckedChangeListener(this);
        mCbFacebook.setOnCheckedChangeListener(this);
        mCbTwitter.setOnCheckedChangeListener(this);
        mCbPoicing.setOnCheckedChangeListener(this);
        mCbPolice.setOnCheckedChangeListener(this);
    }

    @OnClick(R.id.ll_useApp)
    public void showHowToUseApp() {
        boolean visible = mWVUseApp.getVisibility() == View.VISIBLE;
        mWVUseApp.setVisibility(visible ? View.GONE : View.VISIBLE);
        mIvUseApp.animate().rotation(visible ? 0 : -180).setDuration(400);
    }

    @OnClick(R.id.ll_disclaimer)
    public void showDisclaimer() {
        boolean visible = mWVDisclaimer.getVisibility() == View.VISIBLE;
        mWVDisclaimer.setVisibility(visible ? View.GONE : View.VISIBLE);
        mIvDisclaimer.animate().rotation(visible ? 0 : -180).setDuration(400);
    }

    @OnClick(R.id.ll_sos_setting_tab)
    public void showSosSettings() {
        boolean visible = llSosSetting.getVisibility() == View.VISIBLE;
        llSosSetting.setVisibility(visible ? View.GONE : View.VISIBLE);
        mIvSosArrow.animate().rotation(visible ? 0 : -180).setDuration(400);
    }

    @OnClick(R.id.iv_add_members)
    public void addMemberScreen() {
        FragmentUtility.withManager(getFragmentManager())
                .addToBackStack(TAG)
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .replaceToFragment(new AddMemberFragment());

    }

    @OnTextChanged(R.id.et_alert_message)
    public void onTextChange(Editable editable) {
        SharedPrefManager.getInstance(getContext())
                .setStringForKey(editable.toString(), SOS_MESSAGE);
    }


    @OnClick(R.id.ll_profile_update)
    public void toProfileUpdate() {
       /* FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .replaceToFragment(new ProfileUpdateFragment());*/
    }

    @OnClick(R.id.ll_deactivate_account)
    public void deactivateAccount() {
        new AccountPresenter(getActivity(), null).showDeActivateAccountDialog();
    }

    @Override
    public void onDestroy() {
        new SosPresenter(getActivity(), this,null).saveSosMessage();
        super.onDestroy();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_family:
                SosType.SOS_FAMILY.setCheck(getContext(), isChecked);
                break;
            case R.id.cb_facebook:

                if (!SosType.SOS_FACEBOOK.getCheck(getContext())) {
                    if (!FacebookPresenter.isLoggedIn())
                        mFbBusiness.login();
                    else
                        SosType.SOS_FACEBOOK.setCheck(getContext(), true);
                } else {
                    SosType.SOS_FACEBOOK.setCheck(getContext(), false);
                }
                break;
            case R.id.cb_email:
            case R.id.cb_twitter:
                if (!SosType.SOS_TWITTER.getCheck(getContext())) {
                    new TwitterPresenter().login(mTwitterLoginButton, () -> SosType.SOS_TWITTER.setCheck(getContext(), true));
                } else {
                    SosType.SOS_TWITTER.setCheck(getContext(), false);
                }

                break;
            case R.id.cb_sms:
            case R.id.cb_police:
            case R.id.cb_policing:
                Toast.makeText(getContext(), "This Option is Under Development",
                        Toast.LENGTH_LONG).show();
                buttonView.setChecked(!isChecked);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mFbBusiness != null)
            mFbBusiness.getmCallBackManager().onActivityResult(requestCode, resultCode, data);
        if (mTwitterLoginButton != null)
            mTwitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}

