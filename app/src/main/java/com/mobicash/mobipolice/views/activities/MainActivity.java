package com.mobicash.mobipolice.views.activities;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IFragmentUpdateListener;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.GpsHelper;
import com.mobicash.mobipolice.model.utilities.ServiceUtitlity;
import com.mobicash.mobipolice.presenter.AccountPresenter;
import com.mobicash.mobipolice.presenter.BreakingNewPresenter;
import com.mobicash.mobipolice.presenter.ChatPresenter;
import com.mobicash.mobipolice.presenter.SosPresenter;
import com.mobicash.mobipolice.presenter.UserTokenPresenter;
import com.mobicash.mobipolice.presenter.services.UserLocationUpdateService;
import com.mobicash.mobipolice.views.fragments.BaseFragment;
import com.mobicash.mobipolice.views.fragments.ChatFragment;
import com.mobicash.mobipolice.views.fragments.EntranceFragment;
import com.mobicash.mobipolice.views.fragments.LoginFragment;
import com.mobicash.mobipolice.views.viewsetters.FragmentActivityHelper;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements IFragmentUpdateListener,
        FragmentActivityHelper.IActivityContract, ILocationUpdated {

    @BindView(R.id.tv_main_breaking_news)
    TextView mTvBreakingNews;
    @BindView(R.id.rl_chat_header)
    RelativeLayout mRLChatHeader;
    @BindView(R.id.iv_chat_user_image)
    CircleImageView mIvChatUserImage;
    @BindView(R.id.tv_chat_user_name)
    TextView mTvChatUserName;
    @BindView(R.id.rl_default_header)
    RelativeLayout mRlDefaultHeader;

    private FragmentActivityHelper fragmentActivityHelper;
    private GpsHelper mGpsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setBackgroundDrawable(null);///for minimize overdraw
        fragmentActivityHelper = new FragmentActivityHelper(this);
        mGpsHelper = new GpsHelper(this);
        FragmentUtility.withManager(getSupportFragmentManager())
                .replaceToFragment(new EntranceFragment());
        fetchBreakingNews();
        requestLocation();
        postUserToken();
    }



    private void fetchBreakingNews() {
        BreakingNewPresenter breakingNewPresenter = new BreakingNewPresenter(this, (BreakingNewPresenter.IBreakNewListener) news -> {
            mTvBreakingNews.setText(Html.fromHtml(BreakingNewPresenter.getMarqueeNews(news)));
            mTvBreakingNews.setSelected(true);
            mTvBreakingNews.setVisibility(View.VISIBLE);
        });
        breakingNewPresenter.getBreakNews();
    }

    @Override
    public void fragmentUpdated(BaseFragment fragment) {
        fragmentActivityHelper.fragmentUpdated(fragment);

        if (fragment instanceof ChatFragment) {
            mRLChatHeader.setVisibility(View.VISIBLE);
            mRlDefaultHeader.setVisibility(View.GONE);
        } else {
            mRLChatHeader.setVisibility(View.GONE);
            mRlDefaultHeader.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getHeaderTitleId() {
        return R.id.tv_actionBar_title;
    }

    @Override
    public int getHeaderBackId() {
        return R.id.ib_main_back;
    }

    @Override
    public int getRightIconId() {
        return R.id.iv_right_action;
    }

    @Override
    public int getLeftIcon() {
        return R.id.ib_main_slider;
    }

    public void requestLocation() {
        if (mGpsHelper != null)
            mGpsHelper.askForPermmision(this);
    }

    @Override
    public void onUpdate(Location location) {
        BaseFragment currentFragment = FragmentUtility.getCurrentFragment(this,
                R.id.fl_fragmentContainer);
        if (currentFragment != null && currentFragment instanceof ILocationUpdated)
            ((ILocationUpdated) currentFragment).onUpdate(location);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            if (requestCode == GpsHelper.REQUEST_GPS &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocation();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new ChatPresenter(this,null).getAllMessageFromServer();
    }

    public void logOff() {
        new AccountPresenter(this, (AccountPresenter.ILogOffListener) () -> {
            updateViewAfterLogIn();
            checkCountry();
            Toast.makeText(MainActivity.this, R.string.message_logoff, Toast.LENGTH_LONG).show();
            FragmentUtility.withManager(getSupportFragmentManager())
                    .replaceToFragment(new EntranceFragment());
        }).logOffUser();
    }

    private void checkCountry() {

    }

    private void updateViewAfterLogIn() {
        BaseFragment currentFragment = FragmentUtility.getCurrentFragment(this, R.id.fl_fragmentContainer);
        if (currentFragment != null && currentFragment instanceof EntranceFragment) {
            ((EntranceFragment) currentFragment).updateButtons();
        }
        //actionBarHelper.refreshAdapter();
    }

    public void userLoggedIn() {
        Toast.makeText(MainActivity.this, "LogIn Successfully", Toast.LENGTH_LONG).show();
        updateViewAfterLogIn();
        new SosPresenter(MainActivity.this, null, null).fetchSosFromServer();
        postUserToken();
        checkCountry();
        new ChatPresenter(this,null).getAllMessageFromServer();
        ServiceUtitlity.startService(this, UserLocationUpdateService.class);
        //startSpeechRecognizationService();
    }

    private void postUserToken() {
        if (AppUtility.getFCMToken() != null && SessionClass.getInstance().isUserLogin(this)){
            ServiceUtitlity.startService(this, UserLocationUpdateService.class);
            new UserTokenPresenter(this, null).updateToken(AppUtility.getFCMToken());
        }


    }

    public void toLoginScreen() {
        FragmentUtility.withManager(getSupportFragmentManager())
                .addToBackStack("")
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM).
                addFragment(new LoginFragment());
    }

    public View getMtvBreakingNews() {
        return mTvBreakingNews;
    }

    public void setChatHeader(@NonNull String name, @Nullable String imagePath) {
        mTvChatUserName.setText(name);
        if (imagePath != null && !imagePath.isEmpty())
            Picasso.with(this).load(imagePath).placeholder(R.drawable.profile)
                    .into(mIvChatUserImage);
    }
}
