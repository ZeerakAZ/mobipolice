package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.MissingPersonTipAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonGetModel;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonTipModel;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.MapUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.MissingPersonPresenter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonDetailFragment extends BaseFragment implements OnMapReadyCallback,
        MissingPersonPresenter.IMissingTipsListener,MissingPersonPresenter.IMarkFoundListener {

    private static final String ARG_MODEL = "person_model";
    public static final String TAG = "person_detail_frag";

    @BindView(R.id.iv_person_image)
    ImageView mIvImage;
    @BindView(R.id.tv_person_name)
    TextView mTvPersonName;
    @BindView(R.id.tv_person_desc)
    TextView mTvpersonDesc;
    @BindView(R.id.tv_missing_date)
    TextView mTvMissingDate;
    @BindView(R.id.tv_missing_time)
    TextView mTvMissingTime;
    @BindView(R.id.tv_contact_location)
    TextView mTvContactLoccation;
    @BindView(R.id.tv_missing_location)
    TextView mTvMissingLocation;
    @BindView(R.id.tv_contact_name)
    TextView mTvContactName;
    @BindView(R.id.tv_contact_number)
    TextView mTvContactNumber;
    @BindView(R.id.et_tip)
    EditText mEtTip;
    @BindView(R.id.btn_found)
    Button mBtnFound;
    @BindView(R.id.tv_header_tip)
    TextView mTvTip;
    @BindView(R.id.rv_tips)
    RecyclerView mRvTips;

    private MissingPersonGetModel mCurrentPerson;
    private boolean isPostByMe;
    private MissingPersonPresenter mMissingPersonPresenter;

    public static PersonDetailFragment newInstance(MissingPersonGetModel model) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_MODEL, model);
        PersonDetailFragment fragment = new PersonDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public PersonDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentPerson = ((MissingPersonGetModel) getArguments().getSerializable(ARG_MODEL));

        if (mCurrentPerson != null)
            isPostByMe = mCurrentPerson.getPostByUser()
                    == SessionClass.getInstance().getUserId(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMissingPersonPresenter = new MissingPersonPresenter(getActivity(),this);
        MapUtility.loadMapFragment(this,this);
        setViews();
    }

    private void setViews(){
        if(mCurrentPerson!=null){
            mTvPersonName.setText(mCurrentPerson.getPersonName());
            mTvpersonDesc.setText(mCurrentPerson.getPersonDescription());
            mTvMissingDate.setText(mCurrentPerson.getDisapperaceDate());
            mTvMissingTime.setText(mCurrentPerson.getDisapperaceTime());
            mTvMissingLocation.setText(mCurrentPerson.getLocation());
            mTvContactName.setText(mCurrentPerson.getContactName());
            mTvContactNumber.setText(mCurrentPerson.getContactAt());
            mTvContactLoccation.setText(mCurrentPerson.getContactAddress());
            Picasso.with(getContext()).load(mCurrentPerson.getPersonImage())
                    .placeholder(R.drawable.user_placeholder).
                    into(mIvImage);
            mBtnFound.setVisibility(isPostByMe?View.VISIBLE:View.GONE);
            mTvTip.setVisibility(isPostByMe?View.VISIBLE:View.GONE);
            mRvTips.setVisibility(isPostByMe?View.VISIBLE:View.GONE);
            mEtTip.setVisibility(isPostByMe?View.GONE:View.VISIBLE);
            if(isPostByMe){
                mMissingPersonPresenter.getTips(mCurrentPerson.getPersonId());
            }
        }
    }

    @Override
    public void onTipsFetched(ArrayList<MissingPersonTipModel> tips) {
        if(tips.size()>0){
            MissingPersonTipAdapter adapter = new MissingPersonTipAdapter(getContext(),tips);
            mRvTips.setLayoutManager(new LinearLayoutManager(getContext()));
            mRvTips.setAdapter(adapter);
        }else{
            mRvTips.setVisibility(View.GONE);
            mTvTip.setVisibility(View.GONE);
        }
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }



    @OnEditorAction(R.id.et_tip)
    public boolean submitTip(final TextView view, int actionId){

        MissingPersonTipModel missingPersonTipModel = new MissingPersonTipModel(mCurrentPerson.getPersonId(), SessionClass.getInstance().getUserId(getContext()),
                view.getText().toString());
        mMissingPersonPresenter.submitTip(missingPersonTipModel);
       return true;
    }

    @Override
    public void onTipSubmitted() {
        if(getActivity()!=null){
            mEtTip.setText("");
            AppUtility.hideKeyboard(mEtTip,getActivity());
            ToastUtility.showToastForLongTime(getContext(),
                    getString(R.string.message_missing_person_tip_submitted));
        }
    }

    @OnClick(R.id.btn_found)
    public void markAsFound(){
        mMissingPersonPresenter.markAsFound(mCurrentPerson.getPersonId());
    }

    @Override
    public void onMarkedAsFound() {
        if (getActivity() != null) {
            ToastUtility.showToastForLongTime(getContext(),getString(R.string.message_marked_found));
            mBtnFound.setEnabled(false);
            mBtnFound.setAlpha(0.5f);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(mCurrentPerson!=null){
            LatLng target = new LatLng(mCurrentPerson.getLatitude(),mCurrentPerson.getLongitude());
            //googleMap.addMarker(new MarkerOptions().position(target));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(target,15));
            MapUtility.drawCircleOnMap(target,googleMap);
        }
    }


}
