package com.mobicash.mobipolice.views.customviews;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.animation.Interpolator;
import android.widget.Scroller;

import java.lang.reflect.Field;

/**
 * Created by Zeera on 1/28/2018 bt ${File}
 */

public class CustomViewPagerScroller extends Scroller {

    private int mScrollDuration = 1600;

    CustomViewPagerScroller(Context context) {
        super(context);
    }
    CustomViewPagerScroller(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    public void setmScrollDuration(int mScrollDuration) {
        this.mScrollDuration = mScrollDuration;
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, mScrollDuration);
    }
    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        super.startScroll(startX, startY, dx, dy, mScrollDuration);
    }

    public static void changePagerScroller(ViewPager viewPager) {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            CustomViewPagerScroller scroller = new CustomViewPagerScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);
        } catch (Exception e) {
            Log.e("CongoEats", "error of change scroller ", e);
        }
    }
}
