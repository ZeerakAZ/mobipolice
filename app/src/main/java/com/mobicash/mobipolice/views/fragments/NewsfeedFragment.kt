package com.mobicash.mobipolice.views.fragments


import android.location.Location
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.mobicash.mobipolice.R
import com.mobicash.mobipolice.model.dataaccess.adapters.CrimeAdapter
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated
import com.mobicash.mobipolice.model.utilities.MapUtility
import com.mobicash.mobipolice.model.utilities.ToastUtility
import com.mobicash.mobipolice.presenter.CrimeMapPresenter
import com.mobicash.mobipolice.views.activities.BaseActivity
import com.mobicash.mobipolice.views.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_newsfeed.*
import java.util.*
import kotlin.collections.HashMap

const val TAG = "NewsFeedFragment"

class NewsfeedFragment : BaseFragment(), OnMapReadyCallback,
        CrimeMapPresenter.ICrimeListener, CrimeAdapter.ICrimeClickListener,ILocationUpdated {


    private var mMarkerMap: HashMap<String, Marker>? = null
    private var mGoogleMap : GoogleMap? = null
    private var mCrimes : ArrayList<CrimeModel>? = null
    private var mLocation:Location? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_newsfeed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mMarkerMap = HashMap()
        MapUtility.loadMapFragment(this, this)
        if (activity is MainActivity) {
            (activity as MainActivity).requestLocation()
        }
    }

    override fun onCrimesFetched(crimes: ArrayList<CrimeModel>?) {
        crimes?.reverse()
        if (activity != null) {
            pb_wait.visibility = View.GONE
            rv_crimes.visibility = View.VISIBLE
            rv_crimes.layoutManager = LinearLayoutManager(context)
            rv_crimes.adapter = CrimeAdapter(context, crimes).setListener(this)
            drawMarker(crimes)
        }

    }

    private fun drawMarker(crimes: ArrayList<CrimeModel>?) {
        mCrimes = crimes
        if (crimes != null && mGoogleMap!=null) {
            for (crime in crimes){
                if(crime.crimeGPSLatitude.isNullOrBlank()||crime.crimeGPSLongitude.isNullOrBlank() ||
                        crime.crimeGPSLatitude!!.toDoubleOrNull()==null||
                        crime.crimeGPSLongitude!!.toDoubleOrNull()==null)
                    continue
                val marker =MapUtility.addMarker(mGoogleMap, LatLng(crime.crimeGPSLatitude!!.toDouble(),
                        crime.crimeGPSLongitude!!.toDouble()),R.drawable.marker_police,crime.id)
                mMarkerMap?.put(crime.id.toString(),marker)
            }
        }
        updateDistance()
    }

    override fun onMapReady(gMap: GoogleMap?) {
        CrimeMapPresenter(activity, this).getCrimeFromServer()
        mGoogleMap = gMap
    }

    override fun onCrimeSelected(crimeModel: CrimeModel?) {
        moveMapToMarker(crimeModel)
    }

    private fun moveMapToMarker(crime: CrimeModel?) {
        val marker = mMarkerMap?.get(crime?.id.toString())
        if(marker==null)
            ToastUtility.showToastForLongTime(context,getString(R.string.message_no_location_found))
        else{
            mGoogleMap?.clear()
            MapUtility.addMarker(mGoogleMap, LatLng(crime?.crimeGPSLatitude!!.toDouble(),
                    crime.crimeGPSLongitude!!.toDouble()),R.drawable.marker_police,crime.id).run {
                mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(this.position,14f))
            }

        }


    }

    override fun getTagFragment(): String {
        return TAG
    }

    override fun getTitle(): String? {
        return getString(R.string.menu_item_news_feed)
    }

    override fun onUpdate(location: Location?) {
        mLocation = location
        updateDistance()
    }

    private fun updateDistance(){
        if(activity!=null&&mCrimes!=null&&mLocation!=null ){
            for(crime in mCrimes!!){
                if(crime.crimeGPSLatitude.isNullOrBlank()||crime.crimeGPSLongitude.isNullOrBlank() ||
                        crime.crimeGPSLatitude!!.toDoubleOrNull()==null||
                        crime.crimeGPSLongitude!!.toDoubleOrNull()==null){
                    crime.distance = "N/A"
                }else{
                    val result = FloatArray(2)
                    Location.distanceBetween(mLocation!!.latitude,mLocation!!.latitude,crime.crimeGPSLatitude!!.toDouble()
                            ,crime.crimeGPSLongitude!!.toDouble(),result)
                    crime.distance = "${result[0]/1000} km"
                }

            }
            rv_crimes.adapter.notifyDataSetChanged()

        }
    }
}
