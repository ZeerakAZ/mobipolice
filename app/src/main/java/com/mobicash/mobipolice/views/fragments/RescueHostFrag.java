package com.mobicash.mobipolice.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.utilities.ViewPagerBusiness;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RescueHostFrag extends BaseFragment {
    public static final String TAG ="rescuehostfrag";

    @BindView(R.id.tl_main_tabs)
    TabLayout mTabCrime;
    @BindView(R.id.vp_main)
    ViewPager mVpCrimes;
    ViewPagerBusiness vpHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rescue_host, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        setViewpager();
    }

    private void setViewpager(){

        vpHelper = new ViewPagerBusiness(mTabCrime,mVpCrimes,getContext(),
                getChildFragmentManager(), ViewPagerBusiness.TYPE_PAGER.MISSING_PERSON);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
