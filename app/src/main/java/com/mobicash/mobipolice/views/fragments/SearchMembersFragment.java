package com.mobicash.mobipolice.views.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.ChatUserAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.PagingUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.ChatUserPresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchMembersFragment extends BaseFragment
        implements PagingUtility.IPagingInterFace, ChatUserAdapter.IFavourite,
        ChatUserPresenter.ISearchMemberListerner, ChatUserPresenter.IMemberFavListener {

    private static final String ARG_TYPE = "key_type";
    private static final int REQ_CODE_SPEECH_INPUT = 0xf01;
    private static final String TAG = "searchMember";

    @BindView(R.id.et_search)
    EditText mEtSearch;
    @BindView(R.id.rv_members)
    RecyclerView mRvMembers;
    @BindView(R.id.tv_info)
    TextView mTvinfo;

    private SearchType mType;
    private PagingUtility mPagingHelper;
    private ChatUserAdapter mAdapter;
    private String mSearchTerm;
    private ChatUserPresenter mChatUserPresenter;

    public SearchMembersFragment() {
        // Required empty public constructor
    }

    public static SearchMembersFragment newInstance(SearchType type) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        SearchMembersFragment fragment = new SearchMembersFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mType = (SearchType) getArguments().getSerializable(ARG_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_members, container, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        mChatUserPresenter = new ChatUserPresenter(getActivity(), this);
        initiateViews();
    }


    private void initiateViews() {
        mPagingHelper = new PagingUtility(this);
    }

    @OnEditorAction(R.id.et_search)
    public boolean searhForUser(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            mSearchTerm = mEtSearch.getText().toString();

            if (mAdapter != null) {
                mPagingHelper.clearState();
                mAdapter.clearData();
            }
            searchMembers(mSearchTerm);
            mEtSearch.clearFocus();
            AppUtility.hideKeyboard(mEtSearch, getContext());
            return true;
        }

        return false;
    }


    private void searchMembers(String searchTerm) {
        if (!mPagingHelper.isLoading()) {
            mRvMembers.setVisibility(View.VISIBLE);
            mChatUserPresenter.searchMember(searchTerm, mPagingHelper);

        }
    }

    @Override
    public void onUserFetched(ArrayList<UserModel> users) {
        setAdapter(users);
        if (mAdapter == null || mAdapter.getItemCount() == 0) {
            mTvinfo.setText(R.string.message_no_member);
            mTvinfo.setVisibility(View.VISIBLE);
        } else mTvinfo.setVisibility(View.GONE);


    }


    @Override
    public void loadMoreData() {
        if (mSearchTerm != null)
            searchMembers(mSearchTerm);
    }


    public void setAdapter(ArrayList<UserModel> users) {
        if (mRvMembers != null) {
            if (mAdapter == null) {
                mAdapter = new ChatUserAdapter(users, getContext(), mChatUserPresenter::toChatFragment);
                mRvMembers.setLayoutManager(new LinearLayoutManager(getContext()));
                mRvMembers.setAdapter(mAdapter);
                mAdapter.setmFavListener(this);
                mPagingHelper.registerPagindCallbacks(mRvMembers);
            } else {
                mAdapter.addUsers(users);
            }
        }
    }

    @OnClick(R.id.iv_speak_now)
    public void onSpeakNow() {
        AppUtility.startSpeachListener(this, REQ_CODE_SPEECH_INPUT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    mEtSearch.setText(result.get(0));
                    searhForUser(EditorInfo.IME_ACTION_SEARCH);
                }
                break;
            }

        }
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void addToFavourite(int userId) {
        mChatUserPresenter.addMemberToFav(userId);
    }

    @Override
    public void memberAddedToFav() {
        ToastUtility.showToastForLongTime(getContext(), getString(R.string.message_added_to_fav));
    }

    public enum SearchType {
        CONTACTS;
    }
}
