package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.WantedAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.WantedModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.presenter.WantedPresenter;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class WantedFragment extends BaseFragment implements WantedPresenter.IWantedListener{

    private static String TAG = "wanted-frag";

    @BindView(R.id.rv_wanted)
    RecyclerView mRvWanted;
    private WantedPresenter mWantedPresenter;

    public static WantedFragment newInstance() {
        Bundle args = new Bundle();
        WantedFragment fragment = new WantedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public WantedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wanted, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mWantedPresenter = new WantedPresenter(getActivity(), this);
        mWantedPresenter.getWanted();
    }

    @Override
    public void onWantedFetched(ArrayList<WantedModel> wantedModels) {
        WantedAdapter adapter = new WantedAdapter(wantedModels, getContext(),this::onItemClicked);
        mRvWanted.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvWanted.setAdapter(adapter);
    }


    public void onItemClicked(WantedModel model){
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                .addToBackStack(TAG)
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addFragment(CrimeDetailsFragment.newInstance(model));

    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
