package com.mobicash.mobipolice.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.ChatUserAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.PagingUtility;
import com.mobicash.mobipolice.presenter.ChatUserPresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class MyChatListFragment extends BaseFragment
        implements PagingUtility.IPagingInterFace,ChatUserPresenter.IFavUserListener {
    private static final String TAG = "myChatListFragment";

    @BindView(R.id.rv_chats)
    RecyclerView mRvChats;
    @BindView(R.id.fab_add)
    FloatingActionButton mFabAdd;
    @BindView(R.id.pb_wait)
    ProgressBar mPbWait;
    @BindView(R.id.tv_no_contacts)
    TextView mTvNoContacts;

    private PagingUtility mPagingHelper;
    private ChatUserAdapter mAdapter;
    private ChatUserPresenter mChatUserPresenter;

    public MyChatListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_chat_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mChatUserPresenter = new ChatUserPresenter(getActivity(),this);
        mPagingHelper = new PagingUtility(this);
        mAdapter = null;
        getMemberForChat();
    }


    @Override
    public void loadMoreData() {
        getMemberForChat();
    }

    private void getMemberForChat() {
        if (!mPagingHelper.isLoading()) {
           mChatUserPresenter.getFavouriteUser(mPagingHelper);
        }
    }


    private void setAdapter(ArrayList<UserModel> users) {
        if (mRvChats != null) {
            if (mAdapter == null) {
                mAdapter = new ChatUserAdapter(users, getContext(),
                        mChatUserPresenter::toChatFragment);
                mRvChats.setLayoutManager(new LinearLayoutManager(getContext()));
                mRvChats.setAdapter(mAdapter);
                mPagingHelper.registerPagindCallbacks(mRvChats);
            } else {
                mAdapter.addUsers(users);
            }
        }
    }

    @Override
    public void onFavUserFecthed(ArrayList<UserModel> users) {
        mPbWait.setVisibility(View.GONE);
        if (mAdapter == null)
            mTvNoContacts.setVisibility(users.size() == 0 ? View.VISIBLE : View.GONE);
        setAdapter(users);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @OnClick(R.id.fab_add)
    public void toAddMemberFragment() {
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(TAG)
                .replaceToFragment(new SearchMembersFragment());

    }
}
