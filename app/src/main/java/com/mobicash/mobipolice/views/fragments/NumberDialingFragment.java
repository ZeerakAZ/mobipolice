package com.mobicash.mobipolice.views.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.utilities.AudioPlayerUtitlity;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class NumberDialingFragment extends BaseFragment {
    public static final String ARG_NUMBER = "number";
    public static final String ARG_DRAWABLE = "drawable";
    public static final String TAG = "numberDialingFragment";

    @BindView(R.id.ll_main)
    LinearLayout mLlMain;
    @BindView(R.id.tv_message)
    TextView mTvMessage;
    @BindView(R.id.iv_logo)
    ImageView mIvLogo;
    @BindView(R.id.tv_timer)
    TextView mTvTimer;
    @BindView(R.id.tv_press_to_dial)
    TextView mTvPressToDial;
    @BindView(R.id.ll_place_call)
    LinearLayout mLlPlaceCall;
    @BindView(R.id.tv_cancel)
    TextView mTvCancel;


    private String mNumber;
    private int mDrableResid;
    private CountDownTimer mTimer;
    private AudioPlayerUtitlity mAudioPlayer;


    public NumberDialingFragment() {
        // Required empty public constructor
    }

    public static NumberDialingFragment newInstance(String number, int drawableResId) {
        Bundle args = new Bundle();
        args.putString(ARG_NUMBER, number);
        args.putInt(ARG_DRAWABLE, drawableResId);
        NumberDialingFragment fragment = new NumberDialingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mNumber = getArguments().getString(ARG_NUMBER);
            mDrableResid = getArguments().getInt(ARG_DRAWABLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_number_dialing, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mAudioPlayer = new AudioPlayerUtitlity();
        setMessage();
    }

    private void setMessage() {
        String message = String.format(Locale.ENGLISH,
                getString(R.string.message_emergency_call), mNumber);
        mTvMessage.setText(message);
        mIvLogo.setImageResource(mDrableResid);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TransitionManager.beginDelayedTransition(mLlMain);
                mTvMessage.setVisibility(View.VISIBLE);
                mIvLogo.setVisibility(View.VISIBLE);
            }
        }, 200);

    }

    @OnClick(R.id.iv_logo)
    public void callNumber() {

        mTimer = new CountDownTimer(TimeUnit.SECONDS.toMillis(10),
                TimeUnit.SECONDS.toMillis(1)) {
            @Override
            public void onTick(long l) {
                mAudioPlayer.play(getContext(), R.raw.tick, null);

                mTvTimer.setText(String.format(getString(R.string.ph_sec_count),
                        String.valueOf(TimeUnit.MILLISECONDS.toSeconds(l))));
            }

            @Override
            public void onFinish() {
                dialNumber();
            }
        };
        mIvLogo.setVisibility(View.GONE);
        mTvPressToDial.setVisibility(View.GONE);
        mLlPlaceCall.setVisibility(View.VISIBLE);
        mTimer.start();
    }

    @OnClick(R.id.tv_cancel)
    public void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mIvLogo.setVisibility(View.VISIBLE);
            mTvPressToDial.setVisibility(View.VISIBLE);
            mLlPlaceCall.setVisibility(View.GONE);
        }
    }

    boolean placeCall = false;

    @OnClick(R.id.tv_timer)
    public void placeCall() {
        if (placeCall)
            dialNumber();
        placeCall = true;
        new Handler().postDelayed(() -> placeCall = false, 400);


    }

    private void dialNumber() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + mNumber));
        startActivity(intent);
        cancelTimer();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null)
            mTimer.cancel();
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
