package com.mobicash.mobipolice.views.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mobicash.mobipolice.R
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel
import kotlinx.android.synthetic.main.fragment_crime_list_details.*


/**
 * A simple [Fragment] subclass.
 *
 */
class CrimeListDetailsFragment : BaseFragment() {
    private val TAG = "CrimeListThumbFragment"
    private var mCrimeMode:CrimeModel? = null

    companion object {
        private const val ARG_CRIME_MODEL = "crime_model"

        fun newInstance(crimeModel: CrimeModel): CrimeListDetailsFragment {
            val args = Bundle()
            args.putSerializable(ARG_CRIME_MODEL,crimeModel)
            val fragment = CrimeListDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun getTagFragment(): String {
        return TAG
    }

    override fun getTitle(): String? {
        return null
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_list_details, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mCrimeMode = arguments?.getSerializable(ARG_CRIME_MODEL) as CrimeModel?

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCrimeInfo()
    }

    private fun setCrimeInfo() {
        tv_crime_title.text = getString(R.string.value_crime_title,mCrimeMode?.crimeTitle)
        tv_crime_location.text = getString(R.string.value_crime_location,mCrimeMode?.crimeLocation)
        tv_distance.text = getString(R.string.value_distance,mCrimeMode?.distance)
        tv_vehicle_desc.text = getString(R.string.value_vehicle_desc,mCrimeMode?.vehicleDescription)
        tv_witness.text = getString(R.string.value_witness,mCrimeMode?.witness)
        tv_suspect_description.text = getString(R.string.value_suspect_desc,mCrimeMode?.suspectDescription)

    }


}
