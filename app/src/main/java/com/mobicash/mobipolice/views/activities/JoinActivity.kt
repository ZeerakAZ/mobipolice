package com.mobicash.mobipolice.views.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.IntDef
import android.view.View
import android.widget.AdapterView
import android.widget.CheckBox
import android.widget.RadioButton
import com.mobicash.mobipolice.R
import com.mobicash.mobipolice.model.dataaccess.entities.DistrictModel
import com.mobicash.mobipolice.model.dataaccess.entities.ProvinceModel
import com.mobicash.mobipolice.model.dataaccess.entities.UserDeviceToken
import com.mobicash.mobipolice.model.dataaccess.entities.UserRegisterModel
import com.mobicash.mobipolice.model.dataaccess.interfaces.ICode
import com.mobicash.mobipolice.model.dataaccess.validations.*
import com.mobicash.mobipolice.model.utilities.AppUtility
import com.mobicash.mobipolice.model.utilities.ImageUtility
import com.mobicash.mobipolice.model.utilities.SpinnerUtility
import com.mobicash.mobipolice.model.utilities.ToastUtility
import com.mobicash.mobipolice.presenter.MobiProvincePresenter
import com.mobicash.mobipolice.presenter.UserRegistrationPresenter
import com.mobicash.mobipolice.views.dialogs.LoadingDialog
import kotlinx.android.synthetic.main.activity_join.*
import kotlinx.android.synthetic.main.layout_registration_step_1.*
import kotlinx.android.synthetic.main.layout_registration_step_2.*
import kotlinx.android.synthetic.main.layout_registration_step_3.*
import kotlinx.android.synthetic.main.layout_registration_step_4.*
import kotlinx.android.synthetic.main.layout_registration_step_5.*

class JoinActivity : BaseActivity(), MobiProvincePresenter.IProvinceFetchListener,
        MobiProvincePresenter.IDistrictFetchListener, UserRegistrationPresenter.IUserCheckListener,
        UserRegistrationPresenter.ICodeVerificationListener {


    companion object {
        @IntDef(STEP_1.toLong(), STEP_2.toLong(),
                STEP_3.toLong(), STEP_4.toLong(), STEP_5.toLong(),
                STEP_6.toLong(), STEP_7.toLong())
        @Retention(AnnotationRetention.SOURCE)
        annotation class STEPS

        const val STEP_1 = 1
        const val STEP_2 = 2
        const val STEP_3 = 3
        const val STEP_4 = 4
        const val STEP_5 = 5
        const val STEP_6 = 6
        const val STEP_7 = 7

        const val CAMERA_FACE = 0x01
        const val GALLERY_FACE = 0x02
        const val CAMERA_ID = 0x03
        const val GALLERY_ID = 0x04
        const val CAMERA_RESIDENT = 0x05
        const val GALLERY_RESIDENT = 0x06

        const val DATE_FORMAT = "yyyy-MM-dd"

        private val TITLE = arrayOf(R.string.title_registration_1, R.string.title_registration_2,
                R.string.title_registration_3, R.string.title_registration_4, R.string.title_registration_5,
                R.string.title_registration_6, R.string.title_registration_7)


    }

    @STEPS
    private var mCurrentStep: Int = STEP_1
    private var mValidator: Validator? = null
    private var mImageHelperFace: ImageUtility? = null
    private var mImageHelperId: ImageUtility? = null
    private var mImageHelperResident: ImageUtility? = null
    private var mProvincePresenter: MobiProvincePresenter? = null
    private var mUserRegistrationPresenter: UserRegistrationPresenter? = null
    private var mWaitDialog: LoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join)

        mImageHelperFace = ImageUtility(this, iv_face_image, CAMERA_FACE, GALLERY_FACE)
        mImageHelperId = ImageUtility(this, iv_id_image, CAMERA_ID, GALLERY_ID)
        mImageHelperResident = ImageUtility(this, iv_resident_image, CAMERA_RESIDENT, GALLERY_RESIDENT)
        mProvincePresenter = MobiProvincePresenter(this, this).apply { mIsForSpinner = true }
        mUserRegistrationPresenter = UserRegistrationPresenter(this, this)
        mWaitDialog = LoadingDialog(this)

        btn_next.setOnClickListener { performNextAction() }
        et_dob.setOnClickListener { openDobCalender() }
        et_id_expiry_date.setOnClickListener { openExpiryCalendar() }

        iv_resident_image.setOnClickListener { mImageHelperResident?.checkCameraAndExternalStoragePermissions() }
        iv_face_image.setOnClickListener { mImageHelperFace?.checkCameraAndExternalStoragePermissions() }
        iv_id_image.setOnClickListener { mImageHelperId?.checkCameraAndExternalStoragePermissions() }

        mValidator = Validator(this)
        setUpSpinners()
        updateTitle(STEP_1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                CAMERA_RESIDENT -> {
                    mImageHelperResident?.setImageFromCameraResult(data)
                }
                CAMERA_ID -> {
                    mImageHelperId?.setImageFromCameraResult(data)
                }
                CAMERA_FACE -> {
                    mImageHelperFace?.setImageFromCameraResult(data)
                }
                GALLERY_RESIDENT -> {
                    mImageHelperResident?.convertAndSetImageToUI(data)
                }
                GALLERY_FACE -> {
                    mImageHelperFace?.convertAndSetImageToUI(data)
                }
                GALLERY_ID -> {
                    mImageHelperId?.convertAndSetImageToUI(data)
                }
            }
    }

    private fun openExpiryCalendar() {
        AppUtility.showDatePickerDialog(this, fragmentManager, et_id_expiry_date, DATE_FORMAT, true)

    }

    private fun openDobCalender() {
        AppUtility.showDatePickerDialog(this, fragmentManager, et_dob, DATE_FORMAT, false)
    }

    private fun performNextAction() {

        if (validateSteps() == true) {
            when (mCurrentStep) {
                STEP_1 -> {
                    mUserRegistrationPresenter?.checkNumber(getNumber())
                }
                STEP_2 -> {
                    mWaitDialog?.apply {
                        message = getString(R.string.message_verifying_code)
                        show()
                    }
                    mUserRegistrationPresenter?.verifyCode(et_verifyCode.text.toString())
                }
                STEP_3 -> {

                    vf_views.showNext()
                    updateTitle(STEP_4)

                }
                STEP_4 -> {
                    vf_views.showNext()
                    updateTitle(STEP_5)
                }
                STEP_5 -> {
                    vf_views.showNext()
                    updateTitle(STEP_6)
                }
                STEP_6 -> {
                    vf_views.showNext()
                    updateTitle(STEP_7)
                }
                STEP_7 -> {
                    registerUser()
                }
            }
        }

    }

    private fun registerUser() {
        val userRegisterModel = UserRegisterModel().apply {
            mobile = getNumber()
            firstName = et_firstName.text.toString()
            lastName = et_lastName.text.toString()
            email = et_email.text.toString()
            dob = et_dob.text.toString()
            gender = findViewById<RadioButton>(rg_gender.checkedRadioButtonId).text.toString()
            occupation = sp_occupation.selectedItem.toString()
            address = et_address.text.toString()
            location = et_location.text.toString()
            language = sp_language.selectedItem.toString()
            province = (sp_province.selectedItem as ProvinceModel).id
            city = (sp_districts.selectedItem as DistrictModel).id
            maritalstatus = findViewById<RadioButton>(rg_martial_status.checkedRadioButtonId).text.toString()
            idtype = sp_card_type.selectedItem.toString()
            idNo = et_id_number.text.toString()
            idexpiration = et_id_expiry_date.text.toString()
            nokname = et_nok_name.text.toString()
            nokrelation = sp_nok_relation.selectedItem.toString()
            nokmobile = et_nok_number.text.toString()
            pinCode = et_pin.text.toString()
            password = et_password.text.toString()

            /*faceImage = mImageHelperFace?.imageBase64
            idImage = mImageHelperId?.imageBase64
            residencephoto = mImageHelperResident?.imageBase64*/

            userToken = UserDeviceToken(AppUtility.getFCMToken() ?: "",
                    AppUtility.getUniqueDeviceId(this@JoinActivity))
        }
        mUserRegistrationPresenter?.registerUser(userRegisterModel)

    }

    private fun updateTitle(step: Int) {
        mCurrentStep = step
        tv_title.text = getString(TITLE[step - 1])
        tv_step.text = getString(R.string.step_count, step)
    }

    private fun setUpSpinners() {
        sp_occupation.adapter = SpinnerUtility.getSpinnerAdapter(this, R.array.arr_occupation)
        sp_nok_relation.adapter = SpinnerUtility.getSpinnerAdapter(this, R.array.arr_nok_relation)
        sp_language.adapter = SpinnerUtility.getSpinnerAdapter(this, R.array.arr_language)
        sp_card_type.adapter = SpinnerUtility.getSpinnerAdapter(this, R.array.arr_card_type)

        sp_province.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position != 0) {
                    (sp_province.getItemAtPosition(position) as ProvinceModel).let {
                        mProvincePresenter?.getDistricts(it.id)
                    }
                }
            }

        }
        mProvincePresenter?.getProvince()
    }

    private fun getNumber(): String {
        return "${tv_country_code.text}${et_phoneNumber_step1.text}"
    }

    override fun userChecked(isExists: Boolean) {
        if (!isExists) {

            et_phoneNumber_verify.setText(getNumber())
            mWaitDialog?.run {
                message = getString(R.string.message_verification_code)
                show()
            }
            mUserRegistrationPresenter?.sendVerificationCode(getNumber(), ICode {
                if (it) {
                    vf_views.showNext()
                    updateTitle(STEP_2)
                }
                mWaitDialog?.hide()
            })
        } else
            ToastUtility.showToastForLongTime(this, getString(R.string.error_number_exists))
    }

    override fun codeVerificationDone(isVerified: Boolean) {
        mWaitDialog?.hide()
        if (isVerified) {
            vf_views.showNext()
            updateTitle(STEP_3)
        }
    }

    override fun onDistrictFetched(districts: ArrayList<DistrictModel>) {
        sp_districts.adapter = SpinnerUtility.getSpinnerAdapter(this, districts)
    }

    override fun onProvinceFetched(provinces: ArrayList<ProvinceModel>) {
        sp_province.adapter = SpinnerUtility.getSpinnerAdapter(this, provinces)
    }

    private fun validateSteps(step: Int = mCurrentStep): Boolean? {
        return when (step) {
            STEP_1 -> {
                mValidator?.validates(EmptyValidation(getString(R.string.error_phone_number), et_phoneNumber_step1))
            }
            STEP_2 -> {
                mValidator?.validates(EmptyValidation(getString(R.string.error_verification_code), et_verifyCode))
            }
            STEP_3 -> {
                if(mImageHelperFace?.imagePath==null){
                    ToastUtility.showToastForLongTime(this,getString(R.string.error_no_face_image))
                    false
                }else{
                    mValidator?.validates(ArrayList<Validation>().apply {
                        add(EmptyValidation(getString(R.string.error_first_name), et_firstName))
                        add(EmptyValidation(getString(R.string.error_last_name), et_lastName))
                        add(EmptyValidation(getString(R.string.error_email_required), et_email))
                        add(EmailValidation(getString(R.string.error_invalid_email), et_email, null))
                        add(EmptyValidation(getString(R.string.error_dob), et_dob))
                        add(RadioGroupSelectValidation(getString(R.string.error_gender), rg_gender))

                    })
                }

            }
            STEP_4 -> {
                if(mImageHelperResident?.imagePath==null){
                    ToastUtility.showToastForLongTime(this,getString(R.string.error_no_resident_image))
                    false
                }else{
                    mValidator?.validates(ArrayList<Validation>().apply {
                        add(SpinnerItemSelectValidation(getString(R.string.error_occupation), sp_occupation))
                        add(EmptyValidation(getString(R.string.error_address), et_address))
                        add(EmptyValidation(getString(R.string.error_location), et_location))
                        add(SpinnerItemSelectValidation(getString(R.string.error_language), sp_language))
                        add(SpinnerItemSelectValidation(getString(R.string.error_province), sp_province))
                        add(SpinnerItemSelectValidation(getString(R.string.error_city), sp_districts))
                        add(RadioGroupSelectValidation(getString(R.string.error_martial_status), rg_martial_status))

                    })
                }

            }
            STEP_5 -> {
                if(mImageHelperId?.imagePath==null){
                    ToastUtility.showToastForLongTime(this,getString(R.string.error_no_id_image))
                    false
                }else{
                    mValidator?.validates(ArrayList<Validation>().apply {
                        add(SpinnerItemSelectValidation(getString(R.string.error_id_type), sp_card_type))
                        add(EmptyValidation(getString(R.string.error_id_number), et_id_number))
                        add(EmptyValidation(getString(R.string.error_id_date), et_id_expiry_date))

                    })
                }

            }
            STEP_6 -> {
                mValidator?.validates(ArrayList<Validation>().apply {
                    add(EmptyValidation(getString(R.string.error_nok_name), et_nok_name))
                    add(SpinnerItemSelectValidation(getString(R.string.error_nok_relation), sp_nok_relation))
                    add(EmptyValidation(getString(R.string.error_id_number), et_nok_number))
                    add(EmptyValidation(getString(R.string.error_id_date), et_nok_address))

                })
            }
            STEP_7 -> {
                mValidator?.validates(ArrayList<Validation>().apply {
                    add(EmptyValidation(getString(R.string.error_no_pin), et_pin))
                    add(EmptyValidation(getString(R.string.error_no_password), et_password))

                })
            }
            else -> {
                false
            }
        }
    }

    override fun onBackPressed() {
        when (mCurrentStep) {
            STEP_1 -> {
                super.onBackPressed()
            }
            STEP_2 -> {
                /* vf_views.showPrevious()
                 updateTitle(STEP_1)*/
            }
            STEP_3 -> {
                super.onBackPressed()
                /*   vf_views.showPrevious()
                   updateTitle(STEP_2)
   */
            }
            STEP_4 -> {
                vf_views.showPrevious()
                updateTitle(STEP_3)
            }
            STEP_5 -> {
                vf_views.showPrevious()
                updateTitle(STEP_4)
            }
            STEP_6 -> {
                vf_views.showPrevious()
                updateTitle(STEP_5)
            }
            STEP_7 -> {
                vf_views.showPrevious()
                updateTitle(STEP_6)
            }
        }


    }


}
