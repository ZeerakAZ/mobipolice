package com.mobicash.mobipolice.views.fragments;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.mobicash.mobipolice.Manifest;
import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.ViewPagerAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.PagerModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IRightActionOption;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.PermissionsUtitlty;

import java.security.Permission;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChatRootFragment extends BaseFragment implements IRightActionOption{

    private static final String TAG = "tag";

    public static final String RECENT = "Recent Chat";
    public static final String CONTACTS = "Users";
    public static final String GROUP = "Groups";
    private static final int REQUEST_CONTACTS = 1;

    @BindView(R.id.tb_chat)
    TabLayout mtbChats;
    @BindView(R.id.vp_chats)
    ViewPager mVpChats;

    private HashMap<String,String> mTabData;

    public ChatRootFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_root, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setChatsViewPager();
    }

    public void setChatsViewPager(){
        ArrayList<PagerModel> frags = new ArrayList<>();
        frags.add(new PagerModel(RECENT,ChatListFragment.newInstance(ChatListFragment.TYPE_RECENT)));
        frags.add(new PagerModel(CONTACTS,new MyChatListFragment()));
        frags.add(new PagerModel(GROUP,ChatListFragment.newInstance(ChatListFragment.TYPE_GROUP)));
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager(),frags);
        mVpChats.setAdapter(adapter);
        mtbChats.setupWithViewPager(mVpChats);

    }



    public void setTabData(String key,String value){
        if(mTabData==null)
            mTabData = new HashMap<>();
        mTabData.put(key,value);
    }

    public String getTabData(String key){
        if(mTabData!=null)
            return mTabData.get(key);
        return null;
    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.ic_action_name;
    }

    @Override
    public void performRightAction() {
        if(PermissionsUtitlty.isContactsPermisionAllowed(getContext()))
            toShareContact();
        else{
            PermissionsUtitlty.showPermissionsDialogForFragment(this,REQUEST_CONTACTS,
                    android.Manifest.permission.READ_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==REQUEST_CONTACTS){
            if(grantResults.length>0){
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    toShareContact();
                }
            }
        }
    }

    public void toShareContact(){
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(TAG)
                .replaceToFragment(new ShareContactFragment());
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
