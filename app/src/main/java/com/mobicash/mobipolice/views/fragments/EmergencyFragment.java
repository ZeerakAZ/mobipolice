package com.mobicash.mobipolice.views.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.EmergencyAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.EmergencyNumber;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.PermissionsUtitlty;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EmergencyFragment extends BaseFragment {

    private static final int REQUEST_PLACE_CALL = 1;
    private static final String TAG = "emergencyFragment";

    @BindView(R.id.rv_numbers)
    RecyclerView mRvNumbers;
    private EmergencyNumber mEmergencyNumber;

    public EmergencyFragment() {
        // Required empty public constructor
    }

    public static EmergencyFragment newInstance() {
        EmergencyFragment fragment = new EmergencyFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_emergency, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
    }


    private void setAdapter() {
        final ArrayList<EmergencyNumber> data = getData();
        EmergencyAdapter adapter = new EmergencyAdapter(data, getContext(),this::dialNumber);
        mRvNumbers.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvNumbers.setAdapter(adapter);
    }

    private void dialNumber(EmergencyNumber number) {
        mEmergencyNumber = number;
        if (PermissionsUtitlty.isCallPermissionAllowed(getContext()))
            placeCall(mEmergencyNumber.getNumber(),mEmergencyNumber.getImageResouceId());
        else
            PermissionsUtitlty.showPermissionsDialogForFragment(this, REQUEST_PLACE_CALL,
                    Manifest.permission.CALL_PHONE);
    }

    private ArrayList<EmergencyNumber> getData() {
        ArrayList<EmergencyNumber> numbers = new ArrayList<>();
        numbers.add(new EmergencyNumber("Police", "10111",R.drawable.police));
        numbers.add(new EmergencyNumber("Cellphone ", "112",R.drawable.call_icon_emg));
        numbers.add(new EmergencyNumber("Fire", "10177",R.drawable.fire));
        numbers.add(new EmergencyNumber("Hospital ER", "084124",R.drawable.hospital));
        numbers.add(new EmergencyNumber("NetCare", "082911",R.drawable.call_icon_emg));
        numbers.add(new EmergencyNumber("Child line", "0800 123 321",R.drawable.childline));
        numbers.add(new EmergencyNumber("Gender Violence", "0800 150 150",R.drawable.gender_voilence));
        numbers.add(new EmergencyNumber("Ambulance", "10177",R.drawable.ambulance));
        return numbers;
    }

    public void placeCall(String phoneNumber, int drawableId) {
        FragmentUtility.withManager(getFragmentManager())
                .addToBackStack(TAG)
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .replaceToFragment(NumberDialingFragment.newInstance(phoneNumber,drawableId));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PLACE_CALL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                placeCall(mEmergencyNumber.getNumber(),mEmergencyNumber.getImageResouceId());
            }
        }
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
