package com.mobicash.mobipolice.views.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.enums.SosType;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IMapFragment;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IRightActionOption;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.MapUtility;
import com.mobicash.mobipolice.model.utilities.ServiceUtitlity;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.SosPresenter;
import com.mobicash.mobipolice.presenter.UploadVideoPresenter;
import com.mobicash.mobipolice.presenter.services.CrimeLookUpService;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.mobicash.mobipolice.views.dialogs.GenericDialog;
import com.mobicash.mobipolice.views.dialogs.IDialogDouble;
import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */


public class SosFragment extends BaseFragment implements
        OnMapReadyCallback, ILocationUpdated, IMapFragment,SosPresenter.ISosListener,IRightActionOption {
    @IntDef({TYPE_SOS, TYPE_SAFE})
    @Retention(RetentionPolicy.SOURCE)
    @interface Type {
    }



    public static final int TYPE_SOS = 0x102;
    public static final int TYPE_SAFE = 0x103;
    private static final String ARG_TYPE = "type";
    private static final String TAG = "sos-fragment";

    private GoogleMap mMap;
    private SosPresenter mSosPresenter;
    private UploadVideoPresenter mUploadPresenter;

    @Type
    private int mType;

    @BindView(R.id.fab_audio)
    FloatingActionButton mAudio;
    @BindView(R.id.fab_camera)
    FloatingActionButton mCamera;
    @BindView(R.id.fab_chat)
    FloatingActionButton mChat;
    @BindView(R.id.fab_crime)
    FloatingActionButton mCrime;
    @BindView(R.id.btn_send_alert)
    ImageView mIvSendHelp;
    @BindView(R.id.btn_send_alert_police)
    ImageView mIvAlertPolice;

    public SosFragment() {
        // Required empty public constructor
    }

    public static SosFragment newInstance(@Type int type) {
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE, type);
        SosFragment fragment = new SosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mType = getArguments().getInt(ARG_TYPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sos, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MapUtility.loadMapFragment(this, this);

        mSosPresenter = new SosPresenter(getActivity(), this, this);
        mUploadPresenter = new UploadVideoPresenter(getActivity(), this, null);
        setButtons();
    }

    private void setButtons() {
        mCrime.setImageResource(ServiceUtitlity.isMyServiceRunning(CrimeLookUpService.class,
                getContext()) ? R.drawable.crime_lookup_ac : R.drawable.crime_lookup);

        if (mType == TYPE_SAFE) {
            mCrime.setVisibility(View.GONE);
            mIvAlertPolice.setVisibility(View.GONE);
            mIvSendHelp.setImageResource(R.drawable.iamsafe_btn);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void updatePosition() {
        Location location = SessionClass.getInstance().getCurrentUserLocation();
        if (location != null) {
            LatLng userPos = new LatLng(location.getLatitude(),
                    location.getLongitude());
            MapUtility.addMarker(mMap, userPos,
                    null, true, true);
            if (mSosPresenter != null)
                mSosPresenter.Location(location);
        } else {
            if (getActivity() instanceof MainActivity)
                ((MainActivity) getActivity()).requestLocation();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        updatePosition();
    }

    @Override
    public void onUpdate(Location location) {
        updatePosition();
    }

    @Override
    public void onDestroy() {
        destroyMap();
        super.onDestroy();
    }

    @OnClick(R.id.btn_send_alert)
    public void sendAlert() {
        if (mSosPresenter != null) {
            mSosPresenter.sendSos(mType==TYPE_SOS? SosPresenter.SOS_TYPE.SOS_FAMILY:
            SosPresenter.SOS_TYPE.SOS_SAFE);
        }
    }

    @OnClick(R.id.btn_send_alert_police)
    public void alertPolice(){
        if (mSosPresenter != null) {
            mSosPresenter.sendSos(SosPresenter.SOS_TYPE.SOS_POLICE);
        }
    }

    @Override
    public void destroyMap() {
        MapUtility.killMapFragment(this);
    }

    @OnClick(R.id.fab_audio)
    public void onAudio() {
        mUploadPresenter.openAudioSelectionDialog();
    }

    @OnClick(R.id.fab_camera)
    public void onCamera() {
        mUploadPresenter.openVideoSelectionDialog();
    }

    @OnClick(R.id.fab_chat)
    public void onChat() {
       /* if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).pressDrawerItem(1);
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            ToastUtility.showToastForLongTime(getContext(), getString(R.string.message_upload_started));
        }
    }

    @OnClick(R.id.fab_crime)
    public void toggleCrimeService() {
        if (ServiceUtitlity.isMyServiceRunning(CrimeLookUpService.class, getContext())) {
            ServiceUtitlity.stopService(getContext(), CrimeLookUpService.class);
            mCrime.setImageResource(R.drawable.crime_lookup);
        } else {
            new GenericDialog(getContext(), getString(R.string.ph_warning),
                    getString(R.string.message_crime_look_up),
                    getString(R.string.ph_yes), getString(R.string.cancel), new IDialogDouble() {
                @Override
                public void onRightClick(DialogInterface dialogInterface) {
                    dialogInterface.dismiss();
                }

                @Override
                public void onLeftClick(DialogInterface dialogInterface) {

                    ServiceUtitlity.startService(getContext(), CrimeLookUpService.class);
                    mCrime.setImageResource(R.drawable.crime_lookup_ac);
                    dialogInterface.dismiss();
                }
            }).setLogo(R.drawable.dialog_warning).show();

        }
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void sosSend(SosType type, boolean isSuccessful) {

    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.setting_option;
    }

    @Override
    public void performRightAction() {
        FragmentUtility.withManager(getFragmentManager())
                .addToBackStack(TAG)
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .replaceToFragment(new SOSSettingFragment());
    }
}
