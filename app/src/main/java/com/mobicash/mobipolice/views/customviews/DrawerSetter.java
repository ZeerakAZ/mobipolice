package com.mobicash.mobipolice.views.customviews;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.DrawerAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.MenuItem;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.views.activities.BaseActivity;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.mobicash.mobipolice.views.fragments.ChatRootFragment;
import com.mobicash.mobipolice.views.fragments.CrimeMapFragment;
import com.mobicash.mobipolice.views.fragments.CrimeTipsHostFragment;
import com.mobicash.mobipolice.views.fragments.EmergencyLocator;
import com.mobicash.mobipolice.views.fragments.NewsfeedFragment;
import com.mobicash.mobipolice.views.fragments.PoliceLocatorFragment;
import com.mobicash.mobipolice.views.fragments.RescueHostFrag;

import java.util.ArrayList;

/**
 * Created by Zeera on 3/18/2018 bt ${File}
 */

public class DrawerSetter implements DrawerAdapter.IMenuClickListener{

    private ArrayList<MenuItem> menuItems;

    private DrawerLayout mDlMain;
    private RecyclerView mRvMenu;

    private DrawerAdapter mMenuAdapter;
    private BaseActivity mActivity;



    private ActionBarDrawerToggle mDrawerToggle;


    public DrawerSetter(BaseActivity activity,DrawerLayout drawerLayout, RecyclerView rvMenu) {

        mActivity = activity;
        menuItems = new ArrayList<>();
        mDlMain = drawerLayout;
        mRvMenu = rvMenu;

        setMenuItems();
        setAdapter();
    }


    public void addMenuItem(MenuItem item) {
        menuItems.add(item);
    }

    private void setMenuItems() {
        menuItems.add(new MenuItem(R.string.menu_item_home,R.drawable.home_icon,R.drawable.home_icon));
        menuItems.add(new MenuItem(R.string.menu_item_asset_track,R.drawable.asset_track,R.drawable.asset_track,true));
        menuItems.add(new MenuItem(R.string.menu_item_chat,R.drawable.chat,R.drawable.chat,true));
        menuItems.add(new MenuItem(R.string.menu_item_crime_map,R.drawable.crime_map,R.drawable.crime_map,false));
        menuItems.add(new MenuItem(R.string.menu_item_news_feed,R.drawable.crime_map,R.drawable.crime_map,false));
        menuItems.add(new MenuItem(R.string.menu_item_traffic_mapping,R.drawable.crime_map,R.drawable.crime_map,false));
        menuItems.add(new MenuItem(R.string.menu_item_crime_tip,R.drawable.crime_tips,R.drawable.crime_tips,true));
        menuItems.add(new MenuItem(R.string.menu_item_search_and_rescue,R.drawable.search_and_rescue,R.drawable.search_and_rescue,true));
        menuItems.add(new MenuItem(R.string.menu_item_emergency_locator,R.drawable.emergency_locator,R.drawable.emergency_locator,false));
        menuItems.add(new MenuItem(R.string.menu_item_log_out,R.drawable.logout,R.drawable.logout));
        menuItems.add(new MenuItem(R.string.menu_item_exit,R.drawable.exit,R.drawable.exit));
    }

    private void logOffUser() {
        if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).logOff();
        }
    }



    private void setAdapter() {
        mMenuAdapter = new DrawerAdapter(menuItems, mActivity, this);
        mRvMenu.setLayoutManager(new LinearLayoutManager(mActivity));
        mRvMenu.setAdapter(mMenuAdapter);
    }

    public void refreshAdapter() {
        if (mMenuAdapter != null) {
            mMenuAdapter.notifyDataSetChanged();
        }
    }

    private void setDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(mActivity, mDlMain,
                R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public boolean onOptionsItemSelected(android.view.MenuItem item) {
                if (item != null && item.getItemId() == android.R.id.home) {
                    if (mDlMain.isDrawerOpen(Gravity.LEFT)) {
                        mDlMain.closeDrawer(Gravity.LEFT);
                    } else {
                        mDlMain.openDrawer(Gravity.LEFT);
                    }
                }
                return false;
            }


        };
        mDlMain.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    @Override
    public void onMenuItemClicked(int clickedItem) {
        FragmentUtility utility = FragmentUtility.withManager(mActivity.getSupportFragmentManager())
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .addToBackStack(null);

        switch (clickedItem){
            case R.string.menu_item_asset_track:
                break;
            case R.string.menu_item_chat:
                utility.replaceToFragment(new ChatRootFragment());
                break;
            case R.string.menu_item_crime_map:
                utility.replaceToFragment(new CrimeMapFragment());
                break;
            case R.string.menu_item_crime_tip:
                utility.replaceToFragment(new CrimeTipsHostFragment());
                break;
            case R.string.menu_item_emergency_locator:
                utility.replaceToFragment(new EmergencyLocator());
                break;
            case R.string.menu_item_news_feed:
                utility.replaceToFragment(new NewsfeedFragment());
                break;
            case R.string.menu_item_traffic_mapping:
                utility.replaceToFragment(new PoliceLocatorFragment());
                break;
            case R.string.menu_item_search_and_rescue:
                utility.replaceToFragment(new RescueHostFrag());
                break;
            case R.string.menu_item_exit:
                mActivity.showExitDialog();
                break;
            case R.string.menu_item_home:
                break;
            case R.string.menu_item_log_out:
                logOffUser();
                break;
        }
        toggleDrawer();
    }




    public void toggleDrawer(){
        if (mDlMain.isDrawerOpen(Gravity.LEFT)) {
            mDlMain.closeDrawer(Gravity.LEFT);
        } else {
            mDlMain.openDrawer(Gravity.LEFT);
        }
    }


    public DrawerLayout getDrawerLayout() {
        return mDlMain;
    }
}
