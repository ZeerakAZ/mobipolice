package com.mobicash.mobipolice.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.IceTipAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.IceTipModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IceHelperFragment extends BaseFragment {
    private static final String TAG = "iceHelperFragment";

    @BindView(R.id.rv_tips)
    RecyclerView mRvTips;

    public IceHelperFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ice_helper, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setIceAidTips();
    }

    private void setIceAidTips() {
        ArrayList<IceTipModel> iceTips = new ArrayList<>();
        iceTips.add(new IceTipModel(getString(R.string.tip_1),getString(R.string.tip_1_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_2),getString(R.string.tip_2_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_3),getString(R.string.tip_3_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_4),getString(R.string.tip_4_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_5),getString(R.string.tip_5_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_6),getString(R.string.tip_6_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_7),getString(R.string.tip_7_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_8),getString(R.string.tip_8_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_9),getString(R.string.tip_9_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_10),getString(R.string.tip_10_details)));
        iceTips.add(new IceTipModel(getString(R.string.tip_11),getString(R.string.tip_11_details)));

        mRvTips.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvTips.setAdapter(new IceTipAdapter(iceTips,getContext()));
        mRvTips.setNestedScrollingEnabled(false);

    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
