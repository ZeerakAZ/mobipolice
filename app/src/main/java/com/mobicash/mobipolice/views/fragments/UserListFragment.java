package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.AppCountryAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.CountryUserModel;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.dataaccess.network.IApiInterface;
import com.mobicash.mobipolice.presenter.ActiveUserPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserListFragment extends BaseFragment implements ActiveUserPresenter.IActiveUserFetchListener{


    private Unbinder unbinder;
    @BindView(R.id.tv_user_countries)
    RecyclerView mRvUsers;
    @BindView(R.id.pb_wait)
    ProgressBar mPbWait;

    private HashMap<String,CountryUserModel> mCountriesMap;

    public UserListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        mCountriesMap = new HashMap<>();
        getActiveUsers();
    }

    private void getActiveUsers() {
        new ActiveUserPresenter(getActivity(),this).getActiveUser();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public String getTagFragment() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void activeUser(ArrayList<CountryUserModel> users, int count) {
        mRvUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvUsers.setAdapter(new AppCountryAdapter(getContext(), new ArrayList<>(mCountriesMap.values())));
        mPbWait.setVisibility(View.GONE);
    }


}
