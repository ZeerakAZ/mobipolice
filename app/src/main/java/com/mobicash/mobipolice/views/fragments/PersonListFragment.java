package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.MissingPersonAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonGetModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.presenter.MissingPersonPresenter;
import com.mobicash.mobipolice.views.activities.BaseActivity;
import com.paginate.Paginate;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonListFragment extends BaseFragment implements MissingPersonPresenter.IMissingPersonListener{
    public static final String STATUS_LOST = "Lost";
    public static final String STATUS_LOCATED = "Located";
    public static final String STATUS_FOUND = "Found";
    private static final String ARG_TYPE = "type";

    public static final String TAG = "personListFragment";
    public static final int TYPE_MINE = 0xf01;
    public static final int TYPE_OTHERS = 0xf02;
    private static final int NO_OF_ROWS = 1000;


    private ArrayList<MissingPersonGetModel> mPersonMissing;
    private ArrayList<MissingPersonGetModel> mPersonLocated;
    private ArrayList<MissingPersonGetModel> mPersonFound;
    private String mSelectedType = STATUS_LOST;

    @BindView(R.id.rv_missing_persons)
    RecyclerView mRvMissingPerson;
    @BindView(R.id.sp_filter)
    Spinner mSpinnerFilter;
    @BindView(R.id.tv_content)
    TextView mTvContent;

    private Paginate.Callbacks mPagingCallBack;
    private boolean isCallApi, isLoadedAll;
    private Integer mLastPageId = 0;
    private int mType;

    public PersonListFragment() {
        // Required empty public constructor
    }

    public static PersonListFragment newInstance(int type) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        PersonListFragment fragment = new PersonListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_list, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mType = getArguments().getInt(ARG_TYPE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mPersonMissing = new ArrayList<>();
        mPersonFound = new ArrayList<>();
        mPersonLocated = new ArrayList<>();
        setSpinnerFilter();

        setPagging();
        getMissingPerson();
    }

    private void setPagging() {
        isLoadedAll = false;
        mLastPageId = 0;
        isCallApi = false;
        pagingInitiilize = false;
        mPagingCallBack = new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getMissingPerson();
                    }
                }, 500);
            }

            @Override
            public boolean isLoading() {
                return isCallApi;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return isLoadedAll;
            }
        };
    }

    private void setSpinnerFilter() {
        ArrayList<String> types = new ArrayList<>();
        types.add("Missing");
        types.add("Located");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                R.layout.row_spinner,
                R.id.text1, types);
        adapter.setDropDownViewResource(R.layout.row_spinner_dropdown);
        mSpinnerFilter.setAdapter(adapter);
        mSpinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mSelectedType = STATUS_LOST;

                        break;
                    case 1:
                        mSelectedType = STATUS_LOCATED;
                        break;
                }
                setAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getMissingPerson() {
        if (isCallApi)
            return;
        isCallApi = true;
        new MissingPersonPresenter(getActivity(),this).getMissingPerson(NO_OF_ROWS,mLastPageId);
    }

    @Override
    public void onMissingPersonResponse(@Nullable String body) {
        isCallApi = false;
        if (body != null) {
            afterGetPersons(body);
        }
    }

    private void afterGetPersons(String responce) {
        try {
            JSONArray array = new JSONArray(responce);
            Gson gson = new GsonBuilder().create();
            for (int i = 0; i < array.length(); i++) {
                MissingPersonGetModel missingPerson =
                        gson.fromJson(array.getString(i), MissingPersonGetModel.class);
                mLastPageId = missingPerson.getPersonId();
                if ((mType == TYPE_MINE && SessionClass.getInstance().getUserId(getContext()) == missingPerson.getPostByUser())
                        || (mType == TYPE_OTHERS && SessionClass.getInstance().getUserId(getContext()) != missingPerson.getPostByUser()))
                    switch (missingPerson.getStatus()) {
                        case STATUS_FOUND:
                            mPersonFound.add(missingPerson);
                            break;
                        case STATUS_LOCATED:
                            mPersonLocated.add(missingPerson);
                            break;
                        case STATUS_LOST:
                            mPersonMissing.add(missingPerson);
                            break;
                    }
                setAdapter();
            }
            isLoadedAll = array.length() < NO_OF_ROWS;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    MissingPersonAdapter mMissingPersonAdapter;
    private boolean pagingInitiilize;

    private void setAdapter() {
        final ArrayList<MissingPersonGetModel> data = getData();
        mTvContent.setVisibility(data.size() == 0 ? View.VISIBLE : View.GONE);
        mMissingPersonAdapter = new MissingPersonAdapter(getContext(), data, this::viewPersonDetails);
        mRvMissingPerson.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvMissingPerson.setAdapter(mMissingPersonAdapter);
        if (!pagingInitiilize)
            Paginate.with(mRvMissingPerson, mPagingCallBack)
                    .setLoadingTriggerThreshold(1)
                    .addLoadingListItem(true)
                    .build();
        pagingInitiilize = true;
    }

    private ArrayList<MissingPersonGetModel> getData() {
        final ArrayList<MissingPersonGetModel> data = new ArrayList<>();
        switch (mSelectedType) {
            case STATUS_FOUND:
                mTvContent.setText("");
                data.addAll(mPersonFound);
                break;
            case STATUS_LOCATED:
                mTvContent.setText(R.string.message_no_located_person);
                data.addAll(mPersonLocated);
                break;
            case STATUS_LOST:
                mTvContent.setText(R.string.message_no_missing_person);
                data.addAll(mPersonMissing);
                break;
        }
        return data;
    }



    @OnClick(R.id.btn_report)
    public void toAddPersonScreen() {
        if (getActivity() instanceof BaseActivity) {
            FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                    .addToBackStack(TAG)
                    .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                    .replaceToFragment(new AddPersonFragment());
        }
    }

    public void viewPersonDetails(MissingPersonGetModel model) {
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                .addToBackStack(TAG)
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_LEFT)
                .replaceToFragment(PersonDetailFragment.newInstance(model));
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
