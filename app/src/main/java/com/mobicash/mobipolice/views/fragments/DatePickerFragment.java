package com.mobicash.mobipolice.views.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 04-Feb-16.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    IDatePickerListener _iDatePickerListener;
    boolean isToLimit = false;
    boolean isMinimum = false;
    boolean isMonthYearOnly = false;


    public IDatePickerListener get_iDatePickerListener() {
        return _iDatePickerListener;
    }

    public void set_iDatePickerListener(IDatePickerListener _iDatePickerListener) {
        this._iDatePickerListener = _iDatePickerListener;
    }

    public boolean isTolimit() {
        return isToLimit;
    }

    public void setTolimit(boolean tolimit) {
        isToLimit = tolimit;
    }

    public void setToMinimum(boolean isMinimum) {
        this.isMinimum = isMinimum;
    }

    public void setMonthYearOnly(boolean monthYearOnly) {
        isMonthYearOnly = monthYearOnly;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, year, month, day);
        try {
            if (isMonthYearOnly) {
                java.lang.reflect.Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
                for (java.lang.reflect.Field datePickerDialogField : datePickerDialogFields) {
                    if (datePickerDialogField.getName().equals("mDatePicker")) {
                        datePickerDialogField.setAccessible(true);
                        DatePicker datePicker = (DatePicker) datePickerDialogField.get(dpd);
                        java.lang.reflect.Field[] datePickerFields = datePickerDialogField.getType().getDeclaredFields();
                        for (java.lang.reflect.Field datePickerField : datePickerFields) {
                            Log.i("test", datePickerField.getName());
                            if ("mDaySpinner".equals(datePickerField.getName())) {
                                datePickerField.setAccessible(true);
                                Object dayPicker = datePickerField.get(datePicker);
                                ((View) dayPicker).setVisibility(View.GONE);
                            }
                        }
                    }
                }
            }

            if (isToLimit) {
                if (isMinimum) {
                    dpd.getDatePicker().setMinDate(new Date().getTime() - 1000);
                } else {
                    dpd.getDatePicker().setMaxDate(new Date().getTime());
                }
            }
        } catch (Exception ex) {
        }

        return dpd;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (_iDatePickerListener != null) {
            _iDatePickerListener.afterSetDate(year, month, day);
        }
        // Do something with the date chosen by the user
    }

    public interface IDatePickerListener {
        public void afterSetDate(int year, int month, int day);
    }
}
