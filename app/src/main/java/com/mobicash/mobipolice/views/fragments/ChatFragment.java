package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.ChatAdapter;
import com.mobicash.mobipolice.model.dataaccess.database.DataBasehadlerClass;
import com.mobicash.mobipolice.model.dataaccess.entities.GroupModel;
import com.mobicash.mobipolice.model.dataaccess.entities.MessageModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IMessageReceived;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.DateUtility;
import com.mobicash.mobipolice.model.utilities.KeyBoardHelper;
import com.mobicash.mobipolice.presenter.ChatPresenter;
import com.mobicash.mobipolice.views.activities.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment implements
        IMessageReceived,
        KeyBoardHelper.IKeyBoardOpen {
    private int senderId;

    public static final int TYPE_ONE_TO_ONE = 1200;
    public static final int TYPE_GROUP = 1201;

    private static final String ARG_USER = "user";
    private static final String ARG_GROUP = "group";
    private static final String ARG_TYPE = "type";

    private Unbinder mUnBinder;
    @BindView(R.id.rv_chat_messages)
    RecyclerView mRvChats;
    @BindView(R.id.et_chat_message)
    EditText mEtMessage;

    LinearLayoutManager mLlManager;
    ChatAdapter mAdapter;
    private ArrayList<MessageModel> mMessages;
    private UserModel mReceiver;
    private GroupModel mGroup;
    private int mType;
    private boolean isGroup;
    private DataBasehadlerClass mDbHelper;
    private int mLastChatId;

    private ChatPresenter mChatPresenter;

    public static ChatFragment newInstance(UserModel user, int type) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_USER, user);
        args.putInt(ARG_TYPE, type);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ChatFragment newInstance(GroupModel group, int type) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_GROUP, group);
        args.putInt(ARG_TYPE, type);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mType = getArguments().getInt(ARG_TYPE);
            if (getArguments().containsKey(ARG_USER)) {
                mReceiver = ((UserModel) getArguments().getSerializable(ARG_USER));
            } else if (getArguments().containsKey(ARG_GROUP)) {
                mGroup = ((GroupModel) getArguments().getSerializable(ARG_GROUP));
            }
        }
        isGroup = mType == TYPE_GROUP;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDbHelper = SessionClass.getInstance().getmDbHelper(getActivity());
        senderId = SessionClass.getInstance().getUserId(getContext());
        mChatPresenter = new ChatPresenter(getActivity(),null);

        initializeAdapter();
        getMessageFromDb();
        if(getActivity() instanceof MainActivity){
            if(isGroup){
                ((MainActivity) getActivity()).setChatHeader(mGroup.getName(),null);
            }else{
                ((MainActivity) getActivity()).setChatHeader(mReceiver.getFullname(),mReceiver.getProfileurl());
            }

        }

    }

    private void getMessageFromDb() {
        switch (mType) {
            case TYPE_ONE_TO_ONE:
                mMessages = mDbHelper.getAllMessags(getContext(),mReceiver.getUserid(), senderId);
                break;
            case TYPE_GROUP:
                mMessages = mDbHelper.getAllGroupMessags(getContext(),mGroup.getId());
                break;

        }
        mAdapter.setData(mMessages);
        if (mMessages.size() > 0)
            mLastChatId = mMessages.get(mMessages.size() - 1).getChatId();
        scrollChatToPosition(null);
    }

    private void initializeAdapter() {
        mAdapter = new ChatAdapter(null, getContext());
        mLlManager = new LinearLayoutManager(getContext());
        mRvChats.setLayoutManager(mLlManager);
        mRvChats.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnBinder != null)
            mUnBinder.unbind();
        mDbHelper = null;
    }

    @OnClick(R.id.iv_btn_send)
    public void sentMessage() {
        String message = mEtMessage.getText().toString();
        if (message.trim().isEmpty())
            return;
        if (mMessages != null && mAdapter != null) {
            mEtMessage.getText().clear();
            final MessageModel messageModel = getMessage(message);
            messageModel.setSendingTime(DateUtility.getCurrentDate());
            messageModel.setMe(true);
            mMessages.add(messageModel);
            mAdapter.notifyItemChanged(mMessages.size() - 1);
            scrollChatToPosition(null);

            mChatPresenter.sendMessage(messageModel,getMessage(AppUtility.getEncodedString(message)), isGroup);
        }
    }

    private MessageModel getMessage(String messageText) {
        return new MessageModel(senderId, isGroup ? mGroup.getId() :
                mReceiver.getUserid(),
                messageText, isGroup);
    }



    //endregion

    @Override
    public void onMessageReceived(Integer userId) {
        getMessageFromDb();
        scrollChatToPosition(null
        );
    }

    @Override
    public void onGroupMessageReceived(Integer groupId) {
        getMessageFromDb();
        scrollChatToPosition(null);
    }

    @Override
    public void keyBoardStateChanged(boolean isOpen) {
        if (isOpen)
            scrollChatToPosition(null);
    }

    private void scrollChatToPosition(@Nullable Integer pos) {
        if (mLlManager != null) {
            mLlManager.scrollToPosition(pos == null ? mMessages.size() - 1 : pos);
        }

    }

    @Override
    public String getTagFragment() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
