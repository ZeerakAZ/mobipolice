package com.mobicash.mobipolice.views.fragments;


import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.VideoView;

import com.mobicash.mobipolice.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentVideo extends BaseFragment {
    private static final String ARG_PATH = "path";
    private static final String ARG_IMAGE = "resource";

    private String mImagePath;
    private int mOverlayImageId;

    private VideoView mVideoView;
    private FrameLayout mFlOverlay;

    public static FragmentVideo newInstance(String videoPath, int overLayRes) {
        Bundle args = new Bundle();
        args.putString(ARG_PATH, videoPath);
        args.putInt(ARG_IMAGE, overLayRes);

        FragmentVideo fragment = new FragmentVideo();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentVideo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImagePath = getArguments().getString(ARG_PATH);
        mOverlayImageId = getArguments().getInt(ARG_IMAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_video, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mVideoView = view.findViewById(R.id.vid);
        ImageView mIvPlayOverlay = view.findViewById(R.id.play_overlay);
        ImageView mIvPlayIcon = view.findViewById(R.id.play_icon);
        mFlOverlay = view.findViewById(R.id.fl_overlay);

        mIvPlayOverlay.setImageResource(mOverlayImageId);

        if (mImagePath != null)
            mIvPlayOverlay.setOnClickListener(this::playVideo);
        else {
            mIvPlayIcon.setVisibility(View.GONE);
        }
    }

    public void playVideo(View view) {
        if (getParentFragment() instanceof EntranceFragment) {
            ((EntranceFragment) getParentFragment()).stopSlider();
        }
        if (getView() != null) {
            mFlOverlay.setVisibility(View.GONE);
            Uri uri = Uri.parse(mImagePath);
            mVideoView.setVideoURI(uri);
            mVideoView.requestFocus();
            mVideoView.start();
            mVideoView.setOnCompletionListener(mp -> mVideoView.start());
        }

    }

    public void stopVideo() {
        if (getView() != null && mVideoView != null) {
            mFlOverlay.setVisibility(View.VISIBLE);
            mVideoView.stopPlayback();
        }
    }

    @Override
    public String getTagFragment() {
        return "";
    }

    @Override
    public String getTitle() {
        return null;
    }
}
