package com.mobicash.mobipolice.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RadioGroup;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IceFragment extends BaseFragment {

    @BindView(R.id.rg_ice_menu)
    RadioGroup mRgMenu;
    @BindView(R.id.ll_menu)
    LinearLayout mLlMenu;

    private static final String TAG = "ice-fragment";

    private int mTranslateBy;

    public IceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ice, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mRgMenu.setOnCheckedChangeListener((radioGroup, i) -> {
            switch (i) {
                case R.id.rb_instruction:
                    replaceToFragment(new IceInstruction());

                    break;
                case R.id.rb_first_aid_tip:
                    replaceToFragment(new IceHelperFragment());
                    break;

            }
            translateView(mLlMenu, -mTranslateBy);
        });
        mLlMenu.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mLlMenu.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                mTranslateBy = mLlMenu.getWidth(); //height is ready
                translateView(mLlMenu, -mTranslateBy);
            }
        });
        replaceToFragment(new IceInstruction());
    }

    private void replaceToFragment(BaseFragment fragment) {
        FragmentUtility.withManager(getChildFragmentManager())
                .intoContainerId(R.id.fl_ice_fragment)
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .replaceToFragment(fragment);


    }

    @OnClick({R.id.iv_close, R.id.iv_open})
    void toggleMenu(View view) {
        switch (view.getId()) {
            case R.id.iv_close:
                translateView(mLlMenu, -mTranslateBy);
                break;
            case R.id.iv_open:
                translateView(mLlMenu, 0);
                break;
        }
    }

    private void translateView(View viewToTranslate, int by) {
        viewToTranslate.animate().translationX(by).setDuration(500);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
