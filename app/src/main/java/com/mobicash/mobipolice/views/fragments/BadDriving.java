package com.mobicash.mobipolice.views.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.PlaceObject;
import com.mobicash.mobipolice.model.dataaccess.validations.EmptyValidation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validator;
import com.mobicash.mobipolice.model.utilities.ImageUtility;
import com.mobicash.mobipolice.presenter.UploadVideoPresenter;
import com.mobicash.mobipolice.views.activities.LocationActivity;
import com.mobicash.mobipolice.views.dialogs.LoadingDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class BadDriving extends BaseFragment {

    private static final String TAG ="bad_driving_fragment";

    private final int REQUEST_PLACE = 100;
    private final int CAPTURE_AUDIO = 101;
    private final int CAPTURE_VIDEO = 102;
    private final int SELECT_FILE_AUDIO = 103;
    private final int SELECT_FILE_VIDEO = 104;

    private final int REQUEST_CAMERA = 1;

    public static final int CODE_CAMERA_PIC = 5555;
    public static final int CODE_IMAGE_PIC = 5556;

    public static final int DIALOG_VIDEO = 0xF02;

    private ImageUtility mImageHelper;

    @BindView(R.id.iv_camera)
    ImageButton mIvCamera;
    @BindView(R.id.video)
    ImageButton mIvVideo;
    @BindView(R.id.tv_ct_crime_address)
    TextView mTvCrimeAddress;
    @BindView(R.id.videolink)
    TextView mTvVideoLink;
    @BindView(R.id.et_ct_crime_info)
    EditText mEtCrimeInfo;
    @BindView(R.id.et_ct_crime_desc)
    EditText mEtCrimeDesc;

    private UploadVideoPresenter mUploadPresenter;


    public BadDriving() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bad_driving, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mImageHelper = new ImageUtility(this, mIvCamera, CODE_CAMERA_PIC, CODE_IMAGE_PIC);
        mUploadPresenter = new UploadVideoPresenter(getActivity(), this, null);
    }

    private boolean validateFields() {
        ArrayList<Validation> validations = new ArrayList<>();
        validations.add(new EmptyValidation(getString(R.string.error_crime_info), mEtCrimeInfo, null));
        validations.add(new EmptyValidation(getString(R.string.error_crime_desc), mEtCrimeDesc, null));
        validations.add(new EmptyValidation(getString(R.string.error_crime_desc), mTvCrimeAddress, null));

        return new Validator(getContext()).validates(validations);

    }

    @OnClick(R.id.videolink)
    public void startAudio(View view) {
        if (mUploadPresenter.getmFilePath() != null) {
            mUploadPresenter.startVideo();
        }
    }



    @OnClick(R.id.video)
    public void openVideoSelectionDialog() {
        mUploadPresenter.openVideoSelectionDialog();
    }

    @OnClick(R.id.iv_camera)
    public void showImageOption() {
        mImageHelper.checkCameraAndExternalStoragePermissions();
    }

    public void startVideoRecording() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(intent, CAPTURE_VIDEO);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PLACE:
                    PlaceObject place = data.getExtras().getParcelable(LocationActivity.PLACE_KEY);
                    if (place != null) {
                        mTvCrimeAddress.setText(place.getAddress());
                    }
                    break;

                case CODE_CAMERA_PIC: {
                    if (data != null) {
                        mImageHelper.setImageFromCameraResult(data);

                        mIvVideo.setVisibility(View.INVISIBLE);
                        mTvVideoLink.setVisibility(View.INVISIBLE);
                        mIvCamera.setVisibility(View.VISIBLE);
                    }

                }
                break;

                case SELECT_FILE_VIDEO: {
                    if (data != null) {
                        mIvCamera.setVisibility(View.INVISIBLE);
                        mIvCamera.setVisibility(View.INVISIBLE);

                        mUploadPresenter.setmFilePath(data.getData());
                        mTvVideoLink.setVisibility(View.VISIBLE);
                    }
                }
                break;

                case CODE_IMAGE_PIC: {
                    if (data != null) {
                        mImageHelper.convertAndSetImageToUI(data);

                        mIvVideo.setVisibility(View.INVISIBLE);
                        mTvVideoLink.setVisibility(View.INVISIBLE);

                    }
                }
                break;

                case CAPTURE_VIDEO: {
                    if (data != null) {
                        mIvCamera.setVisibility(View.INVISIBLE);
                        mIvCamera.setVisibility(View.INVISIBLE);

                        mUploadPresenter.setmFilePath(data.getData());
                        mTvVideoLink.setVisibility(View.VISIBLE);
                    }
                }
                break;
            }
        }
    }

    @OnClick(R.id.btn_ct_Submit)
    public void postCrime() {
        if (validateFields()) {
            final LoadingDialog loadingDialog = new LoadingDialog(getContext());
            loadingDialog.show();
            loadingDialog.setMessage("Uploading Crime");
            new Handler().postDelayed(() -> {
                loadingDialog.hide();
                Toast.makeText(getContext(), R.string.message_crime_posted, Toast.LENGTH_SHORT).show();
            }, 5000);
        }


    }

    @OnClick(R.id.tv_ct_crime_address)
    public void startLocation() {
        Intent i = new Intent(getActivity(), LocationActivity.class);
        startActivityForResult(i, REQUEST_PLACE);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CAMERA:

                break;
        }
    }
}
