package com.mobicash.mobipolice.views.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.utilities.ImageUtility;


/**
 * Created by Administrator on 11/11/2016.
 */

public class CameraDialog extends Dialog implements View.OnClickListener {
    ImageUtility mHelper;
    TextView cameraTv, galleryTv;

    public CameraDialog(Activity activity, ImageUtility helper) {
        super(activity);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.layout_camera);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        cameraTv = findViewById(R.id.camera);
        galleryTv = findViewById(R.id.gallery);

        cameraTv.setOnClickListener(this);
        galleryTv.setOnClickListener(this);
        this.mHelper = helper;
        setCancelable(true);
        setCanceledOnTouchOutside(true);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.camera: {
                mHelper.openCamera();
                dismiss();
            }
            break;

            case R.id.gallery: {
                mHelper.openGallery();
                dismiss();
            }
            break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       Log.i("ab","on activity result");
    }

}
