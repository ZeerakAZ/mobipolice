package com.mobicash.mobipolice.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.MapDialogAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTypeModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ambre on 5/18/2017.
 */

public class MapDialog extends Dialog implements View.OnClickListener {

    private final Context mContext;
    private Button mBtnCancel, mBtnDone;
    private RecyclerView mRvCrimeTypes;

    private MapDialogAdapter mCrimeTypeAdapter;
    private ArrayList<CrimeTypeModel> list;
    private Set<Integer> selectedIds;
    private IDialogDone mListener;

    public MapDialog(Context context, ArrayList<CrimeTypeModel> list, IDialogDone mListener) {
        super(context);

        this.list = list;
        this.mListener = mListener;
        selectedIds = new HashSet<>();
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.maps_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        mRvCrimeTypes = findViewById(R.id.rv_crime_type);
        mBtnDone = findViewById(R.id.btn_done);
        mBtnCancel = findViewById(R.id.btn_cancel);

        mBtnDone.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);



        mCrimeTypeAdapter = new MapDialogAdapter(list, mContext);
        setHeightForRecyclerView(list.size());
        mRvCrimeTypes.setLayoutManager(new LinearLayoutManager(mContext));
        mRvCrimeTypes.setAdapter(mCrimeTypeAdapter);

        setOnShowListener(dialogInterface -> mCrimeTypeAdapter.setmSelectedIds(selectedIds));
    }

    private void setHeightForRecyclerView(int noOfItems) {
        ViewGroup.LayoutParams params = mRvCrimeTypes.getLayoutParams();
        if (noOfItems < 5) {
            params.height = RecyclerView.LayoutParams.WRAP_CONTENT;
        } else {
            params.height = 5 * getContext().getResources()
                    .getDimensionPixelOffset(R.dimen.height_map_row);
        }
        mRvCrimeTypes.setLayoutParams(params);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_done: {
                selectedIds.clear();
                selectedIds.addAll(mCrimeTypeAdapter.getmSelectedIds());
                if (mListener != null)
                    mListener.onSelected(selectedIds);
                dismiss();

            }
            break;

            case R.id.btn_cancel: {
                dismiss();
            }
            break;
        }
    }

    public ArrayList<CrimeTypeModel> getCrimeTypeList() {
        return list;
    }

    public interface IDialogDone {
        void onSelected(Set<Integer> selectedTypeIds);
    }
}
