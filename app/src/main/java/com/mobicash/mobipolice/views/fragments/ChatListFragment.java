package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.ChatUserAdapter;
import com.mobicash.mobipolice.model.dataaccess.adapters.GroupChatAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.GroupModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IRightActionOption;
import com.mobicash.mobipolice.presenter.ChatUserPresenter;
import com.mobicash.mobipolice.views.activities.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatListFragment extends BaseFragment implements
        IRightActionOption,ChatUserPresenter.IRecentChatListener ,
        ChatUserPresenter.IGroupListener{

    public static final int TYPE_GROUP = 2;
    public static final int TYPE_RECENT = 3;
    private static final String ARG_TYPE = "type";
    public static final String TAG = "Chatlist_frag";
    private int mType = -1;


    @BindView(R.id.rvChats)
    RecyclerView mRvChats;
    private ChatUserPresenter mChatUserPresenter;

    public ChatListFragment() {
        // Required empty public constructor
    }

    public static ChatListFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE,type);
        ChatListFragment fragment = new ChatListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null&&getArguments().containsKey(ARG_TYPE))
            mType = getArguments().getInt(ARG_TYPE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_list, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mChatUserPresenter = new ChatUserPresenter(getActivity(),this);
        getData();
    }

    private void getData(){
        switch (mType){
            case TYPE_GROUP:
                getGroups();
                break;
            case TYPE_RECENT:
                //setPagging();
                getRecentChats();
                break;
        }
    }



    private void getGroups(){
        if(getParent()!=null&&getParent().getTabData(ChatRootFragment.GROUP)!=null){
            ArrayList<GroupModel> groups = mChatUserPresenter.parseGroups(getParent().
                    getTabData(ChatRootFragment.GROUP));
            setAdapter(new GroupChatAdapter(groups, ((BaseActivity) getActivity())));
        }
        else {
            mChatUserPresenter.getGroups();
        }
    }

    @Override
    public void onGroupsFetched(ArrayList<GroupModel> groups) {
        setAdapter(new GroupChatAdapter(groups, ((BaseActivity) getActivity())));
    }

    private void getRecentChats(){
        if(getParent()!=null&&getParent().getTabData(ChatRootFragment.RECENT)!=null) {
            ArrayList<UserModel> userModels = mChatUserPresenter.parseUsers(getParent().
                    getTabData(ChatRootFragment.RECENT));
            setAdapter(new ChatUserAdapter(userModels, getContext(),
                    mChatUserPresenter::toChatFragment));
        }
        else {
            mChatUserPresenter.getRecentChat();
        }

    }

    @Override
    public void recentChatsFetched(ArrayList<UserModel> users) {
        setAdapter(new ChatUserAdapter(users, getContext(),
                mChatUserPresenter::toChatFragment));
    }


    private void setAdapter(RecyclerView.Adapter adapter){
        if(mRvChats!=null){
            mRvChats.setLayoutManager(new LinearLayoutManager(getContext()));
            mRvChats.setAdapter(adapter);
        }
    }


    @Override
    public int getRightActionImageId() {
        return R.drawable.ic_action_name;
    }

    @Override
    public void performRightAction() {

    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Nullable
    private ChatRootFragment getParent(){
        if(getParentFragment() instanceof ChatRootFragment)
            return ((ChatRootFragment) getParentFragment());
        return null;
    }
}
