package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;


import com.mobicash.mobipolice.model.dataaccess.interfaces.IPaymentListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SponsorFragment extends BaseFragment {
    @Override
    public String getTagFragment() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }

    /* private Unbinder mUnBinder;
    @BindView(R.id.rv_donation)
    RecyclerView mRvDonations;
    @BindView(R.id.rg_pay_type)
    RadioGroup mRgPayType;
    @BindView(R.id.tv_message_monthly)
    TextView mTvMonthly;
    @BindView(R.id.pb_wait)
    ProgressBar mPbWait;

    private static ArrayList<String> mDonation = new ArrayList<>();
    DonationAdapter adapter;
    private CurrencyBusiness mCurrencyBusiness;

    static {
        mDonation = new ArrayList<>();
        mDonation.add("1");
        mDonation.add("5");
        mDonation.add("10");
        mDonation.add("15");
        mDonation.add("20");
        mDonation.add("25");
        mDonation.add("50");
        mDonation.add("100");
    }

    public SponsorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sponsor, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnBinder = ButterKnife.bind(this, view);
        setRadioButton();
        if (getActivity() instanceof MainActivity) {
            mCurrencyBusiness = ((MainActivity) getActivity()).getmCurrencyBusiness();
            mCurrencyBusiness.getConversationRate();
        }
    }



    @Override
    public void currencyRateFetched() {
        if(getActivity()==null)
            return;
        adapter = new DonationAdapter(getContext(),mDonation,mCurrencyBusiness);
        mRvDonations.setLayoutManager(new GridLayoutManager(getContext(),3));
        mRvDonations.setAdapter(adapter);
        mPbWait.setVisibility(View.GONE);
        mRvDonations.setNestedScrollingEnabled(false);
    }

    private void setRadioButton(){
        mRgPayType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                mTvMonthly.setVisibility(checkedId == R.id.rb_monthly? View.VISIBLE: View.GONE);
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
    }


    @OnClick(R.id.btn_donate)
    public void donate() {
        PaymentDialog paymentDialog = new PaymentDialog(getContext(),
                mTvMonthly.getVisibility()== View.VISIBLE);
        paymentDialog.setmListener(new PaymentDialog.IDialogItemSelected() {
            @Override
            public void itemSelected(PaymentMethod method) {
                String amount = mDonation.get(adapter.getmSelectedPosition());
                switch (method) {
                    case MOBICASH:
                        new MobiCashBusiness(getActivity(), SponsorFragment.this).
                                initializePay(Double.parseDouble(amount));
                        break;
                    case IPAY:
                        new IPayBusiness(getActivity(),SponsorFragment.this).
                                initiatePayment(amount);
                        break;
                    case PAYFAST:
                        new PayFastDialog(getActivity(),
                                getItemForPayment(amount),
                                SponsorFragment.this).show();
                        break;
                    case PAYPAL:
                        new PayPalDialog(getActivity(), amount,
                                SponsorFragment.this).show();
                        break;
                }
            }
        });
        paymentDialog.show();
    }

    private ArrayList<PayFastItemModel> getItemForPayment(String s) {
        double convertedRate = mCurrencyBusiness.getConvertedAmount(Double.parseDouble(s));
        ArrayList<PayFastItemModel> item = new ArrayList<>();
        item.add(new PayFastItemModel("Sponsor",1,convertedRate));
        return item;
    }

    @Override
    public String getName() {
        return "Donation";
    }

    @Override
    public void paymentSuccess() {

    }

    @Override
    public void paymentFailed() {

    }*/
}
