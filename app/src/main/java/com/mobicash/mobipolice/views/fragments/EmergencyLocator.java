package com.mobicash.mobipolice.views.fragments;


import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatSeekBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IMapFragment;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.LogUtility;
import com.mobicash.mobipolice.model.utilities.MapUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.EmergencyLocatorPresenter;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmergencyLocator extends BaseFragment implements  OnMapReadyCallback,
        ILocationUpdated, GoogleMap.OnMarkerClickListener,IMapFragment,
        EmergencyLocatorPresenter.INearByPlacesListener {

    private static final int DEFAULT_ZOOM = 14;
    private static final String TAG ="emergencyLocator";

    private GoogleMap mMap;
    private BottomSheetBehavior mSheetBehavior;
    private EmergencyLocatorPresenter mEmergencyLocatorPresenter;

    private List<String> mPLaceTypes;

    @BindView(R.id.bs_filters)
    View mBSFilters;
    @BindView(R.id.tv_filters)
    TextView mTvFilters;
    @BindView(R.id.sb_distance)
    AppCompatSeekBar mSBDistance;
    @BindView(R.id.tv_distance)
    TextView mTvDistance;
    @BindView(R.id.sp_places)
    Spinner mSpGooglePlace;
    @BindView(R.id.btn_done)
    TextView mBtnDone;
    @BindView(R.id.btn_cancel)
    TextView mBtnCancel;

    public EmergencyLocator() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_emergency_locator, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       mEmergencyLocatorPresenter = new EmergencyLocatorPresenter(getActivity(),this);
        mPLaceTypes = new ArrayList<>(Arrays.asList(getContext().getResources()
                .getStringArray(R.array.places_google_type)));

        MapUtility.loadMapFragment(this,this);
        setBottomSheet();
        setSpinner();
        setSeekBar();

    }

    private void setBottomSheet() {

        mSheetBehavior = BottomSheetBehavior.from(mBSFilters);
        mSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                float moveFactor = (bottomSheet.getHeight() * slideOffset);
                mTvFilters.setAlpha(1 - slideOffset);
            }
        });
    }

    LatLng mUserPos;

    public void updatePosition() {
        Location location = SessionClass.getInstance().getCurrentUserLocation();
        if (location != null) {
            mUserPos = new LatLng(location.getLatitude(),
                    location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mUserPos, DEFAULT_ZOOM));
            addMarker(mUserPos, null);
        } else {
            if (getActivity() instanceof MainActivity)
                ((MainActivity) getActivity()).requestLocation();
        }
    }

    private void addMarker(LatLng position,
                           @Nullable Object object) {
        Marker m = mMap.addMarker(new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.defaultMarker(object == null ? BitmapDescriptorFactory.HUE_GREEN :
                        BitmapDescriptorFactory.HUE_BLUE)));
        if (object != null) {
            m.setTag(object);
        }
    }

    private void killOldMap() {
        SupportMapFragment mapFragment = ((SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.map_fragment));

        if (mapFragment != null) {
            FragmentManager fM = getChildFragmentManager();
            fM.beginTransaction().remove(mapFragment).commit();
        }

    }

    private void setSeekBar() {
        mSBDistance.setMax(50 * 1000);
        mSBDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                float disInKm = (float) i / 1000;
                String distance = String.format(Locale.US, getString(R.string.unit_km), disInKm);
                mTvDistance.setText(distance);
                LogUtility.debugLog(i + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mSBDistance.setHovered(true);
        mSBDistance.setProgress(5000);
    }

    private void setSpinner() {
        final List<String> spinnerItems = new ArrayList<String>(Arrays.asList(getContext().getResources()
                .getStringArray(R.array.places_google)));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.row_spinner,
                R.id.text1, spinnerItems);
        adapter.setDropDownViewResource(R.layout.row_spinner_dropdown);
        mSpGooglePlace.setAdapter(adapter);
        mSpGooglePlace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick(R.id.tv_filters)
    public void showFilterSheet() {
        mSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    @Override
    public void onUpdate(Location location) {
        updatePosition();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        updatePosition();
    }

    @OnClick(R.id.btn_done)
    public void getNearbyLocation() {

        if(mSpGooglePlace.getSelectedItemPosition()==0){
            ToastUtility.showToastForLongTime(getContext(),getString(R.string.message_no_selected_location));
        }else{
            if(mUserPos==null){
                ToastUtility.showToastForLongTime(getContext(),getString(R.string.message_no_location));
                return;
            }
            String type = mPLaceTypes.get(mSpGooglePlace.getSelectedItemPosition()-1);
            double radius = mSBDistance.getProgress();
            mEmergencyLocatorPresenter.
                    getNearByLocation(mUserPos, radius, type);
        }


    }

    @OnClick(R.id.btn_cancel)
    public void collapseBS() {
        if (mSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
            mSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onNearByLocationFetched(ArrayList<LatLng> places) {
        mSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        if (mMap != null)
            mMap.clear();
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
        addMarker(mUserPos, null);
        for (LatLng place : places) {
            addMarker(place,place);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        FragmentUtility.withManager(getFragmentManager())
                .addToBackStack(TAG)
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .intoContainerId(R.id.fl_map_top_Container)
                .addFragment(NavigationFragment.newInstance(MapUtility.getStringFromLatLng(mUserPos),
                        MapUtility.getStringFromLatLng(marker.getPosition())));

        return false;
    }


    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void destroyMap() {
        killOldMap();
    }
}
