package com.mobicash.mobipolice.views.dialogs

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.mobicash.mobipolice.R
import com.mobicash.mobipolice.model.dataaccess.entities.PoliceLocationModel
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType
import com.mobicash.mobipolice.model.utilities.FragmentUtility
import com.mobicash.mobipolice.views.activities.BaseActivity
import com.mobicash.mobipolice.views.fragments.ChatFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_officer_details.*

/**
 * Created by zeerak on 7/8/2018 bt ${File}
 */
class PoliceOfficerDetailDialog(context: Activity, policeOffice: PoliceLocationModel) : Dialog(context) {
    private val mPoliceOfficer = policeOffice
    private val mActivity = context;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_officer_details)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setOnShowListener { setDetails() }
    }

    private fun setDetails() {
        tv_officer_mobile.text = context.getString(R.string.value_officer_mobile, mPoliceOfficer.mobile)
        tv_officer_name.text = context.getString(R.string.value_officer_name, mPoliceOfficer.fullName)
        tv_officer_username.text = context.getString(R.string.value_officer_username, mPoliceOfficer.userName)
        if (mPoliceOfficer.userImage.isNotBlank()) {
            Picasso.with(context).load(mPoliceOfficer.userImage).placeholder(R.drawable.police_logo).into(iv_officer_image)
        }
        btn_chat.setOnClickListener { toChat() }
    }

    private fun toChat() {
        dismiss()
        UserModel(mPoliceOfficer.userId, mPoliceOfficer.userName).run {
            (mActivity as? BaseActivity)?.let {
                FragmentUtility.withManager(it.supportFragmentManager)
                        .intoContainerId(R.id.fl_fragmentContainer)
                        .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                        .addToBackStack("")
                        .replaceToFragment(ChatFragment.newInstance(this, ChatFragment.TYPE_ONE_TO_ONE))
        }


        }

    }


}
