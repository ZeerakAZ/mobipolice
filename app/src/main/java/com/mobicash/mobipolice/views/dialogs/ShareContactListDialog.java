package com.mobicash.mobipolice.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.ShareContactListAdapter;
import com.mobicash.mobipolice.model.utilities.AppUtility;

import java.util.ArrayList;

/**
 * Created by ambre on 6/4/2017.
 */

public class ShareContactListDialog extends Dialog {
    private RecyclerView rvNumbers;

    private Context context;
    private ArrayList<String> list;
    private ShareContactListAdapter adapter;

    private Button btnCancel;


    public ShareContactListDialog(Context context, ArrayList<String> list) {
        super(context);
        this.context = context;
        this.list = list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_share);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        rvNumbers = findViewById(R.id.rv_numbers);
        btnCancel = findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(this::dismissMe);
        setHeightForRecyclerView(list.size());
        rvNumbers.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ShareContactListAdapter(list, context,this::shareContact);
        rvNumbers.setAdapter(adapter);

    }

    private void dismissMe(View view) {
        dismiss();
    }

    private void shareContact(View view, int pos) {
        AppUtility.startSmsScreen(list.get(pos),getContext());
        dismiss();
    }


    private void setHeightForRecyclerView(int noOfItems) {
        ViewGroup.LayoutParams params = rvNumbers.getLayoutParams();
        params.height = 5 * getContext().getResources()
                .getDimensionPixelOffset(R.dimen.height_share_number);
        rvNumbers.setLayoutParams(params);
    }






}
