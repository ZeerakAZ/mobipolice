package com.mobicash.mobipolice.views.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.MapDialogAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTypeModel;
import com.mobicash.mobipolice.model.dataaccess.entities.PlaceObject;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IMapFragment;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IRightActionOption;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.MapUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.CrimeMapPresenter;
import com.mobicash.mobipolice.views.activities.LocationActivity;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.mobicash.mobipolice.views.dialogs.MapDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Zeera on 3/25/2018 bt ${File}
 */

public class CrimeMapFragment extends BaseFragment
        implements  OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener, ILocationUpdated, IMapFragment,IRightActionOption,CrimeMapPresenter.ICrimeListener {

    private static final int REQUEST_PLACE = 100;
    public static final int DEFAULT_ZOOM = 14;
    private static final String TAG = "crime_map_fragment";

    private GoogleMap mMap;
    private CrimeMapPresenter mCrimeMapPresenter;


    MapDialog mCrimeTypeDialog;
    @BindView(R.id.tv_filters)
    TextView filterButton;
    @BindView(R.id.tv_Serach)
    TextView mTvLocationName;



    public CrimeMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crime_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toast.makeText(getContext(), "aa", Toast.LENGTH_SHORT).show();
        MapUtility.loadMapFragment(this,this);
        mCrimeMapPresenter = new CrimeMapPresenter(getActivity(),this);
    }


    @Override
    public void onCrimesFetched(ArrayList<CrimeModel> crimes) {
        addMarker(crimes);
        mCrimeTypeDialog = new MapDialog(getActivity(), mCrimeMapPresenter.getCrimeTypes(),
                this::filterAndAddMarker);

    }

    private void filterAndAddMarker(Set<Integer> selectedTypes) {
        if (mCrimeMapPresenter != null  && mCrimeMapPresenter.getAllCrimes()!=null) {
            mMap.clear();
            ArrayList<CrimeModel> crimes = new ArrayList<>();

            for (CrimeModel crime : mCrimeMapPresenter.getAllCrimes()) {
                if (selectedTypes.contains(crime.crimeType.hashCode()))
                    crimes.add(crime);
            }

            addMarker(selectedTypes.size() == 0 ?
                    mCrimeMapPresenter.getAllCrimes() : crimes);
        }

        updatePositionAndGetCrimes(false);
    }

    private void addMarker(ArrayList<CrimeModel> crimes) {

        for (CrimeModel crime : crimes) {
            LatLng point = new LatLng(crime.getCrimeGPSLatitude(),
                    crime.getCrimeGPSLongitude());
            addMarkerAndInfoWindow(point, crime);
        }
    }



    private void setCrimeTypeDialog(ArrayList<CrimeTypeModel> crimeTypeModels) {

        mCrimeTypeDialog = new MapDialog(getActivity(), crimeTypeModels,
                this::filterAndAddMarker);


    }


    private void addMarkerAndInfoWindow(LatLng position,
                                        @Nullable CrimeModel crimeModel) {
        if (crimeModel == null)
            mMap.addMarker(new MarkerOptions()
                    .position(position)).
                    setIcon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon));
        else {
            float[] hsv = new float[3];
            Color.colorToHSV(Color.parseColor(crimeModel.crimeTypeColor), hsv);
            Marker m = mMap.addMarker(new MarkerOptions()
                    .position(position)
                    .icon(BitmapDescriptorFactory.defaultMarker(hsv[0])));
            m.setTag(crimeModel);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.clear();
        updatePositionAndGetCrimes(true);
    }

    public void updatePositionAndGetCrimes(boolean isToGetCrimes) {
        Location location = SessionClass.getInstance().getCurrentUserLocation();

        if (location != null) {
            if (isToGetCrimes)
                mCrimeMapPresenter.getCrimeFromServer(location.getLatitude(), location.getLongitude());
            LatLng userPos = new LatLng(location.getLatitude(),
                    location.getLongitude());
            if (isToGetCrimes)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userPos, DEFAULT_ZOOM));
            addMarkerAndInfoWindow(userPos, null);
        } else {
            if (getActivity() instanceof MainActivity)
                ((MainActivity) getActivity()).requestLocation();
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        onInfoWindowClick(marker);
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
      /*  if (marker.getTag() != null && marker.getTag() instanceof CrimeModel)
            ((BaseActivity) getActivity()).replaceFragment(CrimeMapDetails.
                    newInstance((CrimeModel) marker.getTag()), R.id.fl_map_top_Container, true);*/
    }

    @OnClick(R.id.tv_Serach)
    public void onSerachClicked() {
        Intent i = new Intent(getActivity(), LocationActivity.class);
        startActivityForResult(i, REQUEST_PLACE);
    }

    @OnClick(R.id.tv_filters)
    public void openDialog() {
        ArrayList<CrimeTypeModel> crimeTypes = mCrimeMapPresenter.getCrimeTypes();

        if (mCrimeTypeDialog != null && crimeTypes != null) {
            if (crimeTypes.size() > 0) {
                mCrimeTypeDialog.show();
            } else {
                ToastUtility.showToastForLongTime(getContext()
                        ,getString(R.string.message_no_crimes_found));
            }
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PLACE:
                    PlaceObject place = data.getExtras().getParcelable(LocationActivity.PLACE_KEY);
                    if (place != null) {
                        mTvLocationName.setText(place.getAddress());
                        moveMapToPosition(place.getLat(), place.getLng(), mMap);
                        mCrimeMapPresenter.getCrimeFromServer(place.getLat(), place.getLng());
                    }
                    break;
            }
        }
    }

    private void moveMapToPosition(double lat, double lng, GoogleMap map) {
        if (map == null) {
            return;
        }
        LatLng latLng = new LatLng(lat, lng);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
    }




    @Override
    public void destroyMap() {
        MapUtility.killMapFragment(this);
    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.statistic_icon;
    }

    @Override
    public void performRightAction() {
        if (mCrimeMapPresenter.getAllCrimes() != null
                && mCrimeMapPresenter.getAllCrimes().size() > 0) {

            FragmentUtility.withManager(getFragmentManager())
                    .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                    .addToBackStack(TAG)
                    .addFragment(CrimeStatisticFragment.newInstance(mCrimeMapPresenter.getCrimeStatistics(),
                            mCrimeMapPresenter.getmTotalCrimes(),
                            mCrimeTypeDialog.getCrimeTypeList()));

        } else {
            ToastUtility.showToastForLongTime(getContext(),getString(R.string.message_no_crimes_found));

        }
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onUpdate(Location location) {
        updatePositionAndGetCrimes(true);
    }


}