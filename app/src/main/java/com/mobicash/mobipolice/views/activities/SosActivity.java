package com.mobicash.mobipolice.views.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.fcm.FirebaseMessaging;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IMessageReceived;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.dataaccess.network.IApiInterface;
import com.mobicash.mobipolice.model.utilities.CircleTransform;
import com.mobicash.mobipolice.model.utilities.DirectionJsonParser;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.GeoCodeHandlerClass;
import com.mobicash.mobipolice.model.utilities.GpsHelper;
import com.mobicash.mobipolice.model.utilities.MapUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.UploadVideoPresenter;
import com.mobicash.mobipolice.views.fragments.ChatFragment;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

public class SosActivity extends BaseActivity implements OnMapReadyCallback, ILocationUpdated, IMessageReceived {

    @BindView(R.id.tv_user_name)
    TextView mTvName;
    @BindView(R.id.tv_user_address)
    TextView mTvAddress;
    @BindView(R.id.iv_user_image)
    ImageView mIvImage;
    @BindView(R.id.bg_main)
    RelativeLayout mClMain;
    @BindView(R.id.tv_senderName)
    TextView mTvSenderName;
    @BindView(R.id.tv_reason)
    TextView mTvReason;
    @BindView(R.id.ll_reason)
    LinearLayout mLlReason;
    @BindView(R.id.iv_chat_icon)
    ImageView mIvChat;
    @BindView(R.id.fab_audio)
    FloatingActionButton mAudio;
    @BindView(R.id.fab_camera)
    FloatingActionButton mCamera;


    private GpsHelper mGpsHelper;
    private GoogleMap mMap;
    private LatLng mFriendPosition;
    private boolean mIsInDanger;
    private int mUserId;
    private String mUserName;
    private UploadVideoPresenter mUploadVideoPresenter;
    private boolean isChatExpand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos);

        MapUtility.loadMapFragment(this,this);

        String userImage = getIntent().getExtras().getString(FirebaseMessaging.KEY_USER_IMAGE);

        mUserName = getIntent().getExtras().getString(FirebaseMessaging.KEY_USERNAME);
        mIsInDanger = getIntent().getExtras().getBoolean(FirebaseMessaging.KEY_ISINDANGER);

        if (mIsInDanger) {
            String reason = getIntent().getExtras().getString(FirebaseMessaging.KEY_REASON);

            mUserId = getIntent().getExtras().getInt(FirebaseMessaging.KEY_USER_ID);
            mTvSenderName.setText(getString(R.string.ph_sos_sender_name,mUserName ));
            mTvReason.setText(getString(R.string.ph_reason_mention, reason));

            mLlReason.setVisibility(View.VISIBLE);
            mAudio.setVisibility(View.VISIBLE);
            mCamera.setVisibility(View.VISIBLE);
        }

        mGpsHelper = new GpsHelper(this);
        mUploadVideoPresenter = new UploadVideoPresenter(this, null, null);
        setBottomView(userImage, mUserName);
    }

    public void setBottomView(String userImage, String userName) {
        if (userImage != null && !userImage.isEmpty())
            Picasso.with(this).load(userImage).
                    placeholder(R.drawable.profile).
                    transform(new CircleTransform()).
                    into(mIvImage);
        mTvName.setText(userName);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (getFriendPosition() != null) {
            mClMain.setBackgroundResource(mIsInDanger ? R.color.red_imdanger : R.color.green);
            if (mIsInDanger)
                requestLocation();
            MapUtility.addMarker(googleMap, getFriendPosition(), null, true, !mIsInDanger);
            //Toast.makeText(this,"Your friend position",Toast.LENGTH_LONG).show();
        }

    }


    public LatLng getFriendPosition() {
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                double lat = getIntent().getExtras().getDouble(FirebaseMessaging.KEY_LATITUDE);
                double lng = getIntent().getExtras().getDouble(FirebaseMessaging.KEY_LONGITUDE);
                mFriendPosition = new LatLng(lat, lng);
                LatLng latLng = mFriendPosition;
                MapUtility.getAddressFromLocation(latLng, this, new GeoCodeHandlerClass(address -> {
                    if (address != null && mTvAddress.getText().toString().isEmpty())
                        mTvAddress.setText(address);
                }));
                return latLng;
            }
        }
        return null;
    }

    public void requestLocation() {
        if (mGpsHelper != null)
            mGpsHelper.askForPermmision(this);
    }

    @Override
    public void onUpdate(Location location) {
        drawPath(new LatLng(location.getLatitude(), location.getLongitude()), mFriendPosition);
    }

    public void drawPath(LatLng start, LatLng end) {
        new DirectionJsonParser(ApiClient.getMapClient().create(IApiInterface.class), mMap,
                start, end, getString(R.string.google_maps_key)).getMapPoints();
    }


    @OnClick(R.id.iv_chat_icon)
    public void showChat() {
        if (isChatExpand) {
            mIvChat.setImageResource(R.drawable.chat_bubble);
            getSupportFragmentManager().popBackStack();
        } else {
            mIvChat.setImageResource(R.drawable.error);
            UserModel userModel = new UserModel(mUserId, mUserName);

            FragmentUtility.withManager(getSupportFragmentManager())
                    .intoContainerId(R.id.fl_fragmentContainer)
                    .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                    .addToBackStack("")
                    .replaceToFragment(ChatFragment.newInstance(userModel, ChatFragment.TYPE_ONE_TO_ONE));


        }
        isChatExpand = !isChatExpand;
    }

    @Override
    public void onBackPressed() {
        if (isChatExpand)
            showChat();
        else
            super.onBackPressed();
    }


    //////

    @OnClick(R.id.fab_camera)
    public void onCamera() {
        mUploadVideoPresenter.openVideoSelectionDialog();

    }

    @OnClick(R.id.fab_audio)
    public void onAudio() {
        mUploadVideoPresenter.openAudioSelectionDialog();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            ToastUtility.showToastForLongTime(this, getString(R.string.message_upload_started));

        }
    }
}
