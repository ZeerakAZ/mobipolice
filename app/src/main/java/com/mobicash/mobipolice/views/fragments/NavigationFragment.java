package com.mobicash.mobipolice.views.fragments;


import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IRightActionOption;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.dataaccess.network.IApiInterface;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.DirectionJsonParser;
import com.mobicash.mobipolice.model.utilities.MapUtility;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationFragment extends BaseFragment
        implements OnMapReadyCallback,ILocationUpdated,IRightActionOption{
    private static final String TAG = "navigation_fragment";

    public NavigationFragment() {
        // Required empty public constructor
    }
    private static final String ARG_START_POINT = "startloc";
    private static final String ARG_END_POINT = "endLoc";
    private String mStartloc,mEndLoc;
    private GoogleMap mMap;

    public static NavigationFragment newInstance(String startPoint, String endPoint) {

        Bundle args = new Bundle();
        args.putString(ARG_START_POINT,startPoint);
        args.putString(ARG_END_POINT,endPoint);
        NavigationFragment fragment = new NavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null && getArguments().containsKey(ARG_START_POINT)){
            mStartloc = getArguments().getString(ARG_START_POINT);
            mEndLoc = getArguments().getString(ARG_END_POINT);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MapUtility.loadMapFragment(this,this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new DirectionJsonParser(ApiClient.getMapClient().create(IApiInterface.class),googleMap,
                mStartloc,mEndLoc,getString(R.string.google_maps_key)).getMapPoints();

    }



    public void updatePosition() {
        Location location = SessionClass.getInstance().getCurrentUserLocation();
        if (location != null) {
            LatLng userPos = new LatLng(location.getLatitude(),
                    location.getLongitude());
            MapUtility.addMarker(mMap,userPos,null,true,true);
        } else {
            if (getActivity() instanceof MainActivity)
                ((MainActivity) getActivity()).requestLocation();
        }
    }


    @Override
    public void onUpdate(Location location) {
        //updatePosition();
    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.route;
    }

    @Override
    public void performRightAction() {
        String point[]= mEndLoc.split(",");
        double lat = Double.parseDouble(point[0]);
        double lng = Double.parseDouble(point[1]);
        AppUtility.startGoogleMapNavigation(lat,lng,getContext());
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
