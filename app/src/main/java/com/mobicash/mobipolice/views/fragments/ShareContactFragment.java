package com.mobicash.mobipolice.views.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.adapters.ShareContactAdapter;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IRightActionOption;
import com.mobicash.mobipolice.model.utilities.AppUtility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShareContactFragment extends BaseFragment implements IRightActionOption{
    private static final String TAG = "shareContactFragment";

    private RecyclerView recyclerView;
    private HashMap<String,ArrayList<String>> contacts;
    private ShareContactAdapter adapter;




    public ShareContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_share_contact, container, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        initiateViews(view);
    }

    private void initiateViews(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recylerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        contacts=new HashMap<>();
        getAllContacts();
    }

    private void getAllContacts() {
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            if(contacts.containsKey(name)){
                contacts.get(name).add(phoneNumber);
            }else{
                ArrayList<String> l=new ArrayList<>();
                l.add(phoneNumber);
                contacts.put(name,l);
            }
        }

        adapter=new ShareContactAdapter(contacts,getActivity());
        recyclerView.setAdapter(adapter);
        phones.close();
    }


    @Override
    public int getRightActionImageId() {
        return R.drawable.ic_action_name;
    }

    @Override
    public void performRightAction() {
        AppUtility.shareText(getContext(), getString(R.string.messsage_share));
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
