package com.mobicash.mobipolice.views.viewsetters;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IRightActionOption;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IleftActionOption;
import com.mobicash.mobipolice.views.activities.BaseActivity;
import com.mobicash.mobipolice.views.customviews.DrawerSetter;
import com.mobicash.mobipolice.views.fragments.BaseFragment;

/**
 * Created by Zeera on 11/19/2017 bt ${File}
 */

public class FragmentActivityHelper{
    private IActivityContract iContract;
    private BaseActivity mHostingActivity;

    private TextView tvHeader;
    private ImageView backIcon;
    private ImageView rightIcon;
    private ImageView leftIcon;
    private RecyclerView mRvMenu;
    private DrawerLayout mDlMain;

    DrawerSetter mDrawerSetter;

    public FragmentActivityHelper(BaseActivity fragmentHostActivity) {

        if(!(fragmentHostActivity instanceof IActivityContract))
            throw new IllegalArgumentException("must implement contract ");
        this.iContract = (IActivityContract) fragmentHostActivity;
        this.mHostingActivity = fragmentHostActivity;
        tvHeader = fragmentHostActivity.findViewById(iContract.getHeaderTitleId());
        backIcon = fragmentHostActivity.findViewById(iContract.getHeaderBackId());
        rightIcon = fragmentHostActivity.findViewById(iContract.getRightIconId());
        leftIcon = fragmentHostActivity.findViewById(iContract.getLeftIcon());
        mRvMenu = fragmentHostActivity.findViewById(R.id.rv_drawer_main);
        mDlMain = fragmentHostActivity.findViewById(R.id.activity_drawer);

        mDrawerSetter = new DrawerSetter(fragmentHostActivity,mDlMain,mRvMenu);

        leftIcon.setOnClickListener(this::leftIconClicked);
    }

    private void leftIconClicked(View v){
        mDrawerSetter.toggleDrawer();
    }


    public void fragmentUpdated(BaseFragment fragment) {
        if(fragment.getId()== R.id.fl_fragmentContainer||
                fragment.getId()==R.id.fl_map_top_Container){
            if (fragment.getTitle()!=null) {
                tvHeader.setText(fragment.getTitle());
            }

            if(fragment instanceof IRightActionOption){
                setRightAction(((IRightActionOption) fragment));
            }else{
                rightIcon.setVisibility(View.GONE);
            }

            if(fragment instanceof IleftActionOption){
                leftIcon.setImageResource(((IleftActionOption) fragment).getLeftActionImageId());
                leftIcon.setOnClickListener(v -> ((IleftActionOption) fragment).performLeftAction());
            }else{

                leftIcon.setOnClickListener(this::leftIconClicked);
            }
        }
    }

    private void setRightAction(IRightActionOption fragment) {
        rightIcon.setVisibility(View.VISIBLE);
        rightIcon.setImageResource(fragment.getRightActionImageId());
        rightIcon.setOnClickListener(v -> fragment.performRightAction());
    }



    public interface IActivityContract{
        int getHeaderTitleId();
        int getHeaderBackId();
        int getRightIconId();
        int getLeftIcon();
    }



}
