package com.mobicash.mobipolice.views.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.validations.EmailValidation;
import com.mobicash.mobipolice.model.dataaccess.validations.EmptyValidation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validation;
import com.mobicash.mobipolice.model.dataaccess.validations.Validator;
import com.mobicash.mobipolice.model.utilities.KeyBoardHelper;
import com.mobicash.mobipolice.presenter.LoginPresenter;
import com.mobicash.mobipolice.views.activities.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment implements KeyBoardHelper.IKeyBoardOpen,LoginPresenter.ILoginListener {
    private static final String TAG = "loginFragment";

    private Unbinder mUnBinder;
    @BindView(R.id.et_username)
    EditText etUserName;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_city)
    EditText etCity;


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }



    @OnClick(R.id.btn_singin)
    public void signUpUser() {
        if (validateFields()) {
            new LoginPresenter(getActivity(),this).FinancialLogin(etUserName.getText().toString(),
                    etPassword.getText().toString(),etCity.getText().toString());
        }
    }

    @Override
    public void userLogin() {
        if(getActivity()!=null){
            if (getActivity() instanceof MainActivity) {
                ((MainActivity) getActivity()).userLoggedIn();
            }
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    public boolean validateFields() {
        ArrayList<Validation> validations = new ArrayList<>();
        validations.add(new EmptyValidation(getString(R.string.error_empty_username),etUserName,null));
        validations.add(new EmptyValidation(getString(R.string.error_empty_password),etPassword,null));
        validations.add(new EmptyValidation(getString(R.string.error_city),etCity,null));
        return new Validator(getContext()).validates(validations);
    }


    @Override
    public void keyBoardStateChanged(boolean isOpen) {
        if(getActivity() instanceof MainActivity){
            ((MainActivity) getActivity()).getMtvBreakingNews().setVisibility(isOpen? View.GONE: View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_Forget_Password)
    public void toForgotPassword(){
       /* ((BaseActivity) getActivity()).replaceFragment(new ForgotPasswordFragment(),
                FragmentAnimationType.GROW_FROM_BOTTOM,getFragmentManager(),R.id.fl_fragmentContainer,true);
        Toast.makeText(getContext(),"Feature not yet implemented", Toast.LENGTH_LONG).show();*/
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}
