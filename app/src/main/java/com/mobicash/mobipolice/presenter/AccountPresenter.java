package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.mobicash.mobipolice.views.dialogs.GenericDialog;
import com.mobicash.mobipolice.views.dialogs.IDialogDouble;
import com.google.android.gms.common.api.Api;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Zeera on 3/17/2018 bt ${File}
 */

public class AccountPresenter extends BasePresenter {
    public AccountPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }

    public void showDeActivateAccountDialog() {
        new GenericDialog(getContext(), getContext().getString(R.string.ph_warning)
                , getContext().getString(R.string.message_deactivate_account),
                getContext().getString(R.string.ph_cancel), getContext().getString(R.string.ph_deactivate), new IDialogDouble() {
            @Override
            public void onRightClick(DialogInterface dialogInterface) {
                deActivateAccount();
            }

            @Override
            public void onLeftClick(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        }).setLogo(R.drawable.dialog_warning).show();
    }

    private void deActivateAccount() {
        Call<ResponseBody> responseBodyCall = ApiClient.getApiClient().inActiveUser(SessionClass.getInstance().getUserId(getContext()));
        getApiCaller().callService(responseBodyCall, APIsEndPoints.INACTIVE_ACCOUNT);
    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint) {
            case APIsEndPoints.INACTIVE_ACCOUNT:
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).logOff();
                    getActivity().onBackPressed();
                }
                break;
            case APIsEndPoints.LOGOFF_USER:
                ((ILogOffListener) getContract()).logOffUser();
                SessionClass.getInstance().setLoginUser(getContext(),null);
                SessionClass.getInstance().setMobiSquidUserInfo(getContext(),null);
                break;
        }
    }

    public void logOffUser() {
        Call<ResponseBody> logOff = ApiClient.getApiClient().logOffUser(SessionClass.getInstance().getUserId(getContext())
                , AppUtility.getUniqueDeviceId(getContext()));
        getApiCaller().callService(logOff, APIsEndPoints.LOGOFF_USER);
    }


    public interface ILogOffListener extends IPresenterContract{
         void logOffUser();
    }
}



