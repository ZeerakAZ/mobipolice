package com.mobicash.mobipolice.presenter

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mobicash.mobipolice.model.dataaccess.entities.AssetBeaconModel
import com.mobicash.mobipolice.model.dataaccess.entities.BeaconModel
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient
import com.mobicash.mobipolice.views.activities.BaseActivity
import org.altbeacon.beacon.Beacon
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by zeerak on 7/7/2018 bt ${File}
 */
class AssetBeaconPresenter(context: Context, contract: IPresenterContract) : BasePresenter(context, contract) {

    fun getAssetDetails(beacons : Collection<Beacon>) {

        val beaconsInternal = ArrayList<BeaconModel>().apply {
            beacons.forEach {
                val uuid = it.id1.toString().replace("-","")
                val major = Integer.parseInt(it.id2.toString())
                val minor = Integer.parseInt(it.id3.toString())
                add(BeaconModel(uuid, minor, major))
            }
        }
        ApiClient.getApiClient().getBeaconStatus(beaconsInternal).let {
            apiCaller.callService(it, APIsEndPoints.GET_BEACON_STATUS)
        }
    }

    override fun onSuccess(body: String?, endPoint: String?) {
        super.onSuccess(body, endPoint)
        when (endPoint) {
            APIsEndPoints.GET_BEACON_STATUS -> {
                (object : TypeToken<ArrayList<AssetBeaconModel>>() {}.type)
                        .let { type ->
                            Gson().fromJson<ArrayList<AssetBeaconModel>>(body,type)
                                    .apply {
                                        (contract as? IBeaconStatusListener)?.beaconStatusFetched(this)
                                    }
                        }
            }
        }
    }

    interface IBeaconStatusListener :IPresenterContract{
        fun beaconStatusFetched(assetBeacons: ArrayList<AssetBeaconModel>?)
    }

}
