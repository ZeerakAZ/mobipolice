package com.mobicash.mobipolice.presenter.services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.mobicash.mobipolice.model.dataaccess.entities.AssetModel;
import com.mobicash.mobipolice.model.utilities.FusedProvider;
import com.mobicash.mobipolice.model.utilities.LogUtility;
import com.mobicash.mobipolice.model.utilities.PermissionsUtitlty;
import com.mobicash.mobipolice.presenter.PoliceLocationPresenter;
import com.mobicash.mobipolice.presenter.StolenAssetPresenter;

import org.joda.time.DateTime;

import java.util.ArrayList;

public class UserLocationUpdateService extends Service implements
        PoliceLocationPresenter.ILocationUpdateListener {

    private NotificationManager mNotificationManager = null;
    private final NotificationCompat.Builder mNotificationBuilder =
            new NotificationCompat.Builder(this);

    private static final int NOTIFICATION = 1;
    public static final String CLOSE_ACTION = "close";
    PowerManager.WakeLock wakeLock;
    private FusedProvider mFusedProvider;
    private PoliceLocationPresenter mLocationPresenter;
    private StolenAssetPresenter mStolenAssetPresenter;

    public static final String KEY_IS_CRIME = "keyCrime";


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");

        mLocationPresenter = new PoliceLocationPresenter(this, this);
        mStolenAssetPresenter = new StolenAssetPresenter(this,null);

        if (!wakeLock.isHeld())
            wakeLock.acquire();

        LogUtility.debugLog("Location service Created");
        //setupNotifications();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtility.debugLog("Location service Started");
        if (PermissionsUtitlty.isLocationPermissionAllowed(this)) {
            mFusedProvider = new FusedProvider(this, fusedLocationListener);
            showNotification();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtility.infoLog("Location service Stop");
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
        try {
            if (mFusedProvider != null)
                mFusedProvider.stopLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //mNotificationManager.cancel(NOTIFICATION);
    }

    public UserLocationUpdateService() {

    }


    private void showNotification() {

        CharSequence displayText = "keeping you safe ";
        mNotificationBuilder
                .setTicker("Scanning for crimes")
                .setContentText(displayText);
        if (mNotificationManager != null) {
            mNotificationManager.notify(NOTIFICATION, mNotificationBuilder.build());
        }
    }


    private com.google.android.gms.location.LocationListener fusedLocationListener =
            location -> {
                LogUtility.infoLog("Location Update Called FUSED" + new DateTime().toString());
                if (location == null)
                    return;
                if (mLocationPresenter != null) {
                    mLocationPresenter. updateLocation(location.getLatitude(), location.getLongitude());
                }
            };

    @Override
    public void onLocationUpdated(ArrayList<AssetModel> assets) {
        LogUtility.infoLog("Location Update on server sir" + new DateTime().toString());
        if (assets != null && assets.size() > 0) {
            LogUtility.debugLog("found "+assets.size()+" stolen assets");
            mStolenAssetPresenter.sendNotificationAndVibrate(assets);
        }
    }


}
