package com.mobicash.mobipolice.presenter.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.AssetBeaconModel;
import com.mobicash.mobipolice.model.essentials.Constants;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.LogUtility;
import com.mobicash.mobipolice.model.utilities.NotificationUtility;
import com.mobicash.mobipolice.model.utilities.ServiceHelper;
import com.mobicash.mobipolice.presenter.AssetBeaconPresenter;
import com.mobicash.mobipolice.views.activities.MainActivity;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;


/**
 * Created by Zeera on 4/5/2017 bt ${File}
 */

public class BeaconUpdateService extends Service implements BeaconConsumer,AssetBeaconPresenter.IBeaconStatusListener{

    public static final String KEY_ASSET_ID = "asset_id";
    public static final String KEY_ASSET_NAME = "asset_name";

    private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
    private final NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this);
    private NotificationManager mNotificationManager = null;

    private static final int NOTIFICATION = 1;
    public static final String CLOSE_ACTION = "close";
    private Queue<String> mBeaconsDetected;
    private double lat, lng;
    private AssetBeaconPresenter mPresenter;
    private NotificationUtility notificationUtility;
    PowerManager.WakeLock wakeLock;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setupNotifications();

        mPresenter = new AssetBeaconPresenter(this,this);
        mBeaconsDetected = new LinkedList<>();
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        notificationUtility = new NotificationUtility(this);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");
        if (!wakeLock.isHeld())
            wakeLock.acquire();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        beaconManager.removeAllRangeNotifiers();
        beaconManager.removeAllMonitorNotifiers();
        beaconManager.unbind(this);
        mNotificationManager.cancel(NOTIFICATION);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        beaconManager.bind(this);
        showNotification();
        return START_STICKY;
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.addRangeNotifier((beacons, region) -> {
            if (beacons.size() > 0) {

                Beacon firstBeacon = beacons.iterator().next();
                LogUtility.debugLog(" Beacon detected",firstBeacon.getId1().toString(),firstBeacon.getId2().toString()
                        ,firstBeacon.getId3().toString());

                if (!mBeaconsDetected.contains(firstBeacon.getId1().toString())/* &&
                        Constants.mBeacons.contains(firstBeacon.getId1().toString().toUpperCase().replace("-", ""))*/) {
                    mPresenter.getAssetDetails(beacons);
                    mBeaconsDetected.add(firstBeacon.getId1().toString());

                    ServiceHelper.stopService(BeaconUpdateService.this, BeaconUpdateService.class);
                }

            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId",
                    Identifier.parse(Constants.APP_UUID)
                    , null, null));
        } catch (RemoteException e) {
        }
    }



    private void setupNotifications() { //called in onCreate()
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0);
        PendingIntent pendingCloseIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .setAction(CLOSE_ACTION),
                0);
        mNotificationBuilder
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentTitle(getText(R.string.app_name))
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setOngoing(true);
    }

    private void showNotification() {
        String rideText = "Searching for Assets...";


        mNotificationBuilder
                .setTicker("Beacon Service")
                .setContentText(rideText);
        if (mNotificationManager != null) {
            mNotificationManager.notify(NOTIFICATION, mNotificationBuilder.build());
        }
    }

    @Override
    public void beaconStatusFetched(@Nullable ArrayList<AssetBeaconModel> assetBeacons) {

        if(assetBeacons!=null){
            for (AssetBeaconModel assetBeacon : assetBeacons) {
                if (assetBeacon.getStatus().equalsIgnoreCase("S")) {
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra(KEY_ASSET_NAME, assetBeacon.getAssetName());
                    intent.putExtra(KEY_ASSET_ID, assetBeacon.getAssetId());

                    String message = getString(R.string.message_asset_detected,assetBeacon.getAssetName());
                    notificationUtility.sendNotification(message,intent);
                }
            }
            AppUtility.vibrateMobile(this, TimeUnit.SECONDS.toMillis(20));
        }

    }
}
