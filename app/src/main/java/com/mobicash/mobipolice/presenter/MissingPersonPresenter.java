package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonModel;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonTipModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by zeerak on 4/8/2018 bt ${File}
 */
public class MissingPersonPresenter extends BasePresenter {

    public MissingPersonPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }

    public void postMissingPerson(MissingPersonModel missingPerson){
        Call<ResponseBody> missingPersonRequest = ApiClient.getApiClient().postMissingPerson(missingPerson);
        getApiCaller().callService(missingPersonRequest, APIsEndPoints.POST_MISSING_PERSON);
    }


    public void getTips(int personId) {
        Call<ResponseBody> missingPersonTip = ApiClient.getApiClient().getMissingPersonTip(personId);
        getApiCaller().callService(missingPersonTip,APIsEndPoints.GET_MISSING_PERSON_TIP);
    }

    public void submitTip(MissingPersonTipModel tip){
        Call<ResponseBody> tipRequest = ApiClient.getApiClient().postMissingPersonTip(tip);
        getApiCaller().callService(tipRequest,APIsEndPoints.POST_MISSING_PERSON_TIP);
    }

    public void markAsFound(int personId){
        Call<ResponseBody> responseBodyCall = ApiClient.getApiClient().markPersonFound(personId);
        getApiCaller().callService(responseBodyCall,APIsEndPoints.MARK_FOUND);
    }

    public void getMissingPerson(int noOfRows,int lastPageId){


        Call<ResponseBody> missingPerson =
               ApiClient.getApiClient().getMissingPersons(noOfRows, lastPageId);
        getApiCaller().callService(missingPerson,APIsEndPoints.GET_MISSING_PERSON);


    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint){
            case APIsEndPoints.POST_MISSING_PERSON:
                if (getContract() instanceof IMissingPersonPostListener) {
                    ((IMissingPersonPostListener) getContract()).onMissingPersonPost();
                }
            break;
            case APIsEndPoints.GET_MISSING_PERSON_TIP:
                ArrayList<MissingPersonTipModel> missingPersonTipModels = parseTipsResponse(body);
                if (getContract() instanceof IMissingTipsListener) {
                    ((IMissingTipsListener) getContract()).onTipsFetched(missingPersonTipModels);
                }
                break;
            case APIsEndPoints.POST_MISSING_PERSON_TIP:

                if (getContract() instanceof IMissingTipsListener) {
                    ((IMissingTipsListener) getContract()).onTipSubmitted();
                }
                break;
            case APIsEndPoints.MARK_FOUND:
                if (getContract() instanceof IMarkFoundListener) {
                    ((IMarkFoundListener) getContract()).onMarkedAsFound();
                }
                break;
            case APIsEndPoints.GET_MISSING_PERSON:
                if (getContract() instanceof IMissingPersonListener) {
                    ((IMissingPersonListener) getContract()).onMissingPersonResponse(body);
                }
                break;
        }
    }

    private ArrayList<MissingPersonTipModel> parseTipsResponse(String body) {
        ArrayList<MissingPersonTipModel> tips = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(body);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                tips.add(new MissingPersonTipModel(obj.getString("Tip"),
                        obj.getString("TipByUserName"),obj.getInt("TipByUserId"),
                        obj.getString("TipByUserImage")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return tips;
    }


    public interface IMissingPersonPostListener extends IPresenterContract{
        void onMissingPersonPost();
    }

    public  interface  IMissingTipsListener extends IPresenterContract{
        void onTipsFetched(ArrayList<MissingPersonTipModel> tips);
        void onTipSubmitted();
    }

    public interface IMarkFoundListener{
        void onMarkedAsFound();
    }

    public interface IMissingPersonListener extends IPresenterContract{
        void onMissingPersonResponse(String body);
    }
}
