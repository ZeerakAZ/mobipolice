package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.model.dataaccess.entities.BreakingNewsModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Zeera on 3/11/2018 bt ${File}
 */

public class BreakingNewPresenter extends BasePresenter {

    public BreakingNewPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);

    }

    public void getBreakNews(){
        getApiCaller().callService(ApiClient.getApiClient().getBreakingNews(USER_TYPE),
                APIsEndPoints.GET_BREAKING_NEWS);

    }

    @Override
    public void onSuccess(String body, String endPoint) {
        switch (endPoint){
            case APIsEndPoints.GET_BREAKING_NEWS:
                Type breakingNewsType = new TypeToken<ArrayList<BreakingNewsModel>>(){}.getType();
                ArrayList<BreakingNewsModel> news = new Gson().fromJson(body,breakingNewsType);

                if (getContract() != null) {
                    ((IBreakNewListener) getContract()).breakingNewsFetched(news);
                }
                break;
        }
    }

    @Override
    public void onError(String error, String endPoint) {

    }

    @Override
    public void onFailed(Throwable e, String endPoint) {

    }

    public interface IBreakNewListener extends IPresenterContract{
         void breakingNewsFetched(ArrayList<BreakingNewsModel> news);
    }

    public static String getMarqueeNews(ArrayList<BreakingNewsModel> news){
        StringBuilder breakingNews = new StringBuilder();
        for (BreakingNewsModel aNew : news) {
            if (breakingNews.length() == 0)
                breakingNews = new StringBuilder(aNew.getNews());
            else
                breakingNews.append("  ").append(aNew.getNews());
        }
        return breakingNews.toString();
    }
}
