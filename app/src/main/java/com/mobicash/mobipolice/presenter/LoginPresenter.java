package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.FinancialLoginModel;
import com.mobicash.mobipolice.model.dataaccess.entities.MobiUserModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UserDeviceToken;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Zeera on 3/27/2018 bt ${File}
 */

public class LoginPresenter extends BasePresenter {
    public static final String COUNTRY = "RWANDA";

    public LoginPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }

    public void LoginUser(String username, String password) {
        Call<ResponseBody> responseBodyCall = ApiClient.getApiClient().loginUser(username, password);
        getApiCaller().callService(responseBodyCall, APIsEndPoints.LOG_IN);
    }

    public void FinancialLogin(String phone, String pin, String city) {
        FinancialLoginModel model = new FinancialLoginModel(pin, phone, "", city,
                new UserDeviceToken(AppUtility.getFCMToken(), AppUtility.getUniqueDeviceId(getActivity())));

        Call<ResponseBody> responseBodyCall = ApiClient.getApiClient().financialLogin(model, COUNTRY);
        getApiCaller().callService(responseBodyCall, APIsEndPoints.FINANCIAL_LOGIN);
    }

    private void postUserToServer(MobiUserModel mobiUserModel) {

        UserModel userModel = mobiUserModel.getUserModel();
        userModel.setUserToken(new UserDeviceToken(AppUtility.getFCMToken(),
                AppUtility.getUniqueDeviceId(getContext())));


        Call<ResponseBody> postUser = ApiClient.getApiClient().postUser(userModel);
        getApiCaller().setmOverrideCallback((body, endPoint) -> {
            if (endPoint.equalsIgnoreCase(APIsEndPoints.POST_USER)) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    int userId = jsonObject.getInt("UserId");

                    userModel.setUserId(userId);
                    SessionClass.getInstance().setLoginUser(getContext(), userModel);
                    if (getContract() instanceof ILoginListener) {
                        ((ILoginListener) getContract()).userLogin();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).callService(postUser, APIsEndPoints.POST_USER);
    }


    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint) {
            case APIsEndPoints.LOG_IN:

                try {
                    JSONObject jRes = new JSONObject(body);
                    if (jRes.has("error") && jRes.getString("error").
                            equalsIgnoreCase("error")) {
                        ToastUtility.showToastForLongTime(getContext(),
                                jRes.getString("message"));
                    } else {
                        MobiUserModel userModel = new GsonBuilder().create().
                                fromJson(jRes.getJSONObject("chat").toString(), MobiUserModel.class);

                        postUserToServer(userModel);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case APIsEndPoints.POST_USER:


                break;
            case APIsEndPoints.FINANCIAL_LOGIN:
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.has("error")&& (!jsonObject.has("User")||jsonObject.isNull("User"))) {
                        ToastUtility.showToastForLongTime(getActivity(), jsonObject.getString("error"));
                    } else {
                        UserModel userModel = new GsonBuilder().create().
                                fromJson(jsonObject.getJSONObject("User").toString(), UserModel.class);

                        SessionClass.getInstance().setLoginUser(getContext(), userModel);
                        if (getContract() instanceof ILoginListener) {
                            ((ILoginListener) getContract()).userLogin();
                        }
                        ToastUtility.showToastForLongTime(getContext(), getContext().getString(R.string.message_user_logged_in));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public interface ILoginListener extends IPresenterContract {
        void userLogin();
    }
}
