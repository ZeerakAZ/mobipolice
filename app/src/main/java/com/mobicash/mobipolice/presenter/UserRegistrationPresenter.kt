package com.mobicash.mobipolice.presenter

import android.app.Activity
import android.provider.Settings.Global.getString
import com.google.gson.JsonObject
import com.mobicash.mobipolice.R
import com.mobicash.mobipolice.model.dataaccess.entities.UserRegisterModel
import com.mobicash.mobipolice.model.dataaccess.interfaces.ICode
import com.mobicash.mobipolice.model.dataaccess.interfaces.ISignInSuccess
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient
import com.mobicash.mobipolice.model.utilities.FireBaseAuth
import com.mobicash.mobipolice.model.utilities.ToastUtility
import org.json.JSONObject

/**
 * Created by zeerak on 6/10/2018 bt ${File}
 */
class UserRegistrationPresenter(activity: Activity, contract: BasePresenter.IPresenterContract?)
    : BasePresenter(activity, contract), ISignInSuccess {


    private var mAuth: FireBaseAuth = FireBaseAuth(activity, this)

    fun checkNumber(number: String) {
        val checkForNumber = ApiClient.getMobiSquidApiClient().checkForNumber(number)
        apiCaller.callService(checkForNumber, APIsEndPoints.CHECK_NUMBER)
    }

    fun registerUser(user: UserRegisterModel) {
        val register = ApiClient.getApiClient().registerFinancialUser(user)
        apiCaller.callService(register, APIsEndPoints.REGISTER_FINANCIAL_USER)
    }

    override fun onSuccess(body: String?, endPoint: String?) {
        super.onSuccess(body, endPoint)
        when (endPoint) {
            APIsEndPoints.CHECK_NUMBER -> {
                val jObject = JSONObject(body)
                val isNotExists = if (jObject.has("message")) {
                    val message = jObject.getString("message")
                    message == "Client not registered"
                } else {
                    false
                }
                if (contract is IUserCheckListener) {
                    (contract as IUserCheckListener).userChecked(isNotExists.not())
                }
            }
            APIsEndPoints.REGISTER_FINANCIAL_USER -> {
                JSONObject(body).let {
                    if (it.has("result") &&
                            it.getString("result") == "fail") {
                        ToastUtility.showToastForLongTime(activity, it.getString("error"))
                    }else{
                        ToastUtility.showToastForLongTime(activity,context.getString(R.string.message_user_registered))
                        activity?.onBackPressed()

                    }

                }
            }
        }
    }

    fun sendVerificationCode(phoneNumber: String, listener: ICode) {
        mAuth.startPhoneNumberVerification(phoneNumber, listener)
    }

    fun verifyCode(code: String) {
        mAuth.verifyPhoneNumberWithCode(code)
    }


    override fun isProceed(isTo: Boolean) {
        if (contract is ICodeVerificationListener) {
            (contract as ICodeVerificationListener).codeVerificationDone(isTo)
        }
    }

    interface IUserCheckListener : IPresenterContract {
        fun userChecked(isExists: Boolean)
    }

    interface ICodeVerificationListener : IPresenterContract {
        fun codeVerificationDone(isVerified: Boolean)
    }

}
