package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.MemberModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UpdateMemberModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.ToastUtility;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zeera on 5/14/2017.
 */

public class UpdateMembersPresenter extends BasePresenter {

    private UpdateMemberModel memberModel;

    public UpdateMembersPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
        setMemberModel();
    }


    private UpdateMembersPresenter setMemberModel() {
        memberModel = new UpdateMemberModel();
        memberModel.entHost = "string";
        memberModel.entHostIP = "string";
        memberModel.entIMEI = "string";
        //memberModel.friendsIds=new int[]{};
        memberModel.userid = SessionClass.getInstance().getUserId(getContext());
        return this;
    }


    public void addMembers(int... id) {
        ArrayList<MemberModel> previousMembers = SessionClass.getInstance().getOldMembers();
        Integer[] list = new Integer[previousMembers == null ? id.length :
                previousMembers.size() + id.length];
        for (int i = 0; i < list.length; i++) {
            if (previousMembers != null && previousMembers.size() > i)
                list[i] = previousMembers.get(i).getId();
            else
                list[i] = id[previousMembers == null ? i : i - previousMembers.size()];
        }
        memberModel.friendsIds = list;
    }

    public void removeMembers(int... id) {
        ArrayList<MemberModel> previousMembers = SessionClass.getInstance().getOldMembers();
        ArrayList<Integer> ids = new ArrayList<>();

        for (MemberModel previousMember : previousMembers) {
            ids.add(previousMember.getId());
        }
        for (int i : id) {
            ids.remove(Integer.valueOf(i));
        }
        memberModel.friendsIds = ids.toArray(new Integer[0]);
    }


    public void updateMembers() {

        Call<ResponseBody> updateMembers = ApiClient.getApiClient().addUpdateMember(memberModel);
        getApiCaller().callService(updateMembers, APIsEndPoints.UPDATE_MEMBER);
    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint) {
            case APIsEndPoints.UPDATE_MEMBER:

                ((IUpdateMemberListener) getContract()).memberUpdated();

                ToastUtility.showToastForLongTime(getContext(),
                        getContext().getString(R.string.message_updated_member));


                break;
        }
    }

    public interface IUpdateMemberListener extends IPresenterContract {
        void memberUpdated();
    }
}
