package com.mobicash.mobipolice.presenter

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mobicash.mobipolice.model.dataaccess.entities.AssetModel
import com.mobicash.mobipolice.model.dataaccess.entities.PoliceLocationModel

import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient
import com.mobicash.mobipolice.model.essentials.SessionClass
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by zeerak on 5/16/2018 bt ${File}
 */
class PoliceLocationPresenter(context: Context, contract: BasePresenter.IPresenterContract?) : BasePresenter(context, contract) {


    fun updateLocation(lat: Double, lng: Double) {
        val loginUser = SessionClass.getInstance().getLoginUser(context)
        val updateLocation = ApiClient.getApiClient().updateLocation(loginUser!!.userid.toString(),
                lat, lng)
        apiCaller.callService(updateLocation,APIsEndPoints.UPDATE_POLICE_LOCATION)
    }

    fun getLocations(lat: Double,lng: Double){
        val policeLocation = ApiClient.getApiClient().getPoliceLocation(lat, lng)
        apiCaller.callService(policeLocation,APIsEndPoints.GET_POLICE_LOCATIONS)

    }
    override fun onSuccess(body: String?, endPoint: String?) {
        super.onSuccess(body, endPoint)
        when (endPoint) {
            APIsEndPoints.UPDATE_POLICE_LOCATION
            -> {
                if (contract != null && contract is ILocationUpdateListener){
                    val type = object : TypeToken<ArrayList<AssetModel>>(){}.type
                    val assets = Gson().fromJson<ArrayList<AssetModel>>(body,type)
                    (contract as ILocationUpdateListener).onLocationUpdated(assets)
                }
            }
            APIsEndPoints.GET_POLICE_LOCATIONS
            -> {
                val type = object : TypeToken<ArrayList<PoliceLocationModel>>() {
                }.type

                val officers =  Gson().fromJson<ArrayList<PoliceLocationModel>>(body,type)
                if(contract!=null && contract is ILocationFetchListener){
                    (contract as ILocationFetchListener).onLocationFetched(officers)
                }
            }

        }
    }


    interface ILocationUpdateListener : BasePresenter.IPresenterContract {
        fun onLocationUpdated(assets:ArrayList<AssetModel>?)
    }

    interface ILocationFetchListener : BasePresenter.IPresenterContract {
        fun onLocationFetched(officersLocations :ArrayList<PoliceLocationModel> )
    }
}
