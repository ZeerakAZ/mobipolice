package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobicash.mobipolice.model.dataaccess.database.DataBasehadlerClass;
import com.mobicash.mobipolice.model.dataaccess.entities.MessageModel;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IMessageReceived;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.dataaccess.network.CallApi;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.DateUtility;
import com.mobicash.mobipolice.model.utilities.LogUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by zeerak on 4/1/2018 bt ${File}
 */
public class ChatPresenter extends BasePresenter {

    private Integer mReceiverId;

    public ChatPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }

    public ChatPresenter(Context context, @Nullable IPresenterContract contract) {
        super(context, contract);
    }

    public ChatPresenter setmReceiverId(Integer mReceiverId) {
        this.mReceiverId = mReceiverId;
        return this;
    }

    public void getMessagesFromServer() {
        Integer lastMessageId = SessionClass.getInstance().getmDbHelper(getContext()).getLastIdOfChat(false);
        Call<ResponseBody> getMessages;
        if (lastMessageId == null) {
            getMessages = ApiClient.getApiClient().getMessages(mReceiverId);
            getApiCaller().callService(getMessages, APIsEndPoints.GET_RECEIVED_MESSAGES);
        } else {
            getMessages = ApiClient.getApiClient().getMessages(SessionClass.getInstance().getUserId(getContext()),
                    mReceiverId, lastMessageId);
            getApiCaller().callService(getMessages, APIsEndPoints.GET_LAST_MESSAGE);
        }
    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        try {
            switch (endPoint) {
                case APIsEndPoints.GET_RECEIVED_MESSAGES:
                case APIsEndPoints.GET_LAST_MESSAGE:
                case APIsEndPoints.GET_ALL_USER_MESSAGES:
                    addMessageToDb(parseMessages(body), mReceiverId, false);
                    break;
                case APIsEndPoints.GET_RECEIVED_MESSAGE_GROUPS:
                    addMessageToDb(parseMessages(body), mReceiverId, true);
                    break;
                case APIsEndPoints.POST_MESSAGE:
                case APIsEndPoints.POST_GROUP_MESSAGE:
                    JSONObject jRes = new JSONObject(body);
                    int chatId = jRes.getInt("ChatId");

                    if (getContract() instanceof IMessageListener) {
                        ((IMessageListener) getContract()).messageSend(chatId);
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<MessageModel> parseMessages(String body) {
        Type type = new TypeToken<ArrayList<MessageModel>>() {
        }.getType();
        return new Gson().fromJson(body, type);

    }

    public void getAllMessageFromServer() {
        Integer lastMessageId = SessionClass.getInstance().getmDbHelper(getContext()).getLastIdOfChat(false);
        Call<ResponseBody> allMesssage = ApiClient.getApiClient().
                getAllMesssage(SessionClass.getInstance().getUserId(getContext()), lastMessageId);

        getApiCaller().callService(allMesssage, APIsEndPoints.GET_ALL_USER_MESSAGES);

    }

    private void addMessageToDb(ArrayList<MessageModel> messageModels, Integer userId, boolean isGr) {
        DataBasehadlerClass dp = SessionClass.getInstance().getmDbHelper(getContext());
        for (MessageModel messageModel : messageModels) {
            messageModel.setRead(false);
            messageModel.setSendingTime(DateUtility.getDateForDB(messageModel.getSendingTime()));
            dp.addMessage(messageModel, isGr);
        }
        updateOneToOne(userId==null?-1:userId);
    }

    public void getAllMessagesOfGroup(Integer groupId) {
        Integer lastMessageId = SessionClass.getInstance().getmDbHelper(getContext()).getLastIdOfChat(true);
        Call<ResponseBody> allMessageOfGroup = ApiClient.getApiClient().getAllMessageOfGroup(groupId, lastMessageId, SessionClass.
                getInstance().getUserId(getContext()));
        getApiCaller().callService(allMessageOfGroup, APIsEndPoints.GET_RECEIVED_MESSAGE_GROUPS);

    }

    public void sendMessage(MessageModel messageModel,MessageModel serverMessage, boolean isGroup) {
        getApiCaller().setLoadingMessage(null);
        Call<ResponseBody> sendMessage;
        if (isGroup) {
            sendMessage = ApiClient.getApiClient().sendGroupMessage(serverMessage);
        } else {
           sendMessage = ApiClient.getApiClient().sendMessage(serverMessage);
        }

        getApiCaller().setmOverrideCallback((body,endpoint) -> {

            if(APIsEndPoints.POST_GROUP_MESSAGE.equals(endpoint)){
                JSONObject jRes = null;
                try {
                    jRes = new JSONObject(body);
                    int chatId = jRes.getInt("ChatId");
                    messageModel.setChatId(chatId);
                    messageModel.setRead(true);
                    SessionClass.getInstance().getmDbHelper(getActivity()).
                            addMessage(messageModel, isGroup);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }).callService(sendMessage, APIsEndPoints.POST_GROUP_MESSAGE);

    }

    public interface IMessageListener extends IPresenterContract {
        void messageSend(int messageId);
    }

    private void updateOneToOne(@Nullable Integer userId) {
        if (getContext() instanceof IMessageReceived) {
            ((IMessageReceived) getContext()).onMessageReceived(userId);
        }
    }


}
