package com.mobicash.mobipolice.presenter

import android.app.Activity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mobicash.mobipolice.R
import com.mobicash.mobipolice.model.dataaccess.entities.DistrictModel
import com.mobicash.mobipolice.model.dataaccess.entities.ProvinceModel
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by zeerak on 6/10/2018 bt ${File}
 */
class MobiProvincePresenter(activity: Activity, contract: IPresenterContract)
    : BasePresenter(activity, contract) {

    var mIsForSpinner: Boolean = false
    private val mDistricts: HashMap<Int, ArrayList<DistrictModel>> = HashMap()

    fun getProvince() {
        val province = ApiClient.getMobiSquidApiClient().province
        apiCaller.callService(province, APIsEndPoints.GET_PROVINCE)
    }

    fun getDistricts(provinceId: Int) {
        mDistricts[provinceId]?.let { districts ->
            if (contract is IDistrictFetchListener) {
                (contract as IDistrictFetchListener).onDistrictFetched(districts)
            }
        }

        if (mDistricts.containsKey(provinceId).not()) {
            val districts = ApiClient.getMobiSquidApiClient().getDistricts(provinceId)
            apiCaller.callService(districts, APIsEndPoints.GET_DISTRICTS)
        }

    }

    override fun onSuccess(body: String?, endPoint: String?) {
        super.onSuccess(body, endPoint)
        try {
            with(JSONObject(body).getJSONArray("return")) {
                when (endPoint) {
                    APIsEndPoints.GET_PROVINCE -> {
                        if (contract != null && contract is IProvinceFetchListener) {
                            val type = object : TypeToken<ArrayList<ProvinceModel>>() {}.type
                            val province = Gson().fromJson<ArrayList<ProvinceModel>>(this.toString(), type)
                            if (mIsForSpinner)
                                province.add(0, ProvinceModel(-1,
                                        activity!!.getString(R.string.select_province),
                                        false))
                            (contract as IProvinceFetchListener).onProvinceFetched(province)
                        }
                    }
                    APIsEndPoints.GET_DISTRICTS -> {
                        if (contract != null && contract is IDistrictFetchListener) {
                            val type = object : TypeToken<ArrayList<DistrictModel>>() {}.type
                            val districts = Gson().fromJson<ArrayList<DistrictModel>>(this.toString(), type)
                            districts[0].run {
                                mDistricts[provinceId] = districts
                            }
                            if (mIsForSpinner)
                                districts.add(0, DistrictModel(-1,
                                        activity!!.getString(R.string.select_districts),
                                        -1, false))
                            (contract as IDistrictFetchListener).onDistrictFetched(districts)
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }


    }

    interface IDistrictFetchListener : IPresenterContract {
        fun onDistrictFetched(districts: ArrayList<DistrictModel>)
    }

    interface IProvinceFetchListener : IPresenterContract {
        fun onProvinceFetched(provinces: ArrayList<ProvinceModel>)
    }
}
