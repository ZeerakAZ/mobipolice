package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.BreakingNewsModel;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTypeModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Zeera on 3/25/2018 bt ${File}
 */

public class CrimeMapPresenter extends BasePresenter {
    private String mCrimeStatistics;
    private int mTotalCrimes;
    private ArrayList<CrimeModel> mAllCrimes;
    private HashMap<String, CrimeTypeModel> crimeTypes;

    public CrimeMapPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }

    public void getCrimeFromServer(double lat,double lng){
        Call<ResponseBody> reportedCrimes = ApiClient.getApiClient().getReportedCrimes(lat, lng);
        getApiCaller().callService(reportedCrimes, APIsEndPoints.GET_CRIMES_BY_LOCATION);

    }
    public void getCrimeFromServer(){
        Call<ResponseBody> reportedCrimes = ApiClient.getApiClient().getReportedCrimes();
        getApiCaller().setLoadingMessage(null);
        getApiCaller().callService(reportedCrimes, APIsEndPoints.GET_CRIMES);
    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint){
            case APIsEndPoints.GET_CRIMES_BY_LOCATION:
                try {

                    JSONObject jRes = new JSONObject(body);
                    JSONArray jCrimes = jRes.getJSONArray("ReportCrimeList");
                    mCrimeStatistics = jRes.getJSONObject("CrimeStatistics").toString();

                    Type crimeModelType = new TypeToken<ArrayList<CrimeModel>>(){}.getType();
                    ArrayList<CrimeModel> crimes = new Gson().fromJson(jCrimes.toString(),crimeModelType);
                    setTypes(crimes);

                    if (getContract() instanceof ICrimeListener) {
                        ((ICrimeListener) getContract()).onCrimesFetched(crimes);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case APIsEndPoints.GET_CRIMES:
                Type crimeModelType = new TypeToken<ArrayList<CrimeModel>>(){}.getType();
                ArrayList<CrimeModel> crimes = new Gson().fromJson(body,crimeModelType);


                if (getContract() instanceof ICrimeListener) {
                    ((ICrimeListener) getContract()).onCrimesFetched(crimes);
                }
                break;
        }
    }


    private void setTypes(ArrayList<CrimeModel> crimes) {
        if (crimes.size() == 0)
            ToastUtility.showToastForLongTime(getContext(),
                    getContext().getString(R.string.message_no_crimes_found));

        mTotalCrimes = crimes.size();
        mAllCrimes = crimes;
        crimeTypes = new HashMap<>();

        for (int i = 0; i < crimes.size(); i++) {
            CrimeModel crime = crimes.get(i);
            if (crime.getCrimeGPSLatitude() != 0) {
                if (!crimeTypes.containsKey(crime.crimeType)) {
                    CrimeTypeModel ctm = new CrimeTypeModel();

                    ctm.setColorCode(crime.crimeTypeColor);
                    ctm.setCrimeTypeId(crime.crimeType.hashCode());
                    ctm.setCrimeTypeName(crime.crimeType);

                    crimeTypes.put(crime.crimeType, ctm);
                }
            }
        }


    }

    public ArrayList<CrimeModel> getAllCrimes() {
        return mAllCrimes;
    }

    public ArrayList<CrimeTypeModel> getCrimeTypes() {
        if (crimeTypes==null) {
            return null;
        }
        return new ArrayList<>(crimeTypes.values());
    }

    public String getCrimeStatistics() {
        return mCrimeStatistics;
    }

    public int getmTotalCrimes() {
        return mTotalCrimes;
    }

    public interface ICrimeListener extends IPresenterContract{
        void onCrimesFetched(ArrayList<CrimeModel> crimes);
    }


}
