package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobicash.mobipolice.model.dataaccess.adapters.WantedAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTipModel;
import com.mobicash.mobipolice.model.dataaccess.entities.WantedModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;

import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by zeerak on 4/8/2018 bt ${File}
 */
public class WantedPresenter extends BasePresenter {

    public WantedPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }

    public void getWanted(){
        Call<ResponseBody> wantedRequest = ApiClient.getApiClient().getWanted();
        getApiCaller().callService(wantedRequest, APIsEndPoints.GET_WANTED);
    }

    public void submitTip(CrimeTipModel model){
        Call<ResponseBody> submitTip = ApiClient.getApiClient().submitTip(model);
        getApiCaller().callService(submitTip,APIsEndPoints.POST_TIP);
    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint){
            case APIsEndPoints.GET_WANTED:
                Type type = new TypeToken<ArrayList<WantedModel>>(){}.getType();
                ArrayList<WantedModel> wantedUsers = new Gson().fromJson(body, type);

                if (getContract() instanceof IWantedListener) {
                    ((IWantedListener) getContract()).onWantedFetched(wantedUsers);
                }
                break;
            case APIsEndPoints.POST_TIP:
                if (getContract() instanceof ICrimeTipListener) {
                    ((ICrimeTipListener) getContract()).onTipSubmitted();
                }
                break;
        }
    }

    public interface IWantedListener extends IPresenterContract{
        void onWantedFetched(ArrayList<WantedModel> wantedModels);
    }

    public interface ICrimeTipListener extends IPresenterContract{
        void onTipSubmitted();
    }
}
