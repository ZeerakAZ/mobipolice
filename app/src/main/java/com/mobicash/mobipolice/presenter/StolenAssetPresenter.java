package com.mobicash.mobipolice.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.AssetModel;
import com.mobicash.mobipolice.model.utilities.NotificationUtility;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by zeerak on 5/27/2018 bt ${File}
 */
public class StolenAssetPresenter extends BasePresenter {
    public static final String KEY_FROM_STOLEN_ASSET = "from_stolen_asset";
    public static final String KEY_STOLEN_ASSETS = "stolen_assets";

    private static final long VIBRATION_DURATION = TimeUnit.SECONDS.toMillis(2);

    private NotificationUtility mNotificationUtility;

    public StolenAssetPresenter(Context context, @Nullable IPresenterContract contract) {
        super(context, contract);
        mNotificationUtility = new NotificationUtility(context);
    }

    public void sendNotificationAndVibrate(ArrayList<AssetModel> assetModels){
        StringBuilder message = new StringBuilder(getContext().getString(R.string.message_stolen_asset));
        for (AssetModel assetModel : assetModels) {
            message.append(" \nStolen item : ").append(assetModel.getName())
                    .append(" \nReported by : ").append(assetModel.getOwner().getOwnerFullName());
        }

        Intent intent = new Intent(getContext(), StolenAssetPresenter.class);
        intent.putExtra(KEY_FROM_STOLEN_ASSET, true);
        intent.putExtra(KEY_STOLEN_ASSETS,assetModels);

        mNotificationUtility.sendNotification(message.toString(),intent);

        Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if(v!=null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(VIBRATION_DURATION,VibrationEffect.DEFAULT_AMPLITUDE));
            }else{
                //deprecated in API 26
                v.vibrate(VIBRATION_DURATION);
            }
        }
    }



}
