package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTypeModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.LogUtility;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zeerak on 4/7/2018 bt ${File}
 */
public class CrimeTipPresenter extends BasePresenter {

    public CrimeTipPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }

    public void getCrimesTypes(){
        Call<ResponseBody> getCategories = ApiClient.getApiClient().getCrimeTypes();
        getApiCaller().callService(getCategories, APIsEndPoints.GET_CRIME_TYPES);
    }


    public void postCrime(CrimeModel crimeModel) {
        Call<ResponseBody> postCrime = ApiClient.getApiClient().postCrime(crimeModel);
        getApiCaller().callService(postCrime,APIsEndPoints.REPORT_CRIME);
    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint){
            case APIsEndPoints.GET_CRIME_TYPES:
                Type type = new TypeToken<ArrayList<CrimeTypeModel>>(){}.getType();
                ArrayList<CrimeTypeModel> crimeTypeModels = new Gson().fromJson(body,type);

                if (getContract() instanceof ICrimeTypeListener) {
                    ((ICrimeTypeListener) getContract())
                            .onCrimeTypesFetched(crimeTypeModels);
                }

                break;
            case APIsEndPoints.REPORT_CRIME:
                Toast.makeText(getContext(), R.string.message_crime_posted, Toast.LENGTH_LONG).show();
                SessionClass.getInstance().setCrimeModel(null);
                if (getContract() instanceof ICrimeUpdateListener) {
                    ((ICrimeUpdateListener) getContract()).onCrimePosted();
                }

                break;
        }
    }

    public void updateCrimeOnUpload(String serverPath,String localPath){
        LogUtility.debugLog("Uploading crime after file Upload");
        CrimeModel crimeModel = SessionClass.getInstance().getCrimeModel();
        if(crimeModel!=null&&crimeModel.getLocalFilePath().equalsIgnoreCase(localPath)){
            crimeModel.setCrimeFileURL(serverPath);
            postCrime(crimeModel);
        }
    }

    public interface ICrimeTypeListener extends IPresenterContract{
        void onCrimeTypesFetched(ArrayList<CrimeTypeModel> models);
    }

    public interface ICrimeUpdateListener extends IPresenterContract{
        void onCrimePosted();
    }
}
