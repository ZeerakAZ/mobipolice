package com.mobicash.mobipolice.presenter;


import com.mobicash.mobipolice.model.utilities.LogUtility;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import retrofit2.Call;

/**
 * Created by Zeera on 1/7/2018 bt ${File}
 */

public class TwitterPresenter {


    public void login(TwitterLoginButton button, final ITwitterResponse response){

        button.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                LogUtility.debugLog(result.toString());
                if(response!=null)
                    response.loggedIn();
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
                LogUtility.debugLog(e.getMessage());
            }
        });
        button.performClick();
    }

    public static void postTweet(String message){
        TwitterApiClient client = TwitterCore.getInstance().getApiClient();
        StatusesService statusesService = client.getStatusesService();

        Call<Tweet> call = statusesService.update(message,null,
                false, null, null,null,
                false,false,null);
        call.enqueue(new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> result) {
                //Do something with result
                LogUtility.debugLog(result.toString());
            }

            public void failure(TwitterException exception) {
                //Do something on failure
                exception.printStackTrace();
            }
        });
    }

    public interface ITwitterResponse{
        void loggedIn();
    }

}
