package com.mobicash.mobipolice.presenter;

import android.net.Uri;

import com.mobicash.mobipolice.model.dataaccess.enums.SosType;
import com.mobicash.mobipolice.views.fragments.BaseFragment;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import org.json.JSONObject;

import java.util.Collections;

/**
 * Created by Zeera on 1/6/2018 bt ${File}
 */
public class FacebookPresenter {
    private BaseFragment fragment;
    CallbackManager mCallBackManager;

    public FacebookPresenter(BaseFragment fragment) {
        this.fragment = fragment;
        mCallBackManager = CallbackManager.Factory.create();

    }


    public static boolean isLoggedIn() {
        return AccessToken.getCurrentAccessToken() != null;
    }

    public void login() {
        LoginManager.getInstance().logInWithReadPermissions(fragment, Collections.singletonList("public_profile,publish_actions"));
        LoginManager.getInstance().registerCallback(mCallBackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                SosType.SOS_FACEBOOK.setCheck(fragment.getActivity(),true);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    public void shareOnFacebook(String message, String link) {
       /* ShareLinkContent content = new ShareLinkContent.Builder()
                .setQuote("some qute")
                .setContentUrl(Uri.parse("https://developers.facebook.com"))
                .build();*/
       ShareLinkContent linkContent = new ShareLinkContent.Builder().
               setQuote(message).setContentUrl(Uri.parse(link)).build();
       ShareDialog.show(fragment.getActivity(),linkContent);

       /* GraphRequest.newPostRequest(AccessToken.getCurrentAccessToken(),
                "me/feed"
                , getShareJson(message,link), new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if (fragment.getActivity() != null) {
                            LogUtility.debugLog(fragment.getActivity().getString(R.string.message_share_fb));
                            Toast.makeText(fragment.getActivity(), fragment.getActivity()
                                    .getString(R.string.message_share_fb), Toast.LENGTH_SHORT).show();
                        }

                    }
                }).executeAsync();*/

    }

    public CallbackManager getmCallBackManager() {
        return mCallBackManager;
    }


    private JSONObject getShareJson(String message, String link) {
        JSONObject shareJson = null;
        try {
            shareJson = new JSONObject();
            shareJson.put("message", message);
            shareJson.put("link",link);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return shareJson;
    }


}
