package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.model.dataaccess.entities.MemberModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.essentials.SessionClass;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Zeera on 3/18/2018 bt ${File}
 */

public class MemberPresenter extends BasePresenter {

    public MemberPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }


    public void getMembers() {
        Call<ResponseBody> getMember = ApiClient.getApiClient()
                .getMembers(SessionClass.getInstance().getUserId(getContext()));
        getApiCaller().callService(getMember, APIsEndPoints.GET_MEMBERS);
    }

    public void searchMember(String name) {
        getApiCaller().setLoadingMessage(null);
        Call<ResponseBody> searchName = ApiClient.getApiClient()
                .searchFriend(name);
        getApiCaller().callService(searchName, APIsEndPoints.SEARCH_FRIENDS);
    }


    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint) {
            case APIsEndPoints.GET_MEMBERS:
            case APIsEndPoints.SEARCH_FRIENDS:
                try {
                    ArrayList<MemberModel> members = parseMembers(body);

                    if (getContract() instanceof IGetMemberListener) {
                        ((IGetMemberListener) getContract()).onMemberFetched(members);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }
    }

    @NonNull
    private ArrayList<MemberModel> parseMembers(String body) throws JSONException {
        JSONArray array = new JSONArray(body);
        ArrayList<MemberModel> members = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            MemberModel member = new MemberModel();
            member.setId(array.getJSONObject(i).getInt("FriendUserId"));
            member.setImage(array.getJSONObject(i).getString("FriendImage"));
            member.setName(array.getJSONObject(i).getString("FriendName"));
            members.add(member);
        }
        return members;
    }

    public interface IGetMemberListener extends IPresenterContract{
        void onMemberFetched(ArrayList<MemberModel> members);
    }
}
