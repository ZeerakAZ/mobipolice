package com.mobicash.mobipolice.presenter.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.dataaccess.network.CallApi;
import com.mobicash.mobipolice.model.dataaccess.network.IApiInterface;
import com.mobicash.mobipolice.model.dataaccess.network.IResponse;
import com.mobicash.mobipolice.model.utilities.FusedProvider;
import com.mobicash.mobipolice.model.utilities.LogUtility;
import com.mobicash.mobipolice.model.utilities.NotificationUtility;
import com.mobicash.mobipolice.model.utilities.PermissionsUtitlty;
import com.mobicash.mobipolice.views.activities.MainActivity;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Locale;
import java.util.Random;


public class CrimeLookUpService extends Service {

    private NotificationManager mNotificationManager = null;
    private final NotificationCompat.Builder mNotificationBuilder =
            new NotificationCompat.Builder(this);

    private static final int NOTIFICATION = 1;
    public static final String CLOSE_ACTION = "close";
    PowerManager.WakeLock wakeLock;
    private FusedProvider mFusedProvider;
    private NotificationUtility notificationUtility;
    public static final String KEY_IS_CRIME = "keyCrime";


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");
        notificationUtility = new NotificationUtility(this);
        if (!wakeLock.isHeld())
            wakeLock.acquire();

        LogUtility.debugLog("Service Created");
        setupNotifications();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtility.debugLog("Service Started");
        if (PermissionsUtitlty.isLocationPermissionAllowed(this)) {
            mFusedProvider = new FusedProvider(this,fusedLocationListener);
            showNotification();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtility.infoLog("Service Stop");
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
        try {
            if(mFusedProvider!=null)
                mFusedProvider.stopLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mNotificationManager.cancel(NOTIFICATION);
    }

    public CrimeLookUpService() {

    }

    private void setupNotifications() { //called in onCreate()
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0);
        PendingIntent pendingCloseIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .setAction(CLOSE_ACTION),
                0);
        mNotificationBuilder
                .setSmallIcon(R.drawable.logo_new)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentTitle(getText(R.string.app_name))
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setOngoing(true);
    }

    private void showNotification() {

        CharSequence displayText = "keeping you safe ";
        mNotificationBuilder
                .setTicker("Scanning for crimes")
                .setContentText(displayText);
        if (mNotificationManager != null) {
            mNotificationManager.notify(NOTIFICATION, mNotificationBuilder.build());
        }
    }

    private com.google.android.gms.location.LocationListener fusedLocationListener =
            location -> {
                LogUtility.infoLog("Location Update Called FUSED" +new DateTime().toString());
                if (location == null)
                    return;
                getCrimeFromServer(location.getLatitude(),location.getLongitude());
            };

    public void getCrimeFromServer(Double lat, Double lng) {
       LogUtility.debugLog("Getting crimes");
        CallApi crimeStatistics = new CallApi(this, new IResponse() {
            @Override
            public void onSuccess(String body, String endPoint) {
                try {
                    String crimeStatistics = new JSONObject(body).getJSONObject("CrimeStatistics").toString();

                    JSONObject jCrimes = new JSONObject(crimeStatistics);
                    Iterator<?> keys = jCrimes.keys();
                    StringBuilder notificationMessage = new StringBuilder();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        int numbers = jCrimes.getInt(key);

                        notificationMessage.append(getNotificationMessage(key, numbers));
                    }

                    Intent intent = new Intent(CrimeLookUpService.this, MainActivity.class);
                    intent.putExtra(KEY_IS_CRIME, true);
                    notificationUtility.sendNotification(notificationMessage.toString(), intent);
                } catch (JSONException e) {

                }
            }

            @Override
            public void onError(String error, String endPoint) {

            }

            @Override
            public void onFailed(Throwable e, String endPoint) {

            }
        });

        crimeStatistics.callService(ApiClient.getApiClient().getReportedCrimes(lat,lng),
                APIsEndPoints.GET_CRIMES_BY_LOCATION);
    }
    private String getNotificationMessage(String name, int count){
        return String.format(Locale.ENGLISH,"There is %d %s crimes in 100 meters " +
                "radius of you location.Be careful!\n",count,name);
    }


}
