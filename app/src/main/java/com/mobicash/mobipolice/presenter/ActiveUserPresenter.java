package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.CountryUserModel;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

/**
 * Created by Zeera on 3/11/2018 bt ${File}
 */

public class ActiveUserPresenter extends BasePresenter {


    public ActiveUserPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }


    public void getActiveUser(){
        getApiCaller().callService(ApiClient.getApiClient().getActiveUser(USER_TYPE),
                APIsEndPoints.GET_ACTIVE_USER);
    }


    @Override
    public void onSuccess(String body, String endPoint) {
       switch (endPoint){
           case APIsEndPoints.GET_ACTIVE_USER:
               parseActiveUserCount(body);
               break;
       }

    }

    private void parseActiveUserCount(String body) {
        LinkedHashMap<String,CountryUserModel> countriesMap = new LinkedHashMap<>();
        try {
            JSONObject jObj = new JSONObject(body);
            JSONArray jCountriesUser = jObj.getJSONArray("CountryWiseUsers");
            int totalUserCount = jObj.getInt("TotalUserCount");
            for (int i = 0; i < jCountriesUser.length(); i++) {
                JSONObject jCountryUser = jCountriesUser.getJSONObject(i);
                String countryName = jCountryUser.getString("Country");
                String count = jCountryUser.getString("Count");
                countriesMap.put(countryName,new CountryUserModel(countryName,count));
            }
            setFlagsDrawable(countriesMap);

            if (getContract() != null) {
                ((IActiveUserFetchListener) getContract()).
                        activeUser(new ArrayList<>(countriesMap.values()),totalUserCount);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setFlagsDrawable(LinkedHashMap<String,CountryUserModel> countryMap) {
        String[] rl = getContext().getResources().getStringArray(R.array.CountryCodes);
        for (int i = rl.length - 1; i >= 0; i--) {
            String[] c = rl[i].split(",");
            if (c.length == 4) {
                String countryName = c[0];
                if(countryMap.containsKey(countryName)){
                    CountryUserModel countryUserModel = countryMap.get(countryName);
                    countryUserModel.setCountryFlag(c[3]);
                }
            }
        }

    }

    @FunctionalInterface
    public interface IActiveUserFetchListener extends IPresenterContract{
        void activeUser(ArrayList<CountryUserModel> users,int count);
    }
}
