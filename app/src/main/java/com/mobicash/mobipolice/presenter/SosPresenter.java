package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.app.Dialog;
import android.location.Location;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.enums.SosType;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.essentials.SharedPrefManager;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.GeoCodeHandlerClass;
import com.mobicash.mobipolice.model.utilities.MapUtility;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.views.dialogs.GenericDialog;
import com.mobicash.mobipolice.views.dialogs.ReasonDialog;
import com.mobicash.mobipolice.views.fragments.BaseFragment;
import com.mobicash.mobipolice.views.fragments.SOSSettingFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zeera on 3/16/2018 bt ${File}
 */

public class SosPresenter extends BasePresenter {
    @IntDef({SOS_TYPE.SOS_FAMILY,SOS_TYPE.SOS_POLICE,SOS_TYPE.SOS_SAFE})
    public @interface SOS_TYPE{
        int SOS_FAMILY = 0x01;
        int SOS_POLICE = 0x02;
        int SOS_SAFE = 0x03;
    }

    private final BaseFragment mfragment;
    private String mAddress;
    private boolean isAddressFetchCalled;
    private Location mLocation;



    public SosPresenter(Activity activity, BaseFragment fragment,
                        @Nullable IPresenterContract contract) {
        super(activity, contract);
        mfragment = fragment;
    }

    public SosPresenter Location(Location mLocation) {
        this.mLocation = mLocation;
        return this;
    }

    public void sendSos(@SOS_TYPE int type){
        if(mLocation==null)
            ToastUtility.showToastForLongTime(getContext(),
                    getContext().getString(R.string.message_no_location));
        switch (type) {

            case SOS_TYPE.SOS_FAMILY:
                showReasonDialog(true);
                break;
            case SOS_TYPE.SOS_POLICE:
                sendSosPolice();
                break;
            case SOS_TYPE.SOS_SAFE:
                sendSosToFamily("",false);
                break;
        }
    }

    private void sendSosPolice(){
        Call<ResponseBody> sosCall = ApiClient.getApiClient()
                .sendSoSToPoliceAll( "In danger"
                        , mLocation.getLatitude(), mLocation.getLongitude(), true);

        getApiCaller().callService(sosCall, APIsEndPoints.SEND_SOS_TO_POLICE_ALL);
    }

    private void showReasonDialog(boolean isInDanger) {
        boolean isAny = false;
        for (SosType sos_type : SosType.values()) {
            if (sos_type.getCheck(getContext())) {
                isAny = true;
                break;
            }
        }
        if (!isAny) {
            new GenericDialog(getContext(),
                    getContext().getString(R.string.ph_warning)
                    ,getContext().getString(R.string.message_no_sos),
                    getContext().getString(R.string.ph_ok),null)
                    .setLogo(R.drawable.dialog_warning).show();

        } else {
            ReasonDialog reasonDialog = new ReasonDialog(getContext());
            reasonDialog.setmListener(reason -> sendSosToAllSetting(reason,isInDanger));
            reasonDialog.show();
        }
    }

    private void sendSosToAllSetting(String reason,boolean isInDanger) {
        fetchAddress(reason,isInDanger);
    }

    private void fetchAddress(final String reason, boolean isInDanger) {
        if (mAddress == null && !isAddressFetchCalled) {
            isAddressFetchCalled = true;

            LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            MapUtility.getAddressFromLocation(latLng, getContext(),
                    new GeoCodeHandlerClass(address -> {
                        isAddressFetchCalled = false;
                        if(address!=null){
                            mAddress = address;
                            sendSOSWithAddress(reason,isInDanger);
                        }
                    }));
        }else if(mAddress!=null){
            sendSOSWithAddress(reason,isInDanger);
        }
    }

    private void sendSOSWithAddress(String reason,boolean  isInDanger) {
        for (SosType sos_type : SosType.values()) {
            if(!sos_type.getCheck(getContext()))
                return;
            switch (sos_type) {
                case SOS_FACEBOOK:
                    shareSosOnFaceBook(reason, mAddress);
                    break;
                case SOS_TWITTER:
                    shareSosOnTwitter(reason,mAddress);
                    break;
                case SOS_FAMILY:
                    sendSosToFamily(reason,isInDanger);
                    break;
            }
        }

    }

    private void sendSosToFamily(String reason,boolean isInDanger) {
        Call<ResponseBody> sosCall = ApiClient.getApiClient()
                .sendSoSToFamily(SessionClass.getInstance().getUserId(getContext()), reason
                        , mLocation.getLatitude(), mLocation.getLongitude(), isInDanger);

        getApiCaller().callService(sosCall, APIsEndPoints.SEND_SOS_TO_FRIENDS);
    }

    private void shareSosOnTwitter(String reason, String address) {
        StringBuilder message = new StringBuilder();
        String link = String.format(Locale.ENGLISH, "https://www.google.com/maps/place/%f,%f",
                mLocation.getLatitude()
                , mLocation.getLongitude());
        message.append(SessionClass.getInstance().getMobiUserName(getContext()))
                .append(" is in danger!\n")
                .append("Crime Type : ").append(reason)
                .append("\nFull Location\n")
                .append(address).append("\n\n").append(link)
                .append("\n\nSOS id : ").append(System.currentTimeMillis());
        TwitterPresenter.postTweet(message.toString());

        ((ISosListener) getContract()).sosSend(SosType.SOS_TWITTER,true);
    }

    private void shareSosOnFaceBook(String reason, String address) {
        StringBuilder message = new StringBuilder();
        message.append(SessionClass.getInstance().getMobiUserName(getContext()))
                .append(" is in danger!\n")
                .append("Crime Type : ").append(reason)
                .append("\nFull Location\n")
                .append(address);

        String link = AppUtility.getStaticMapUrl(mLocation);
        new FacebookPresenter(mfragment)
                .shareOnFacebook(message.toString(), link);

        ((ISosListener) getContract()).sosSend(SosType.SOS_FACEBOOK,true);

    }

    public interface ISosListener extends IPresenterContract{
        void sosSend(SosType type,boolean isSuccessful);
    }

    public void saveSosMessage(){
        String message = SharedPrefManager.getInstance(getContext())
                .getStringByKey(SOSSettingFragment.SOS_MESSAGE);
        if(message!=null&&!message.isEmpty()){

            Call<ResponseBody> updateSos = ApiClient.getApiClient()
                    .updateSosAlertMessage(SessionClass.getInstance().getUserId(getContext()), message);
            getApiCaller().callService(updateSos,APIsEndPoints.UPDATE_SOS_MESSAGE);
        }

    }

    public void fetchSosFromServer(){

        Call<ResponseBody> fetchUserDeatials = ApiClient.getApiClient()
                .getUserDetails(SessionClass.getInstance().getUserId(getContext()));
        getApiCaller().callService(fetchUserDeatials,APIsEndPoints.GET_USER_DETAILS);

    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint){
            case APIsEndPoints.GET_USER_DETAILS:

                try {
                    JSONArray jsonArray = new JSONArray(body);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String sosAlertMessage = jsonObject.getString("SOSAlertMessage");
                    SharedPrefManager.getInstance(getContext()).setStringForKey(sosAlertMessage, SOSSettingFragment.SOS_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
