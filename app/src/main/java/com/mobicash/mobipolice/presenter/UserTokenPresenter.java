package com.mobicash.mobipolice.presenter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.model.dataaccess.entities.UserDeviceToken;
import com.mobicash.mobipolice.model.dataaccess.fcm.FirebaseToken;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.LogUtility;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by zeerak on 4/2/2018 bt ${File}
 */
public class UserTokenPresenter extends BasePresenter{

    public UserTokenPresenter(Context context, @Nullable IPresenterContract contract) {
        super(context, contract);
    }
    public void updateToken(String token) {
        Call<ResponseBody> updateToken = ApiClient.getApiClient().updateToken(SessionClass.
                        getInstance().getUserId(getContext()),
                new UserDeviceToken(token, AppUtility.getUniqueDeviceId(getContext())));
        updateToken.request().header("Content-Type: text/json");

        getApiCaller().callService(updateToken, APIsEndPoints.UPDATE_TOKEN);



    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint){
            case APIsEndPoints.UPDATE_TOKEN:
                LogUtility.debugLog("token Updated");
                break;
        }
    }
}
