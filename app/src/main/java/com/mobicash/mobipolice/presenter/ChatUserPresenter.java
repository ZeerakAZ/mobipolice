package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobicash.mobipolice.model.dataaccess.entities.GroupModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.model.utilities.PagingUtility;
import com.mobicash.mobipolice.views.fragments.ChatFragment;
import com.mobicash.mobipolice.views.fragments.ChatListFragment;

import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by zeerak on 4/2/2018 bt ${File}
 */
public class ChatUserPresenter extends BasePresenter {
    private static final int NO_OF_ROWS = 100;

    public ChatUserPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
        getApiCaller().setLoadingMessage(null);
    }

    public void getRecentChat() {
        Call<ResponseBody> recentChats = ApiClient.getApiClient().getRecentChats(SessionClass.
                getInstance().getUserId(getContext()), NO_OF_ROWS);
        getApiCaller().callService(recentChats, APIsEndPoints.GET_RECENT_CHAT_USER);
    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint) {
            case APIsEndPoints.GET_RECENT_CHAT_USER:
                ArrayList<UserModel> users = parseUsers(body);
                if (getContract() instanceof IRecentChatListener) {
                    ((IRecentChatListener) getContract()).recentChatsFetched(users);
                }
                break;
            case APIsEndPoints.GET_GROUP_CHATS:
                ArrayList<GroupModel> groups = parseGroups(body);

                if (getContract() instanceof IGroupListener) {
                    ((IGroupListener) getContract()).onGroupsFetched(groups);
                }
                break;
            case APIsEndPoints.POST_FAV_USER:
                if (getContract() instanceof IMemberFavListener) {
                    ((IMemberFavListener) getContract()).memberAddedToFav();
                }
                break;
        }
    }


    public void getGroups() {
        Call<ResponseBody> getUser = ApiClient.getApiClient().getGroups(USER_TYPE);
        getApiCaller().callService(getUser, APIsEndPoints.GET_GROUP_CHATS);
    }

    @Nullable
    public ArrayList<UserModel> parseUsers(String body) {
        Type type = new TypeToken<ArrayList<UserModel>>() {
        }.getType();
        return new Gson().fromJson(body, type);
    }

    public ArrayList<GroupModel> parseGroups(String body) {
        Type type = new TypeToken<ArrayList<GroupModel>>() {
        }.getType();
        return new Gson().fromJson(body, type);
    }

    public void toChatFragment(UserModel userModel) {
        if (getActivity() != null) {
            FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                    .withAnimationType(FragmentAnimationType.SLIDE_FROM_LEFT)
                    .addToBackStack(ChatListFragment.TAG)
                    .replaceToFragment(ChatFragment.newInstance(userModel,
                            ChatFragment.TYPE_ONE_TO_ONE));
            new ChatPresenter(getContext(), null).setmReceiverId(userModel.getUserid())
                    .getMessagesFromServer();

        }
    }

    public void getFavouriteUser(PagingUtility pagingUtility) {
        pagingUtility.setLoading(true);
        Call<ResponseBody> favouritesChats = ApiClient.getApiClient().getFavouritesChats(SessionClass.getInstance().getUserId(getContext()),
                pagingUtility.getmNoOfRows(),
                pagingUtility.getmLastId());

        getApiCaller().setmOverrideCallback((body, endpoint) -> {

            if (APIsEndPoints.GET_FAV_USER.equals(endpoint)) {
                pagingUtility.setLoading(false);
                ArrayList<UserModel> userModels = parseUsers(body);
                if (userModels != null) {
                    removeMeFromList(userModels);
                    if (userModels.size() > 0)
                        pagingUtility.setmLastId(userModels.get(userModels.size() - 1).getUserid());
                    pagingUtility.setLoadedAll(userModels.size());
                }

                if (getContract() instanceof IFavUserListener) {
                    ((IFavUserListener) getContract()).onFavUserFecthed(userModels);
                }
            }


        }).setmOverrideFailureCallback((e, endPoint) -> {
            if (endPoint.equals(APIsEndPoints.POST_FAV_USER))
                pagingUtility.setLoading(false);
        }).callService(favouritesChats, APIsEndPoints.GET_FAV_USER);
    }


    public void searchMember(String searchTerm, PagingUtility pagingUtility) {

        pagingUtility.setLoading(true);
        Call<ResponseBody> searchMember = ApiClient.getApiClient().searchMembers(searchTerm,
                pagingUtility.getmNoOfRows(),
                pagingUtility.getmLastId(),USER_TYPE);

        getApiCaller().setmOverrideCallback((body,endPoint) -> {
            if(endPoint.equals(APIsEndPoints.SEARCH_USER)){
                pagingUtility.setLoading(false);
                ArrayList<UserModel> users = parseUsers(body);
                if (users != null) {
                    removeMeFromList(users);
                    if (users.size() > 0)
                        pagingUtility.setmLastId(users.get(users.size() - 1).getUserid());
                    pagingUtility.setLoadedAll(users.size());
                }

                if (getContract() instanceof ISearchMemberListerner) {
                    ((ISearchMemberListerner) getContract()).onUserFetched(users);
                }
            }
        }).setmOverrideFailureCallback((e,endPoint)-> {
            if(endPoint.equals(APIsEndPoints.SEARCH_USER))
                pagingUtility.setLoading(false);
        }).callService(searchMember, APIsEndPoints.SEARCH_USER);
    }

    public void addMemberToFav(int userId) {
        Call<ResponseBody> post = ApiClient.getApiClient().
                postFavourite(SessionClass.getInstance().getUserId(getContext()),
                        userId);
        getApiCaller().callService(post, APIsEndPoints.POST_FAV_USER);
    }

    private void removeMeFromList(ArrayList<UserModel> userModels) {
        for (UserModel user : userModels) {
            if (user.getUserid() == SessionClass.getInstance().
                    getUserId(getContext())) {
                userModels.remove(user);
                break;
            }
        }
    }

    public interface IRecentChatListener extends IPresenterContract {
        void recentChatsFetched(ArrayList<UserModel> users);
    }

    public interface IGroupListener extends IPresenterContract {
        void onGroupsFetched(ArrayList<GroupModel> groups);
    }

    public interface IFavUserListener extends IPresenterContract {
        void onFavUserFecthed(ArrayList<UserModel> users);
    }

    public interface ISearchMemberListerner extends IPresenterContract {
        void onUserFetched(ArrayList<UserModel> users);
    }

    public interface IMemberFavListener extends IPresenterContract {
        void memberAddedToFav();
    }

}
