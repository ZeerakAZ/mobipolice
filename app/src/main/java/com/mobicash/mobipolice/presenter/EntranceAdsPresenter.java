package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.model.dataaccess.adapters.VideoPagerAdapter;

import java.util.concurrent.TimeUnit;

/**
 * Created by Zeera on 3/11/2018 bt ${File}
 */

public class EntranceAdsPresenter extends BasePresenter {

    VideoPagerAdapter videoPagerAdapter;
    public static final long SLIDE_TIME = TimeUnit.SECONDS.toMillis(5);

    public EntranceAdsPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }





    @Override
    public void onSuccess(String body, String endPoint) {

    }

    @Override
    public void onError(String error, String endPoint) {

    }

    @Override
    public void onFailed(Throwable e, String endPoint) {

    }
}
