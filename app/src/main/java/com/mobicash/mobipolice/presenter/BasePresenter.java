package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.network.CallApi;
import com.mobicash.mobipolice.model.dataaccess.network.IResponse;
import com.mobicash.mobipolice.views.activities.BaseActivity;

/**
 * Created by Zeera on 3/11/2018 bt ${File}
 */

public class BasePresenter implements IResponse{

    public static final String USER_TYPE = "P";
    private Activity activity;
    private Context mContext;
    private CallApi mApiCaller;
    private IPresenterContract mContract;





    BasePresenter(Activity activity,@Nullable IPresenterContract contract) {
        if (!(activity instanceof BaseActivity))
            throw new IllegalArgumentException("invalid activity");
        this.activity = activity;
        mApiCaller = new CallApi(activity, this);
        mApiCaller.setLoadingMessage(activity.getString(R.string.message_wait));
        mContract = contract;
        mContext=activity;
    }

    BasePresenter(Context context,@Nullable IPresenterContract contract){
        mApiCaller = new CallApi(context, this);
        mContract = contract;
        mContext=context;
    }



    public Context getContext() {

        return mContext;
    }

    CallApi getApiCaller() {
        return mApiCaller;
    }

    @Nullable
    BaseActivity getActivity() {
        return ((BaseActivity) activity);
    }



    public IPresenterContract getContract() {
        return mContract;
    }



    public interface IPresenterContract {

    }

    @Override
    public void onSuccess(String body, String endPoint) {
    }

    @Override
    public void onError(String error, String endPoint) {

    }

    @Override
    public void onFailed(Throwable e, String endPoint) {

    }
}
