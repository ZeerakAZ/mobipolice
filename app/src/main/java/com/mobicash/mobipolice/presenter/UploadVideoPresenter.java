package com.mobicash.mobipolice.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.network.ProgressRequestBody;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.PermissionsUtitlty;
import com.mobicash.mobipolice.model.utilities.ToastUtility;
import com.mobicash.mobipolice.presenter.services.FileUploadService;
import com.mobicash.mobipolice.views.fragments.BaseFragment;



/**
 * Created by Zeera on 3/17/2018 bt ${File}
 */

public class UploadVideoPresenter extends BasePresenter{
   
    public static final int CAPTURE_AUDIO = 101;
    public static final int CAPTURE_VIDEO = 102;
    public static final int SELECT_FILE_AUDIO = 103;
    public static final int SELECT_FILE_VIDEO = 104;
    public static final int REQUEST_CAMERA = 1;

    private Uri mFilePath;
    
    
    private final BaseFragment fragment;

    public UploadVideoPresenter(Activity activity,@Nullable BaseFragment fragment, @Nullable IPresenterContract contract) {
        super(activity, contract);
        this.fragment = fragment;
    }

    public void openAudioSelectionDialog() {
        final CharSequence[] items = {getActivity().getString(R.string.ph_take_audio), 
                getActivity().getString(R.string.message_select_from_gallery)};
        
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getString(R.string.ph_audio));
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals(getActivity().getString(R.string.ph_take_audio))) {
                startAudioRecordingScreen();
            } else if (items[item].equals(getActivity().getString(R.string.message_select_from_gallery))) {
                Intent intent = new Intent(
                        Intent.ACTION_GET_CONTENT);
                intent.setType("audio/*");
                fragment.startActivityForResult(
                        Intent.createChooser(intent, getActivity().getString(R.string.message_select_file)),
                        SELECT_FILE_AUDIO);
            } else if (items[item].equals(getActivity().getString(R.string.cancel))) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void openVideoSelectionDialog() {
        final CharSequence[] items = {getActivity().getString(R.string.ph_take_video), getActivity().getString(R.string.message_select_from_gallery)};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getString(R.string.ph_video));
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals(getActivity().getString(R.string.ph_take_video))) {
                if (PermissionsUtitlty.isCameraPermissionAllowed(getContext()))
                    startVideoRecording();
                else
                    PermissionsUtitlty.showPermissionsDialogForFragment(fragment,
                            REQUEST_CAMERA, Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE);
            } else if (items[item].equals(getActivity().getString(R.string.message_select_from_gallery))) {
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("video/*");
                fragment.startActivityForResult(
                        Intent.createChooser(intent, getActivity().getString(R.string.message_select_file)),
                        SELECT_FILE_VIDEO);
            } else if (items[item].equals(getActivity().getString(R.string.cancel))) {
                dialog.dismiss();
            }
        });
        builder.show();

    }


    public void uploadFile(String path){

    }

    private void startAudioRecordingScreen() {

        Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        if (fragment!=null) {
            fragment.startActivityForResult(intent, CAPTURE_AUDIO);
        }else{
            getActivity().startActivityForResult(intent,CAPTURE_AUDIO);
        }


    }

    public void startVideoRecording() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (fragment!=null) {
            fragment.startActivityForResult(intent, CAPTURE_VIDEO);
        }else{
            getActivity().startActivityForResult(intent,CAPTURE_VIDEO);
        }
    }

    public UploadVideoPresenter setmFilePath(Uri mFilePath) {
        this.mFilePath = mFilePath;
        return this;
    }

    public void audioPlayer(){
        //set up MediaPlayer
        MediaPlayer mp = new MediaPlayer();

        try {
            mp.setDataSource(getRealPathFromURI(mFilePath));
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startVideo(){
        //Log.i("ab",currentVideoPath.getPath());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getRealPathFromURI(mFilePath)));
        intent.setDataAndType(Uri.parse(getRealPathFromURI(mFilePath)), "video/mp4");
        fragment.startActivity(intent);
    }

    public String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = fragment.getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public Uri getmFilePath() {
        return mFilePath;
    }

    public void startUploadingService(ServiceConnection connection){

            String realPathFromURI = getRealPathFromURI(mFilePath);
            SessionClass.getInstance().getCrimeModel().setLocalCrimePath(realPathFromURI);
            FileUploadService.startUploading(getContext(), realPathFromURI,connection);

    }





}
