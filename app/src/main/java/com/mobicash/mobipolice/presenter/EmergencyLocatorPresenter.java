package com.mobicash.mobipolice.presenter;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.network.APIsEndPoints;
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient;
import com.mobicash.mobipolice.model.dataaccess.network.IApiInterface;
import com.mobicash.mobipolice.model.utilities.MapUtility;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Zeera on 3/24/2018 bt ${File}
 */

public class EmergencyLocatorPresenter extends BasePresenter {

    public EmergencyLocatorPresenter(Activity activity, @Nullable IPresenterContract contract) {
        super(activity, contract);
    }

    public void getNearByLocation(LatLng location, double radius, String type){
        Call<ResponseBody> nearByLocations = ApiClient.getMapClient().create(IApiInterface.class)
                .getNearByLocations(MapUtility.getStringFromLatLng(location),radius,type,
                        getContext().getString(R.string.google_maps_key));
        getApiCaller().callService(nearByLocations, APIsEndPoints.NEARBY_PLACES);
    }

    @Override
    public void onSuccess(String body, String endPoint) {
        super.onSuccess(body, endPoint);
        switch (endPoint){
            case APIsEndPoints.NEARBY_PLACES:
                try {
                    ArrayList<LatLng> places =new ArrayList<>();

                    JSONObject jresponce = new JSONObject(body);
                    JSONArray jResult = jresponce.getJSONArray("results");
                    for (int i = 0; i < jResult.length(); i++) {
                        JSONObject location = jResult.getJSONObject(i).
                                getJSONObject("geometry").getJSONObject("location");
                        LatLng latLng = new LatLng(location.getDouble("lat"),
                                location.getDouble("lng"));
                        places.add(latLng);
                    }
                    if (getContract() instanceof INearByPlacesListener) {
                        ((INearByPlacesListener) getContract()).onNearByLocationFetched(places);
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
                break;
        }
    }

    public interface INearByPlacesListener extends IPresenterContract{
        void onNearByLocationFetched(ArrayList<LatLng> places);
    }
}
