package com.mobicash.mobipolice.model.utilities;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;

/**
 * Created by Zeera on 1/7/2018 bt ${File}
 */

public class GeoCodeHandlerClass extends Handler {

    private final IHandlerCallback mCallback;

    public GeoCodeHandlerClass(IHandlerCallback callback){
        mCallback = callback;
    }

    @Override
    public void handleMessage(Message message) {
        String result;
        switch (message.what) {
            case 1:
                Bundle bundle = message.getData();
                result = bundle.getString("address");
                break;
            default:
                result = null;
        }
        // replace by what you need to do
        if (mCallback!=null)
            mCallback.addressFetched(result);
    }

    public interface IHandlerCallback{
        void addressFetched(@Nullable String address);
    }
}
