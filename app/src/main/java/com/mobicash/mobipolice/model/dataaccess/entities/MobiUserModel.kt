package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by zeerak on 4/5/2018 bt ${File}
 */
class MobiUserModel {
    @SerializedName("accesscode")
    @Expose
    private val accesscode: String? = null
    @SerializedName("code")
    @Expose
    private val code: String? = null
    @SerializedName("created")
    @Expose
    private val created: String? = null
    @SerializedName("fullname")
    @Expose
    private val fullname: String? = null
    @SerializedName("imei")
    @Expose
    private val imei: String? = null
    @SerializedName("language")
    @Expose
    private val language: String? = null
    @SerializedName("latitude")
    @Expose
    private val latitude: Any? = null
    @SerializedName("longitude")
    @Expose
    private val longitude: Any? = null
    @SerializedName("location")
    @Expose
    private val location: String? = null
    @SerializedName("mobile")
    @Expose
    private val mobile: String? = null
    @SerializedName("password")
    @Expose
    private val password: String? = null
    @SerializedName("profileurl")
    @Expose
    private val profileurl: String? = null
    @SerializedName("status")
    @Expose
    private val status: String? = null
    @SerializedName("token")
    @Expose
    private val token: Any? = null
    @SerializedName("userid")
    @Expose
    private val userid: String? = null
    @SerializedName("username")
    @Expose
    private val username: String? = null

    val userModel: UserModel
        get() = UserModel().setStatus(true)
                .setFullName(fullname)
                .setMobile(mobile)
                .setUserImage(profileurl)
                .setMobiUserId(userid)
                .setUserName(username)
                .setLocation(location)
}
