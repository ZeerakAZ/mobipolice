package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.MemberModel;
import com.mobicash.mobipolice.model.utilities.CircleTransform;
import com.mobicash.mobipolice.presenter.MemberPresenter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeera on 6/25/2017 bt ${File}
 */

public class SearchMemberAdapter extends BaseAdapter
        implements Filterable, MemberPresenter.IGetMemberListener {

    private static final int MAX_RESULTS = 10;
    private Activity mActivity;
    private List<MemberModel> resultList = new ArrayList<>();

    public SearchMemberAdapter(Activity activity) {
        mActivity = activity;
    }

    public void setResultList(List<MemberModel> resultList) {
        this.resultList = resultList;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public MemberModel getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_seatch_member, parent, false);
        }
        ImageView userImage = convertView.findViewById(R.id.iv_user_image);
        TextView userName = convertView.findViewById(R.id.tv_user_name);

        userName.setText(getItem(position).getName());
        if (getItem(position).getImage() != null && !getItem(position).getImage().isEmpty())
            Picasso.with(mActivity).load(getItem(position).getImage()).
                    placeholder(R.drawable.profile).
                    transform(new CircleTransform()).
                    into(userImage);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                filterResults.values = resultList;
                filterResults.count = resultList.size();
                new MemberPresenter(mActivity, SearchMemberAdapter.this)
                        .searchMember(constraint.toString());

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<MemberModel>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    @Override
    public void onMemberFetched(ArrayList<MemberModel> members) {
        resultList.clear();
        resultList.addAll(members);
        notifyDataSetChanged();
    }

}
