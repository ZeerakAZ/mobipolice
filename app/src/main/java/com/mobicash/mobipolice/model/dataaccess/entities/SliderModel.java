package com.mobicash.mobipolice.model.dataaccess.entities;

import android.support.annotation.Nullable;

import com.mobicash.mobipolice.model.essentials.BeaconReferenceApplication;

/**
 * Created by Zeera on 2/4/2018 bt ${File}
 */

public class SliderModel {
    private String videoPath;
    private int thumbImageResource;

    public SliderModel(@Nullable Integer videoPathRawId, int thumbImageResource) {
        setVideoPath(videoPathRawId);
        this.thumbImageResource = thumbImageResource;
    }

    public String getVideoPath() {
        return videoPath;
    }

    private void setVideoPath(@Nullable Integer videoPathRawId) {
        if (videoPathRawId == null)
            return;
        this.videoPath = "android.resource://" + BeaconReferenceApplication.context.getPackageName() + videoPathRawId;
    }

    public int getThumbImageResource() {
        return thumbImageResource;
    }

    public void setThumbImageResource(int thumbImageResource) {
        this.thumbImageResource = thumbImageResource;
    }

    public boolean isVideo() {
        return videoPath != null;
    }
}
