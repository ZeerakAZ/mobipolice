package com.mobicash.mobipolice.model.dataaccess.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ambre on 5/13/2017.
 */

public class MemberModel {
    @SerializedName("FriendName")
    @Expose
    private String name;

    @SerializedName("FriendImage")
    @Expose
    private String image;

    @SerializedName("FriendUserId")
    @Expose
    private int id;

    public int getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setName(String name) {
        this.name = name;
    }
}
