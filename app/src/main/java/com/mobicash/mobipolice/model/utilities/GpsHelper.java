package com.mobicash.mobipolice.model.utilities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.interfaces.ILocationUpdated;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.views.dialogs.GenericDialog;
import com.mobicash.mobipolice.views.dialogs.IDialogDouble;


/**
 * Created by Administrator on 3/9/2017 bt ${File}
 */

public class GpsHelper {

    public static final int REQUEST_GPS = 1001;
    private Context mContext;

    private LocationListener mListner;
    private LocationManager locationManager;
    private GenericDialog gpsDialog;
    private Location mLocation;

    public GpsHelper(Context context) {
        mContext = context;
    }

    public boolean askForPermmision(@Nullable final ILocationUpdated locationUpdated) {
        if (!PermissionsUtitlty.isLocationPermissionAllowed(mContext)) {
            PermissionsUtitlty.showPermissionsDialogForActivity
                    ((Activity) mContext, REQUEST_GPS, Manifest.permission.ACCESS_FINE_LOCATION);
            return false;
        } else {
            locationManager = (LocationManager) mContext
                    .getSystemService(Context.LOCATION_SERVICE);
            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (isGPSEnabled) {
                getLocation(locationUpdated);
                return true;
            } else {
                getLocation(locationUpdated);
                buildAlertMessageNoGps(mContext);
                return false;
            }

        }
    }

    private void getLocation(@Nullable final ILocationUpdated locationUpdated) {
        mListner = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                SessionClass.getInstance().setCurrentUserLocation(location);
                mLocation = location;
                if (gpsDialog != null)
                    gpsDialog.hide();
                if (locationUpdated != null)
                    locationUpdated.onUpdate(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                LogUtility.debugLog("onStatusChanged");
            }

            @Override
            public void onProviderEnabled(String s) {
                LogUtility.debugLog("onProviderEnabled");
            }

            @Override
            public void onProviderDisabled(String s) {
                LogUtility.debugLog("onProviderDisabled");
            }
        };
        try {

            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER,
                    mListner, mContext.getMainLooper());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    private void buildAlertMessageNoGps(final Context ctx) {
        if (gpsDialog == null || !gpsDialog.isShowing())
            gpsDialog = new GenericDialog(ctx, ctx.getString(R.string.ph_alert),
                    ctx.getString(R.string.message_no_gps),
                    ctx.getString(R.string.ph_dismiss),
                    ctx.getString(R.string.ph_settings), new IDialogDouble() {
                @Override
                public void onRightClick(DialogInterface dialogInterface) {
                    ctx.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }

                @Override
                public void onLeftClick(DialogInterface dialogInterface) {
                    dialogInterface.dismiss();
                }
            }).setLogo(R.drawable.dialog_info);
        gpsDialog.show();

    }

    @Nullable
    public Location getmLocation() {
        return mLocation;
    }
}