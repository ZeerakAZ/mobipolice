package com.mobicash.mobipolice.model.dataaccess.network;

import android.app.Activity;
import android.content.Context;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.views.dialogs.ErrorDialog;
import com.mobicash.mobipolice.views.dialogs.LoadingDialog;


import java.io.IOException;
import java.net.SocketTimeoutException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallApi {

    private IResponse iResponse;
    private ErrorDialog errorDialog;
    private LoadingDialog loadingDialog;
    private Context context;
    private String errorMessage;
    private String loadingMessage;
    private IResponseSuccessListener mOverrideCallback;
    private IResponseFailureListener mOverrideFailureCallback;


    public String getErrorMessage() {
        return errorMessage;
    }

    public String getLoadingMessage() {
        return loadingMessage;
    }

    public CallApi(Context context, IResponse Iresponse) {
        this.iResponse = Iresponse;
        if(context instanceof Activity){
            this.errorDialog = new ErrorDialog(context);
            this.loadingDialog = new LoadingDialog(context);
        }

        this.context = context;
    }

    public CallApi setmOverrideCallback(IResponseSuccessListener mOverrideCallback) {
        this.mOverrideCallback = mOverrideCallback;
        return this;
    }

    public CallApi setmOverrideFailureCallback(IResponseFailureListener mOverrideFailureCallback) {
        this.mOverrideFailureCallback = mOverrideFailureCallback;
        return this;
    }

    public void callService(Call<ResponseBody> call, final String endPoint) {
        if(getLoadingMessage()!=null)
        showLoadingDialog(true, getLoadingMessage());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,Response<ResponseBody> response) {
                try {
                    if (response != null) {
                        showLoadingDialog(false, "");
                        if (response.isSuccessful()) {
                            String body = response.body().string();
                            iResponse.onSuccess(body, endPoint);

                            if(mOverrideCallback!=null)
                                mOverrideCallback.onSuccess(body,endPoint);
                            showLoadingDialog(false, "");
                        } else if (response.code() == 404)
                            showErrorDialog(true, context.getString(R.string.not_found));
                        else if (response.code() == 500) {
                            showErrorDialog(true, context.getString(R.string.internal_server_error));
                        } else {
                            iResponse.onError(response.body().string(), endPoint);
                            showErrorDialog(true, response.errorBody().string());
                        }
                    }
                } catch (Exception e) {
                    showLoadingDialog(false, "");
                    showErrorDialog(true, e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call,Throwable t) {
                iResponse.onFailed(t, endPoint);
                if(mOverrideFailureCallback!=null)
                    mOverrideFailureCallback.onFailure(t,endPoint);
                showLoadingDialog(false, "");
                if (t instanceof SocketTimeoutException) {
                    showErrorDialog(true, context.getString(R.string.server_not_available));
                } else if (t instanceof IOException) {
                    showErrorDialog(true, context.getString(R.string.no_internet));
                } else {
                    showErrorDialog(true, t.getMessage());
                    t.printStackTrace();
                }
            }
        });
    }

    private void showErrorDialog(boolean isShow, String errorMessage) {
        if(errorDialog==null)
            return;
        if (isShow && !errorDialog.isShowing()) {
            errorDialog.show();
            errorDialog.setErrorText(errorMessage);
        } else
            errorDialog.dismiss();
    }

    private void showLoadingDialog(boolean isShow, String loadingMessage) {
        if(loadingDialog==null)
            return;
        if (isShow && !loadingDialog.isShowing()) {
            loadingDialog.show();
            loadingDialog.setMessage(loadingMessage);
        } else {
            loadingDialog.dismiss();
        }
    }

    public void setErrorMessage(String message) {
        errorMessage = message;
    }

    public void setLoadingMessage(String message) {
        loadingMessage = message;
    }

    @FunctionalInterface
    public interface IResponseSuccessListener{
         void onSuccess(String body,String endPoint);
    }

    @FunctionalInterface
    public interface IResponseFailureListener{
        void onFailure(Throwable e,String endPoint);
    }
}