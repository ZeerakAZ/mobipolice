package com.mobicash.mobipolice.model.dataaccess.interfaces;

import com.mobicash.mobipolice.presenter.BasePresenter;

/**
 * Created by Zeera on 4/22/2017.
 */

public interface IMessageReceived extends BasePresenter.IPresenterContract{
    void onMessageReceived(Integer userId);
    void onGroupMessageReceived(Integer groupId);
}
