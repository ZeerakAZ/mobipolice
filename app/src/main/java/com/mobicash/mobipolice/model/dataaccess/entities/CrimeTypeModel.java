package com.mobicash.mobipolice.model.dataaccess.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Zeera on 5/18/2017 bt ${File}
 */

public class CrimeTypeModel implements Serializable {
    @SerializedName("Id")
    @Expose
    private int crimeTypeId;
    @SerializedName("Color")
    @Expose
    private String colorCode;
    @SerializedName("Description")
    @Expose
    private String crimeTypeName;
    private boolean isCheck;

    public int getCrimeTypeId() {
        return crimeTypeId;
    }

    public void setCrimeTypeId(int crimeTypeId) {
        this.crimeTypeId = crimeTypeId;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getCrimeTypeName() {
        return crimeTypeName;
    }

    public void setCrimeTypeName(String crimeTypeName) {
        this.crimeTypeName = crimeTypeName;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return crimeTypeName;
    }
}
