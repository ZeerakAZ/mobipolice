package com.mobicash.mobipolice.model.dataaccess.validations;

import android.content.Context;

import com.mobicash.mobipolice.model.utilities.ToastUtility;

import java.util.ArrayList;

/**
 * Created by Administrator on 5/12/2017.
 */

public class Validator {

    private final Context mContext;

    public Validator(Context context){
        mContext = context;
    }

    public boolean validates(ArrayList<Validation> validations) {
        boolean isValidated = false;
        for (Validation validation : validations) {
            isValidated = validation.passes();
            if (!isValidated){
                ToastUtility.showToastForLongTime(mContext,validation.getMessage());
                return false;
            }
        }
        return true;
    }

    public boolean validates(Validation...validation) {
        for (Validation validation1 : validation) {
            if(!validation1.passes()){
                ToastUtility.showToastForLongTime(mContext,validation1.getMessage());
                return false;
            }
        }
        return true;

    }

}
