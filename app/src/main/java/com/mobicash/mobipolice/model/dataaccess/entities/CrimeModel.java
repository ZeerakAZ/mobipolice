package com.mobicash.mobipolice.model.dataaccess.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * Created by Zeera on 4/11/2017.
 */

public class CrimeModel implements Serializable {
    @SerializedName("Id")
    @Expose
    public Integer id;
    @SerializedName("CrimeTitle")
    @Expose
    public String crimeTitle;
    @SerializedName("CrimeTypeId")
    @Expose
    public Integer crimeTypeId;
    @SerializedName("CrimeDescription")
    @Expose
    public String crimeDescription;
    @SerializedName("SuspectDescription")
    @Expose
    public String suspectDescription;
    @SerializedName("VehicleDescription")
    @Expose
    public String vehicleDescription;
    @SerializedName("CrimeType")
    @Expose
    public String crimeType;
    @SerializedName("CrimeImageURL")
    @Expose
    public String crimeImageURL;
    @SerializedName("CrimeFileName")
    @Expose
    public String crimeFileName;
    @SerializedName("CrimeFileURL")
    @Expose
    public String crimeFileURL;
    @SerializedName("CrimeLocation")
    @Expose
    public String crimeLocation;
    @SerializedName("UserGPSLatitude")
    @Expose
    public String userGPSLatitude;
    @SerializedName("UserGPSLongitude")
    @Expose
    public String userGPSLongitude;
    @SerializedName("CrimeGPSLatitude")
    @Expose
    public String crimeGPSLatitude;
    @SerializedName("CrimeGPSLongitude")
    @Expose
    public String crimeGPSLongitude;
    @SerializedName("Witness")
    @Expose
    public String witness;
    @SerializedName("EntDate")
    @Expose
    public String entDate;
    @SerializedName("EntHost")
    @Expose
    public String entHost;
    @SerializedName("EntHostIP")
    @Expose
    public String entHostIP;
    @SerializedName("EntOperation")
    @Expose
    public String entOperation;
    @SerializedName("EntUserId")
    @Expose
    public Integer entUserId;
    @SerializedName("EntIMEI")
    @Expose
    public String entIMEI;
    @SerializedName("Active")
    @Expose
    public Boolean isActive;
    @SerializedName("CrimeTypeColor")
    @Expose
    public String crimeTypeColor;

    private String distance;

    private transient String localFilePath;

    public Double getCrimeGPSLatitude() {
        if(crimeGPSLatitude.equals(""))
            return 0.0;
        return Double.valueOf(crimeGPSLatitude);
    }

    public Double getCrimeGPSLongitude() {
        if(crimeGPSLongitude.equals(""))
            return 0.0;
        return Double.valueOf(crimeGPSLongitude);
    }

   public String getDistance() {
       return checkNull(distance);
   }
    public String getCrimeTitle() {
        return checkNull(crimeTitle);
    }

    public String getSuspectDescription() {
        return checkNull(suspectDescription);
    }


    public String getVehicleDescription() {
        return checkNull(vehicleDescription);
    }


    public String getWitness() {
        return checkNull(witness);
    }

    public String getCrimeLocation() {
        return checkNull(crimeLocation);
    }

    private String checkNull(String value) {
        return value==null?"N/A":value;
    }

    public CrimeModel setDistance(String distance) {
        this.distance = distance;
        return this;
    }


    public void setCrimeTitle(String crimeTitle) {
        this.crimeTitle = crimeTitle;
    }

    public void setSuspectDescription(String suspectDescription) {
        this.suspectDescription = suspectDescription;
    }

    public void setVehicleDescription(String vehicleDescription) {
        this.vehicleDescription = vehicleDescription;
    }

    public void setWitness(String witness) {
        this.witness = witness;
    }

    public void setCrimeLocation(String crimeLocation) {
        this.crimeLocation = crimeLocation;
    }

    public String getLocalFilePath() {
        return localFilePath;
    }

    public void setCrimeFileURL(String crimeFileURL) {
        this.crimeFileURL = crimeFileURL;
    }

    public void setLocalCrimePath(String localCrimePath) {
        this.localFilePath = localCrimePath;
    }
}
