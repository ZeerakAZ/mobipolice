package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.EmergencyNumber;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 4/1/2017.
 */

public class EmergencyAdapter extends RecyclerView.Adapter<EmergencyAdapter.ViewHolder> {
    private ArrayList<EmergencyNumber> mData;
    private Context mContext;
    private IEmergencyNumberListener mListener;

    public EmergencyAdapter(ArrayList<EmergencyNumber> mData, Context mContext, IEmergencyNumberListener clickListener) {
        this.mData = mData;
        this.mContext = mContext;
        mListener = clickListener;
    }

    @Override
    public EmergencyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_emergency, null);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(EmergencyAdapter.ViewHolder holder, int position) {
        EmergencyNumber item = mData.get(position);
        holder.tvPhoneId.setText(item.getName());
        holder.ivLogo.setImageResource(item.getImageResouceId());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_phone_id)
        TextView tvPhoneId;
        @BindView(R.id.iv_logo)
        ImageView ivLogo;

         ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ((RelativeLayout) tvPhoneId.getParent()).setOnClickListener(this::numberSelected);
        }

        private void numberSelected(View view) {
            if (mListener != null) {
                mListener.numberSelected(mData.get(getAdapterPosition()));
            }
        }


    }

    public  interface IEmergencyNumberListener{
         void numberSelected(EmergencyNumber number);
    }
}
