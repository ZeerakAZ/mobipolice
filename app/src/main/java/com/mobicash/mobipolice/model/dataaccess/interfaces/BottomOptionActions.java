package com.mobicash.mobipolice.model.dataaccess.interfaces;

import android.support.annotation.IntDef;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.BottomSheetOptionModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Zeera on 12/16/2017 bt ${File}
 */

@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.FIELD,
        ElementType.PARAMETER, ElementType.METHOD})
@IntDef({BottomOptionActions.DELETE_COMMENT,
        BottomOptionActions.REPORT_SPAM,
        BottomOptionActions.EDIT_COMMENT,
        BottomOptionActions.BLOCK_USER
})
public @interface BottomOptionActions {
    int REPORT_SPAM = 0xf01;
    int EDIT_COMMENT = 0xf02;
    int DELETE_COMMENT = 0xf03;
    int BLOCK_USER = 0xf04;

    BottomSheetOptionModel ACTION_REPORT_SPAM = new BottomSheetOptionModel(REPORT_SPAM,
            R.string.action_report_spam);
    BottomSheetOptionModel ACTION_EDIT_COMMENT = new BottomSheetOptionModel(EDIT_COMMENT,
            R.string.action_edit_comment);
    BottomSheetOptionModel ACTION_DELETE_COMMENT = new BottomSheetOptionModel(DELETE_COMMENT,
            R.string.action_delete_comment);
    BottomSheetOptionModel ACTION_BLOCK_USER = new BottomSheetOptionModel(BLOCK_USER,
            R.string.action_block_user);

}


