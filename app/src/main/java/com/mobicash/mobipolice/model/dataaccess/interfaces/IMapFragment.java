package com.mobicash.mobipolice.model.dataaccess.interfaces;

/**
 * Created by Zeera on 3/16/2018 bt ${File}
 */

public interface IMapFragment {
    void destroyMap();
}
