package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonGetModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 8/6/2017 bt ${File}
 */

public class MissingPersonAdapter extends RecyclerView.Adapter<MissingPersonAdapter.ViewHolder> {
    private ArrayList<MissingPersonGetModel> mData;
    private Context mContext;
    private IMissingPeronSelectionListener mListener;

    public MissingPersonAdapter(Context mContext, ArrayList<MissingPersonGetModel> mData,
                                IMissingPeronSelectionListener listener) {
        this.mData = mData;
        this.mContext = mContext;
        mListener = listener;
    }

    @Override
    public MissingPersonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_missing_person, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MissingPersonAdapter.ViewHolder holder, int position) {
        MissingPersonGetModel item = mData.get(position);
        holder.tvLocation.setText(item.getLocation());
        holder.tvPersonName.setText(item.getPersonName());
        Picasso.with(mContext).load(item.getPersonImage())
                .placeholder(R.drawable.user_placeholder).
                into(holder.mIvPersonImage);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_person_image)
        ImageView mIvPersonImage;
        @BindView(R.id.tv_missing_location)
        TextView tvLocation;
        @BindView(R.id.tv_person_name)
        TextView tvPersonName;
        @BindView(R.id.cv_parent)
        CardView cvParent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cvParent.setOnClickListener(v -> mListener.onMissingPersonSelect(mData.get(getAdapterPosition())));
        }
    }

    public interface IMissingPeronSelectionListener {
        void onMissingPersonSelect(MissingPersonGetModel person);
    }
}
