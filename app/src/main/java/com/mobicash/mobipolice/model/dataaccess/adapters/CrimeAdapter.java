package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel;
import com.mobicash.mobipolice.presenter.CrimeMapPresenter;

import java.util.ArrayList;

/**
 * Created by zeerak on 5/13/2018 bt ${File}
 */
public class CrimeAdapter extends RecyclerView.Adapter<CrimeAdapter.ViewHolder> {
    private ArrayList<CrimeModel> mCrimes;
    private Context mContext;
    private ICrimeClickListener mListener;


    public CrimeAdapter(Context mContext, ArrayList<CrimeModel> mUser) {
        this.mCrimes = mUser;
        this.mContext = mContext;

    }

    public CrimeAdapter setListener(ICrimeClickListener mListener) {
        this.mListener = mListener;
        return this;
    }

    @Override
    public CrimeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_crime, parent, false);
        return new CrimeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CrimeAdapter.ViewHolder holder, int position) {
        holder.bindData(mCrimes.get(position));
    }

    @Override
    public int getItemCount() {
        return mCrimes == null ? 0 : mCrimes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_crime_title, tv_crime_location, tv_distance,
                tv_vehicle_desc, tv_witness, tv_suspect_description;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_crime_title = itemView.findViewById(R.id.tv_crime_title);
            tv_crime_location = itemView.findViewById(R.id.tv_crime_location);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_vehicle_desc = itemView.findViewById(R.id.tv_vehicle_desc);
            tv_witness = itemView.findViewById(R.id.tv_witness);
            tv_suspect_description = itemView.findViewById(R.id.tv_suspect_description);
            itemView.setOnClickListener(view->{
                if (mListener!=null) {
                    mListener.onCrimeSelected(mCrimes.get(getAdapterPosition()));
                }
            });
        }

        void bindData(CrimeModel crimeModel) {
            tv_crime_title.setText(mContext.getString(R.string.value_crime_title, crimeModel.getCrimeTitle()));
            tv_crime_location.setText(mContext.getString(R.string.value_crime_location, crimeModel.getCrimeLocation()));
            tv_distance.setText(mContext.getString(R.string.value_distance, crimeModel.getDistance()));
            tv_vehicle_desc.setText(mContext.getString(R.string.value_vehicle_desc,
                    crimeModel.getVehicleDescription()));
            tv_witness.setText(mContext.getString(R.string.value_witness, crimeModel.getWitness()));
            tv_suspect_description.setText(mContext.getString(R.string.value_suspect_desc,
                    crimeModel.getSuspectDescription()));
        }
    }

    public interface ICrimeClickListener{
        void onCrimeSelected(CrimeModel crimeModel);
    }
}
