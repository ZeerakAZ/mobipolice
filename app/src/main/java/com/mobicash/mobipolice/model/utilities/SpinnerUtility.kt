package com.mobicash.mobipolice.model.utilities

import android.content.Context
import android.support.annotation.ArrayRes
import android.widget.ArrayAdapter
import com.mobicash.mobipolice.R
import java.util.*

/**
 * Created by zeerak on 6/9/2018 bt ${File}
 */
public class SpinnerUtility{

    companion object {
        fun <T> getSpinnerAdapter(context: Context, items: List<T>): ArrayAdapter<T> {

            val adapter = ArrayAdapter(context,
                    R.layout.row_spinner,
                    R.id.text1, items)
            adapter.setDropDownViewResource(R.layout.row_spinner_dropdown)
            return adapter
        }

        fun getSpinnerAdapter(context: Context, @ArrayRes itemsId: Int): ArrayAdapter<String> {

            val stringArray = context.resources.getStringArray(itemsId)
            return getSpinnerAdapter(context, Arrays.asList(*stringArray))
        }


    }

}