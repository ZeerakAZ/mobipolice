package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Zeera on 8/6/2017 bt ${File}
 */

class MissingPersonModel(@field:SerializedName("PersonName")
                         @field:Expose
                         private val personName: String, @field:SerializedName("PersonImage")
                         @field:Expose
                         private val personImage: String,
                         @field:SerializedName("PersonDescription")
                         @field:Expose
                         private val personDescription: String, @field:SerializedName("LastDisappearanceTime")
                         @field:Expose
                         private val lastDisappearanceTime: String,
                         @field:SerializedName("Location")
                         @field:Expose
                         private val location: String, @field:SerializedName("Latitude")
                         @field:Expose
                         private val latitude: String, @field:SerializedName("Longitude")
                         @field:Expose
                         private val longitude: String, @field:SerializedName("ContactAt")
                         @field:Expose
                         private val contactAt: String,
                         @field:SerializedName("ContactName")
                         @field:Expose
                         private val contactName: String, @field:SerializedName("ContactAddress")
                         @field:Expose
                         private val contactAddress: String, @field:SerializedName("PostByUserId")
                         @field:Expose
                         private val postByUserId: Int?) {
    @SerializedName("EntHost")
    @Expose
    private val entHost: String? = null
    @SerializedName("EntHostIP")
    @Expose
    private val entHostIP: String? = null
    @SerializedName("EntIMEI")
    @Expose
    private val entIMEI: String? = null
}
