package com.mobicash.mobipolice.model.dataaccess.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Zeera on 4/15/2017.
 */

public class PlaceObject implements Parcelable {
    private String name;
    private double lat;
    private double lng;
    private String address;

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public PlaceObject(String name, double lat, double lng, String address) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    /**
     * Storing the Student data to Parcel object
     **/
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeString(address);
    }



    /**
     * Retrieving Student data from Parcel object
     * This constructor is invoked by the method createFromParcel(Parcel source) of
     * the object CREATOR
     **/
    private PlaceObject(Parcel in){
        this.name = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.address = in.readString();
    }

    public static final Parcelable.Creator<PlaceObject> CREATOR = new Parcelable.Creator<PlaceObject>() {

        @Override
        public PlaceObject createFromParcel(Parcel source) {
            return new PlaceObject(source);
        }

        @Override
        public PlaceObject[] newArray(int size) {
            return new PlaceObject[size];
        }
    };
}
