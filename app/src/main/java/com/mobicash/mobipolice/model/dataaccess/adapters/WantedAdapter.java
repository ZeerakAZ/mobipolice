package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.WantedModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 4/2/2017.
 */

public class WantedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<WantedModel> mData;
    private Context mContext;
    private IWantedItemListener mListener;

    public WantedAdapter(ArrayList<WantedModel> mData, Context mContext, IWantedItemListener fragment) {
        this.mData = mData;
        this.mContext = mContext;
        mListener = fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_wanted, parent,false);
        return new WantedAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        WantedModel item = mData.get(position);
        WantedAdapter.ViewHolder holderVw = ((WantedAdapter.ViewHolder) holder);
        holderVw.tvCriminalDesc.setText(item.getCriminalInfo());
        Picasso.with(mContext).load(item.getWantedImageURL())
                .placeholder(R.drawable.profile)
                .into(holderVw.ivCriminalImaage);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_criminalImage)
        ImageView ivCriminalImaage;
        @BindView(R.id.tv_criminal_decr)
        TextView tvCriminalDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ((ViewGroup) ivCriminalImaage.getParent()).setOnClickListener(view -> {
                mListener.onCriminalClicked(mData.get(getAdapterPosition()));
            });
        }
    }

    @FunctionalInterface
    public interface IWantedItemListener {
        void onCriminalClicked(WantedModel model);
    }
}
