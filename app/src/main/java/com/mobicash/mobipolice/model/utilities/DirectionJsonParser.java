package com.mobicash.mobipolice.model.utilities;

import android.graphics.Color;

import com.mobicash.mobipolice.model.dataaccess.network.IApiInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 12/5/2016.
 */

public class DirectionJsonParser {
    private IApiInterface mApiClient;
    private GoogleMap mMap;
    private Marker pickUpMarker, dropOffMarker;
    private String pickUp, dropoff, key;
    private boolean hideEndPoint;



    public DirectionJsonParser(IApiInterface mApiClient, GoogleMap mMap,
                               String pickUp, String dropoff, String key) {
        this.mApiClient = mApiClient;
        this.mMap = mMap;
        this.pickUp = pickUp;
        this.dropoff = dropoff;
        this.key = key;
    }

    public DirectionJsonParser(IApiInterface mApiClient, GoogleMap mMap,
                               LatLng pickUp, LatLng dropoff, String key) {
        this.mApiClient = mApiClient;
        this.mMap = mMap;
        this.pickUp = getStringFromLatLng(pickUp);
        this.dropoff = getStringFromLatLng(dropoff);
        this.key = key;
        hideEndPoint=true;
    }

    public boolean getMapPoints() {
        if (dropoff.length() == 0)
            return false;
        getDirectionPoints(pickUp, dropoff);
        return true;
    }


    private String getStringFromLatLng(LatLng latLng) {
        return String.valueOf(latLng.latitude) +
                "," +
                latLng.longitude;
    }

    private void getDirectionPoints(LatLng origin, LatLng dest) {
        Call<ResponseBody> getPoints = mApiClient.getMapsPoints(getStringFromLatLng(origin),
                getStringFromLatLng(dest), true, true, true, key);


        getPoints.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    List<List<HashMap<String, String>>> routes = parse(jsonObject);
                    drawPath(routes);

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getDirectionPoints(String origin, String dest) {
        Call<ResponseBody> getPoints = mApiClient.getMapsPoints(origin, dest, false, false, true, key);
        getPoints.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());

                    List<List<HashMap<String, String>>> routes = parse(jsonObject);
                    drawPath(routes);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private void drawPath(List<List<HashMap<String, String>>> routes) {

        ArrayList<LatLng> points = null;
        PolylineOptions lineOptions = null;
        MarkerOptions markerOptions = new MarkerOptions();

        // Traversing through all the routes
        for (int i = 0; i < routes.size(); i++) {
            points = new ArrayList<LatLng>();
            lineOptions = new PolylineOptions();

            // Fetching i-th route
            List<HashMap<String, String>> path = routes.get(i);

            // Fetching all the points in i-th route
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);

                points.add(position);
            }

            // Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            lineOptions.width(10);
            lineOptions.color(Color.BLUE);
        }
        if (points == null) {
            return;
        }
        pickUpMarker = addMarker(points.get(0), true);

        dropOffMarker = addMarker(points.get(points.size() - 1), false);
        updateCamera(pickUpMarker.getPosition(), dropOffMarker.getPosition());
        if(hideEndPoint)
            dropOffMarker.remove();
        // Drawing polyline in the Google Map for the i-th route
        mMap.addPolyline(lineOptions);

    }

    private void updateCamera(LatLng pickUp, LatLng dropOff) {
        LatLngBounds locationBounds = new LatLngBounds.Builder()
                .include(pickUp).include(dropOff).build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(locationBounds, 100));
        mMap.setLatLngBoundsForCameraTarget(locationBounds);
    }

    private Marker addMarker(LatLng target, boolean isPickUp) {
        return mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(isPickUp ? BitmapDescriptorFactory.HUE_GREEN :
                        BitmapDescriptorFactory.HUE_BLUE))
                .position(target)
                .flat(true));
    }

    public void removeMarkers() {
        if (pickUpMarker != null && dropOffMarker != null) {
            pickUpMarker.remove();
            dropOffMarker.remove();
            mMap.clear();
        }
    }

    private List<List<HashMap<String, String>>> parse(JSONObject jObject) {

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;
        Double distanceMain = null;

        try {

            jRoutes = jObject.getJSONArray("routes");


            for (int i = jRoutes.length() - 1; i >= 0; i--) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                Double distanceInternal = jLegs.getJSONObject(0).getJSONObject("distance").getDouble("value");
                if (distanceMain == null || distanceInternal < distanceMain) {
                    distanceMain = distanceInternal;
                } else
                    break;
                List path = new ArrayList<HashMap<String, String>>();


                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");


                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);


                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
                            hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        return routes;
    }

    /***
     * decode encoded polyline" : {
     "points" : "}m_wC{jayKKf@Qv@]fBWhAMn@Ot@UfAMf@SbAS|@Mn@EN"
     },--> appropriate to lat lng coordinates so we can draw polyline
     we can also use MapUtil class decode polyline method instead of this
     * @param encoded the  encodes points receive from google map service
     * @return decoded lat/lng points for polyline
     */

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }


}