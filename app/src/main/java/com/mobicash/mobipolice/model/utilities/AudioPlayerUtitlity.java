package com.mobicash.mobipolice.model.utilities;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;

/**
 * Created by Zeera on 9/24/2017 bt ${File}
 */

public class AudioPlayerUtitlity {
    private MediaPlayer mMediaPlayer;

    public void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    public void play(Context c, int rid, @Nullable final IAudioCompleteListener listener) {
        stop();
        mMediaPlayer = MediaPlayer.create(c, rid);
        mMediaPlayer.setOnCompletionListener(mp -> {
            stop();
            if(listener!=null)
                listener.audioEnded();
        });

        mMediaPlayer.start();
    }



    public interface IAudioCompleteListener{
        void audioEnded();
    }
}
