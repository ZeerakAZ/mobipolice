package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IItemClickListener;

import java.util.ArrayList;

/**
 * Created by ambre on 6/4/2017.
 */

public class ShareContactListAdapter extends RecyclerView.Adapter<ShareContactListAdapter.CustomViewHolder>{

    private ArrayList<String> list;
    private Context context;
    private IItemClickListener mListener;

    public ShareContactListAdapter(ArrayList<String> list, Context context, IItemClickListener listener){
        this.list=list;
        this.context=context;
        mListener = listener;
    }



    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_number_list,parent,false);
        CustomViewHolder holder=new CustomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
       holder.number.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{
        private TextView number;

        public CustomViewHolder(View itemView) {
            super(itemView);
            number= itemView.findViewById(R.id.number);
            itemView.setOnClickListener(v -> mListener.onClick(v,getAdapterPosition()));
        }
    }



}
