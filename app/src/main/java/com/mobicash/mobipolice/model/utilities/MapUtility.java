package com.mobicash.mobipolice.model.utilities;

import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;

import com.google.android.gms.maps.model.CircleOptions;
import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.views.activities.BaseActivity;
import com.mobicash.mobipolice.views.fragments.BaseFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Zeera on 5/2/2017.
 */

public class MapUtility {
    public static final int DEFAULT_ZOOM = 14;

    public static void addMarker(GoogleMap map, LatLng position,
                                 @Nullable Object object, boolean isToMove, boolean isGreen) {
        if (map == null)
            return;
        Marker m = map.addMarker(new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.defaultMarker(isGreen ? BitmapDescriptorFactory.HUE_GREEN :
                        BitmapDescriptorFactory.HUE_RED)));
        if (isToMove)
            animateCamera(position, map);
        if (object != null) {
            m.setTag(object);
        }
    }

    public static Marker addMarker(GoogleMap map, LatLng position, @DrawableRes int imageResId, @Nullable Object object) {

        Marker m = map.addMarker(new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.fromResource(imageResId)));
        if (object != null) {
            m.setTag(object);
        }
        return m;
    }

    public static void animateCamera(LatLng pos, GoogleMap map) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, DEFAULT_ZOOM));
    }


    public static float getHue(String color) {
        color = color.replace("#", "");
        double r = Integer.parseInt(color.substring(0, 2), 16) / 255.0;
        double g = Integer.parseInt(color.substring(2, 4), 16) / 255.0;
        double b = Integer.parseInt(color.substring(4, 6), 16) / 255.0;

        double hue = 0;
        if ((r >= g) && (g >= b)) {
            hue = 60 * (g - b) / (r - b);
        } else if ((g > r) && (r >= b)) {
            hue = 60 * (2 - (r - b) / (g - b));
        }
        //... continue here
        return (float) hue;
    }

    public static void getAddressFromLocation(
            final LatLng location, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    List<Address> list = geocoder.getFromLocation(
                            location.latitude, location.longitude, 1);
                    if (list != null && list.size() > 0) {
                        Address address = list.get(0);
                        // sending back first address line and locality
                        result = address.getAddressLine(0) + ", " + address.getLocality();
                    }
                } catch (IOException e) {
                    //Log.e(TAG, "Impossible to connect to Geocoder", e);
                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (result != null) {
                        msg.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        msg.setData(bundle);
                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }

    public static void loadMapFragment(BaseFragment fragment, OnMapReadyCallback callback){
        SupportMapFragment mapFragment =
                (SupportMapFragment) fragment.
                        getChildFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(callback);
    }

    public static void loadMapFragment(BaseActivity activity, OnMapReadyCallback callback){
        SupportMapFragment mapFragment =
                (SupportMapFragment) activity.
                        getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(callback);
    }

    public static void killMapFragment(BaseFragment fragment){
        SupportMapFragment mapFragment = ((SupportMapFragment)
                fragment.getChildFragmentManager().findFragmentById(R.id.map_fragment));

        if (mapFragment != null) {
            FragmentManager fM = fragment.getChildFragmentManager();
            fM.beginTransaction().remove(mapFragment).commit();
        }

    }

    public static String getStringFromLatLng(LatLng latLng) {
        return String.valueOf(latLng.latitude) +
                "," +
                latLng.longitude;
    }

    public static void drawCircleOnMap(LatLng centerPoint,GoogleMap map){
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(centerPoint);
        circleOptions.radius(100);
        circleOptions.strokeColor(Color.RED);
        circleOptions.fillColor(0x30ff0000);
        circleOptions.strokeWidth(2);
        map.addCircle(circleOptions);
    }
}
