package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import org.json.JSONException
import org.json.JSONObject

import java.util.HashMap

/**
 * Created by Zeera on 7/8/2017 bt ${File}
 */

class UserDeviceToken(@field:SerializedName("TokenId")
                      @field:Expose
                      private val tokenId: String, @field:SerializedName("DeviceId")
                      @field:Expose
                      private val deviceId: String) {
    @SerializedName("DeviceType")
    @Expose
    private val deviceType: String
    @SerializedName("Active")
    @Expose
    private val active: Boolean?


    val hashMap: HashMap<String, String>
        get() {
            val value = HashMap<String, String>()
            value["DeviceId"] = deviceId
            value["TokenId"] = tokenId
            value["Active"] = active!!.toString()
            value["DeviceType"] = deviceType
            return value
        }

    //return new JSONObject().put("userToken",jsonObject);
    val jsonObj: JSONObject
        get() {

            try {
                val jsonObject = JSONObject()
                jsonObject.put("DeviceId", deviceId)
                jsonObject.put("TokenId", tokenId)
                jsonObject.put("Active", active)
                jsonObject.put("DeviceType", deviceType)
                return jsonObject
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return JSONObject()
        }

    init {
        deviceType = "android"
        active = true
    }

}
