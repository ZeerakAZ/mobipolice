package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTypeModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ambre on 5/18/2017.
 */

public class MapDialogAdapter extends RecyclerView.Adapter<MapDialogAdapter.CustomViewHolder> {
    private ArrayList<CrimeTypeModel> list;
    private Context context;
    private Set<Integer> mSelectedIds;

    public MapDialogAdapter(ArrayList<CrimeTypeModel> list, Context context) {
        this.context = context;
        this.list = list;
        mSelectedIds = new HashSet<>();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_list_map, parent, false);
       // view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        CrimeTypeModel item = list.get(position);
        holder.mainText.setText(item.getCrimeTypeName());
        holder.colorDisplay.setBackgroundColor(Color.parseColor(item.getColorCode()));
        holder.checkBox.setImageResource(mSelectedIds.contains(item.getCrimeTypeId())
                ? R.drawable.check_ac : R.drawable.check_ua);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView mainText;
        private ImageView checkBox;
        private View colorDisplay;

        CustomViewHolder(View itemView) {
            super(itemView);
            mainText = itemView.findViewById(R.id.maintext);
            checkBox = itemView.findViewById(R.id.iv_check);
            colorDisplay = itemView.findViewById(R.id.colordisplay);
            checkBox.setOnClickListener(view -> {
                int typeId = list.get(getAdapterPosition()).getCrimeTypeId();
                if(mSelectedIds.contains(typeId)){
                    mSelectedIds.remove(typeId);
                }else
                    mSelectedIds.add(typeId);
                notifyItemChanged(getAdapterPosition());
            });
            ((LinearLayout) checkBox.getParent()).setOnClickListener(view -> checkBox.performClick());
        }
    }

    public Set<Integer> getmSelectedIds() {
        return mSelectedIds;
    }

    public void setmSelectedIds(Set<Integer> mSelectedIds) {
        this.mSelectedIds.retainAll(mSelectedIds);
        notifyDataSetChanged();
    }
}
