package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.SerializedName

data class ProvinceModel(
    @SerializedName("id") val id: Int,
    @SerializedName("value") val value: String,
    @SerializedName("defaultValue") val defaultValue: Boolean
){
    override fun toString(): String {
        return value
    }
}