package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.MemberModel;
import com.mobicash.mobipolice.model.utilities.CircleTransform;
import com.mobicash.mobipolice.views.fragments.AddMemberFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MemberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<MemberModel> mData;
    private Context mContext;
    private AddMemberFragment fragment;

    public MemberAdapter(ArrayList<MemberModel> mData, AddMemberFragment fragment, Context mContext) {
        this.mData = mData;
        this.mContext = mContext;
        this.fragment = fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MemberModel item = mData.get(position);
        ViewHolder holder1 = (ViewHolder) holder;
        holder1.tvUserName.setText(item.getName());
        holder1.ivMemberRemove.setVisibility(View.VISIBLE);
        if (item.getImage() != null && !item.getImage().isEmpty())
            Picasso.with(mContext).load(item.getImage()).
                    placeholder(R.drawable.profile).
                    transform(new CircleTransform()).
                    into(holder1.ivUserImage);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.iv_user_image)
        ImageView ivUserImage;
        @BindView(R.id.iv_member_remove)
        ImageView ivMemberRemove;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.iv_member_remove)
        public void removeMember() {
            fragment.removeMember(mData.get(getAdapterPosition()).getId());
            mData.remove(getAdapterPosition());
            notifyItemRemoved(getAdapterPosition());
        }
    }


}
