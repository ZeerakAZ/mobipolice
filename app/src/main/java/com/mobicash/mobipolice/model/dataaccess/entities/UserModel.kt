package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobicash.mobipolice.presenter.BasePresenter

import java.io.Serializable

/**
 * Created by Zeera on 3/27/2018 bt ${File}
 */

class UserModel : Serializable {

    @SerializedName("FullName")
    @Expose
    var fullname: String? = null
        private set
    @SerializedName("Location")
    @Expose
    private var location: String? = null
    @SerializedName("Mobile")
    @Expose
    private var mobile: String? = null
    @SerializedName("UserId")
    @Expose
    private var mobiUserId: String? = null
    @SerializedName("UserName")
    @Expose
    private var userName: String? = null
    @SerializedName("UserImage")
    @Expose
    var profileurl: String? = null
        private set
    @SerializedName("UserCity")
    @Expose
    private var userCity: String? = null
    @SerializedName("UserToken")
    @Expose
    private var userToken: UserDeviceToken? = null
    @SerializedName("Status")
    @Expose
    private var status: Boolean? = null
    @SerializedName("EntHost")
    @Expose
    private val entHost: String? = null
    @SerializedName("EntHostIP")
    @Expose
    private val entHostIP: String? = null
    @SerializedName("UserType")
    @Expose
    private val userType = BasePresenter.USER_TYPE
    @SerializedName("Id" + "")
    private var userId: Int? = null


    val userid: Int
        get() = userId!!

    constructor() {}

    constructor(userId: Int, userName: String) {
        this.fullname = userName
        this.userId = userId
    }

    constructor(userId: Int, string: String, string1: String, string2: String, string3: String) {}

    constructor(id: Int?, fullName: String, userImage: String) {
        this.userId = id
        this.fullname = fullName
        this.profileurl = userImage
    }


    fun setUserToken(userToken: UserDeviceToken): UserModel {
        this.userToken = userToken
        return this
    }

    fun setStatus(status: Boolean?): UserModel {
        this.status = status
        return this
    }

    fun setUserId(userid: Int): UserModel {
        this.userId = userid
        return this
    }

    fun setFullName(fullName: String?): UserModel {
        this.fullname = fullName
        return this
    }

    fun setLocation(location: String?): UserModel {
        this.location = location
        return this
    }

    fun setMobile(mobile: String?): UserModel {
        this.mobile = mobile
        return this
    }

    fun setMobiUserId(mobiUserId: String?): UserModel {
        this.mobiUserId = mobiUserId
        return this
    }

    fun setUserName(userName: String?): UserModel {
        this.userName = userName
        return this
    }

    fun setUserImage(userImage: String?): UserModel {
        this.profileurl = userImage
        return this
    }

    fun setUserCity(userCity: String): UserModel {
        this.userCity = userCity
        return this
    }
}
