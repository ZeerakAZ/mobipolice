package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.SerializedName

data class FinancialLoginModel(
    @SerializedName("clientPin") val clientPin: String,
    @SerializedName("clientNumber") val clientNumber: String,
    @SerializedName("imei") val imei: String,
    @SerializedName("city") val city: String,
    @SerializedName("UserToken") val userToken: UserDeviceToken
)