package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Zeera on 8/6/2017 bt ${File}
 */

class MissingPersonTipModel {

    @SerializedName("PersonId")
    @Expose
    private var personId: Int? =null
    @SerializedName("UserId")
    @Expose
    private var userId: Int? =null
    @SerializedName("Tip")
    @Expose
    var tip: String? = null
        private set
    @SerializedName("EntHost")
    @Expose
    private val entHost: String? = null
    @SerializedName("EntHostIP")
    @Expose
    private val entHostIP: String? = null
    @SerializedName("EntIMEI")
    @Expose
    private val entIMEI: String? = null
    var tipBy: String? = null
        private set
    var tipById: Int? = null
    var tipByUserImage: String?=null

    constructor(personId: Int?, userId: Int?, tip: String) {
        this.personId = personId
        this.userId = userId
        this.tip = tip
    }

    constructor(tip: String, tipBy: String, tipById: Int, tipByUserImage: String) {
        this.tip = tip
        this.tipBy = tipBy
        this.tipById = tipById
        this.tipByUserImage = tipByUserImage
    }

    constructor(tip: String, tipBy: String) {
        this.tip = tip
        this.tipBy = tipBy
    }
}
