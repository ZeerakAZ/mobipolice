package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mobicash.mobipolice.views.fragments.CrimeListDetailsFragment;

import java.util.ArrayList;

/**
 * Created by zeerak on 5/12/2018 bt ${File}
 */
public class CrimePagerAdapter extends FragmentStatePagerAdapter{
    //private ArrayList<CrimeListDetailsFragment>

    public CrimePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 0;
    }
}
