package com.mobicash.mobipolice.model.dataaccess.entities;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.mobicash.mobipolice.model.dataaccess.interfaces.BottomOptionActions;

/**
 * Created by Zeera on 12/16/2017 bt ${File}
 */

public class BottomSheetOptionModel {
    @BottomOptionActions
    private int uniqueId;
    @StringRes
    private int optionNameId;
    @DrawableRes
    private Integer optionImageId;

    public BottomSheetOptionModel(int uniqueId, int optionNameId, Integer optionImageId) {
        this.uniqueId = uniqueId;
        this.optionNameId = optionNameId;
        this.optionImageId = optionImageId;
    }

    public BottomSheetOptionModel(int uniqueId, int optionNameId) {
        this.uniqueId = uniqueId;
        this.optionNameId = optionNameId;
    }

    @BottomOptionActions
    public int getUniqueId() {
        return uniqueId;
    }

    public int getOptionNameId() {
        return optionNameId;
    }

    public Integer getOptionImageId() {
        return optionImageId;
    }
}
