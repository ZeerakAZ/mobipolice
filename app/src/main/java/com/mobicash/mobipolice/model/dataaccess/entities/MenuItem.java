package com.mobicash.mobipolice.model.dataaccess.entities;

import android.view.View;

/**
 * Created by Zeera on 3/4/2017.
 */

public class MenuItem {
    private int nameId;
    private int iconIdActivated;
    private int iconIdDeactivated;
    private boolean isSelected;
    private boolean hideForLogin;

    public MenuItem(int nameId, int iconIdActivated, int iconIdDeactivated) {
        this.nameId = nameId;
        this.iconIdActivated = iconIdActivated;
        this.iconIdDeactivated = iconIdDeactivated;
    }

    public MenuItem(int nameId, int iconIdActivated, int iconIdDeactivated, boolean hideForLogin) {
        this.nameId = nameId;
        this.iconIdActivated = iconIdActivated;
        this.iconIdDeactivated = iconIdDeactivated;
        this.hideForLogin = hideForLogin;
    }

    public MenuItem(int nameId, View.OnClickListener listener) {
        this.nameId = nameId;
    }

    public int getNameId(){
        return nameId;
    }

    public int getIconIdActivated(){
        return iconIdActivated;
    }

    public int getIconIdDeactivated(){
        return iconIdDeactivated;
    }

    public void setSelected(boolean isSelected){
        this.isSelected=isSelected;
    }

    public boolean getIsSelected(){
        return isSelected;
    }


    public boolean isHideForLogin() {
        return hideForLogin;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MenuItem && (((MenuItem) obj).getNameId() == this.getNameId());
    }
}
