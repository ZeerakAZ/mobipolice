package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.SerializedName

data class DistrictModel(
    @SerializedName("id") val id: Int,
    @SerializedName("value") val name: String,
    @SerializedName("parentId") val provinceId: Int,
    @SerializedName("defaultValue") val defaultValue: Boolean
){
    override fun toString(): String {
        return name
    }
}