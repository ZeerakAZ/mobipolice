package com.mobicash.mobipolice.model.utilities;

import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;



/**
 * Created by Zeera on 4/22/2017.
 */

public class KeyBoardHelper {

    boolean isOpened = false;

    public void setListenerToRootView(Activity ctx, final IKeyBoardOpen listener) {
        final View activityRootView = ctx.getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {

            int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
            if (heightDiff > 100) { // 99% of the time the height diff will be due to a keyboard.
                //Toast.makeText(getApplicationContext(), "Gotcha!!! softKeyboardup", Toast.LENGTH_SHORT).show();
                if (!isOpened) {
                    listener.keyBoardStateChanged(true);
                   //scrollMyListViewToBottom();
                    //Do two things, make the view top visible and the editText smaller
                }
                isOpened = true;
            } else if (isOpened) {
                //Toast.makeText(getApplicationContext(), "softkeyborad Down!!!", Toast.LENGTH_SHORT).show();
                listener.keyBoardStateChanged(false);
                isOpened = false;
            }
        });
    }


    public interface IKeyBoardOpen {
        void keyBoardStateChanged(boolean isOpen);
    }

}
