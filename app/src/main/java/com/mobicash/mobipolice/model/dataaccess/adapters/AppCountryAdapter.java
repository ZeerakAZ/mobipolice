package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.CountryUserModel;

import java.util.ArrayList;
import java.util.Collections;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Zeera on 1/28/2018 bt ${File}
 */

public class AppCountryAdapter extends RecyclerView.Adapter<AppCountryAdapter.ViewHolder> {
    private ArrayList<CountryUserModel> mCountryUser;
    private Context mContext;


    public AppCountryAdapter(Context mContext, ArrayList<CountryUserModel> mUser) {
        this.mCountryUser = mUser;
        this.mContext = mContext;
        Collections.sort(mCountryUser);
    }

    @Override
    public AppCountryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_app_country, parent, false);
        return new AppCountryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppCountryAdapter.ViewHolder holder, int position) {
        holder.bindData(mCountryUser.get(position));
    }

    @Override
    public int getItemCount() {
        return mCountryUser == null ? 0 : mCountryUser.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTvUserCount;
        TextView mTvCountryName;

        CircleImageView mCountryFlag;

        public ViewHolder(View itemView) {
            super(itemView);
            mTvUserCount = itemView.findViewById(R.id.tv_user_count);
            mCountryFlag = itemView.findViewById(R.id.iv_country_flag);
            mTvCountryName = itemView.findViewById(R.id.tv_country_name);

        }

        void bindData(CountryUserModel countryUserModel) {
            mTvUserCount.setText(String.valueOf(countryUserModel.getNumberOfUsers()));
            mCountryFlag.setImageResource(countryUserModel.getCountryFlag(mContext));
            mTvCountryName.setText(countryUserModel.getCountryName());
        }


    }


}

