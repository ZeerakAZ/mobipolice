package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

/**
 * Created by Zeera on 4/2/2017.
 */

public class WantedModel : Serializable {

    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("Code")
    @Expose
    var code: String? = null
    @SerializedName("FirstName")
    @Expose
    var firstName: String? = null
    @SerializedName("MiddleName")
    @Expose
    var middleName: String? = null
    @SerializedName("LastName")
    @Expose
    var lastName: String? = null
    @SerializedName("Age")
    @Expose
    var age_: Int? = null
    @SerializedName("Height")
    @Expose
    var height: Double? = null
    @SerializedName("Weight")
    @Expose
    var weight: Int? = null
    @SerializedName("WantedFor")
    @Expose
    var wantedFor: String? = null
    @SerializedName("WantedImageURL")
    @Expose
    var wantedImageURL: String? = null
    @SerializedName("CrimeCategoryId")
    @Expose
    var crimeCategoryId: Int? = null
    @SerializedName("CrimeCategory")
    @Expose
    var crimeCategory: String? = null
    @SerializedName("Active")
    @Expose
    var active: Boolean? = null
    @SerializedName("IsCaptured")
    @Expose
    var isCaptured: Boolean? = null
    @SerializedName("EntDate")
    @Expose
    var entDate: String? = null
    @SerializedName("EntHost")
    @Expose
    var entHost: String? = null
    @SerializedName("EntHostIP")
    @Expose
    var entHostIP: String? = null
    @SerializedName("EntUserId")
    @Expose
    var entUserId: Int? = null

    val criminalInfo: String
        get() = "$lastName, $firstName ($wantedFor)"

    val compeleteName: String
        get() = "$lastName $middleName $firstName"

    val heigth: String
        get() = height!!.toString() + " feet"

    val weigth: String
        get() = weight!!.toString() + " KGs"

    fun getAge(): String {
        return "Age $age_ years"
    }

}
