package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.GroupModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.presenter.ChatPresenter;
import com.mobicash.mobipolice.views.activities.BaseActivity;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.mobicash.mobipolice.views.fragments.ChatFragment;
import com.mobicash.mobipolice.views.fragments.ChatListFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 4/17/2017.
 */

public class GroupChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<GroupModel> mData;
    private BaseActivity mContext;

    public GroupChatAdapter(ArrayList<GroupModel> mData, BaseActivity mContext) {
        this.mData = mData;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat, null);
        GroupChatAdapter.ViewHolder holder = new GroupChatAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        GroupModel item = mData.get(position);
        GroupChatAdapter.ViewHolder holder1 = (GroupChatAdapter.ViewHolder) holder;
        holder1.tvUserName.setText(item.getName());
        /*Picasso.with(mContext).load(item.getUserImage()).
                placeholder(R.drawable.profile).
                transform(new CircleTransform()).
                into(holder1.ivUserImage);*/
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.iv_user_image)
        ImageView ivUserImage;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ((RelativeLayout) tvUserName.getParent()).setOnClickListener(view -> {
                FragmentUtility.withManager(mContext.getSupportFragmentManager())
                        .addToBackStack(ChatListFragment.TAG)
                        .withAnimationType(FragmentAnimationType.SLIDE_FROM_LEFT)
                        .replaceToFragment(ChatFragment.newInstance(mData.get(getAdapterPosition()),
                                ChatFragment.TYPE_GROUP));

                new ChatPresenter(mContext, null).
                        getAllMessagesOfGroup(mData.get(getAdapterPosition()).getId());


            });
        }
    }
}
