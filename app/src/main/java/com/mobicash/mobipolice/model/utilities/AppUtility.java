package com.mobicash.mobipolice.model.utilities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.annotation.ArrayRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.mobicash.mobipolice.views.dialogs.GenericDialog;
import com.mobicash.mobipolice.views.dialogs.IDialogDouble;
import com.mobicash.mobipolice.views.fragments.BaseFragment;
import com.mobicash.mobipolice.views.fragments.DatePickerFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by Zeera on 3/11/2018 bt ${File}
 */

public class
AppUtility {
    public static void startGoogleMapNavigation(double destinationLatitude,
                                                double destinationLongitude,
                                                Context context) {
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)",
                destinationLatitude, destinationLongitude, "");
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        context.startActivity(intent);
    }

    public static String getStaticMapUrl(Location location) {

        return String.format(Locale.ENGLISH, "https://maps.googleapis.com/maps/api/staticmap?" +
                        "center=%1$s,%2$s&zoom=15&markers=color:red||%1$s,%2$s&size=400x400&" +
                        "key=AIzaSyAl5vSX9sroX15Uh-MwUGzigGQ3F_R6nv0", String.valueOf(location.getLatitude())
                , String.valueOf(location.getLongitude()));
    }

    public static void startSmsScreen(String number, Context context) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + number));
        sendIntent.putExtra("sms_body", context.getString(R.string.message_share));
        context.startActivity(sendIntent);
    }

    public static void startNewActivity(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void loadHtml(Context context, WebView webView, int id) {
        String javascrips = null;
        try {
            InputStream input = context.getResources().openRawResource(id);
            Reader is = new BufferedReader(
                    new InputStreamReader(input, "windows-1252"));


            //InputStream input = getAssets().open("ws.TXT");
            int size;
            size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();
            // byte buffer into a string
            javascrips = new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // String html = readFile(is);
        WebSettings w = webView.getSettings();
        w.setPluginState(WebSettings.PluginState.ON);
        w.setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
        });
        webView.loadDataWithBaseURL("file:///android_res/raw/", javascrips, "text/html",
                "UTF-8", null);

    }

    public static void setHtmlToWebView(WebView webView, String html) {
        WebSettings w = webView.getSettings();
        w.setPluginState(WebSettings.PluginState.ON);
        w.setJavaScriptEnabled(true);
       /* webView.setWebChromeClient(new WebChromeClient() {
        });*/
        webView.loadDataWithBaseURL("file:///android_res/raw/", html, "text/html",
                "UTF-8", null);
    }

    @Nullable
    public static String getHtml(Context context, int id) {
        String javascrips = null;
        try {
            InputStream input = context.getResources().openRawResource(id);
            Reader is = new BufferedReader(
                    new InputStreamReader(input, "windows-1252"));


            //InputStream input = getAssets().open("ws.TXT");
            int size;
            size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();
            // byte buffer into a string
            return new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressLint("HardwareIds")
    public static String getUniqueDeviceId(Context context) {

        String serialNumber = Build.SERIAL != Build.UNKNOWN ? Build.SERIAL : Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        LogUtility.debugLog(serialNumber);
        return serialNumber;
    }

    @Nullable
    public static String getFCMToken() {
        String fcmToken;
        if (FirebaseInstanceId.getInstance().getToken() != null) {
            fcmToken = FirebaseInstanceId.getInstance().getToken();
        } else {
            fcmToken = null;
        }
        return fcmToken;
    }

    @Nullable
    public static String getCountryNameByCode(Context context, String code) {
        //Get Data From Text Resource File Contains Json Data.
        InputStream inputStream = context.getResources().openRawResource(R.raw.country);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int ctr;
        try {
            ctr = inputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LogUtility.debugLog(byteArrayOutputStream.toString());
        try {
            // Parse the data into jsonobject to get original data in form of json.
            JSONArray jCountries = new JSONArray(
                    byteArrayOutputStream.toString());
            for (int i = 0; i < jCountries.length(); i++) {
                JSONObject jCountry = jCountries.getJSONObject(i);
                String dialCode = jCountry.getString("dial_code");
                if (code.replace("+", "").equals(dialCode.replace("+", "")))
                    return jCountry.getString("name");
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void showLoginDialog(final Context context) {
        new GenericDialog(context, context.getString(R.string.ph_alert), context.getString(R.string.message_login),
                context.getString(R.string.ph_dismiss), context.getString(R.string.ph_login),
                new IDialogDouble() {
                    @Override
                    public void onRightClick(DialogInterface dialogInterface) {
                        if (context instanceof MainActivity)
                            ((MainActivity) context).toLoginScreen();
                    }

                    @Override
                    public void onLeftClick(DialogInterface dialogInterface) {
                        dialogInterface.dismiss();
                    }
                }).setLogo(R.drawable.dialog_info).show();
    }

    public static void shareText(Context context, String text) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        //sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public static void launchUrl(Activity context, String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    public static String getEncodedString(String str) {
        try {
            return URLEncoder.encode(str, "utf-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return str;
        }
    }

    public static String getDecodedString(String str) {
        try {
            return URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return str;
        }
    }

    public static void hideKeyboard(View view, Context context) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void startSpeachListener(BaseFragment fragment, int requestCode) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                fragment.getString(R.string.speech_prompt));
        try {
            fragment.startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException a) {
            ToastUtility.showToastForLongTime(fragment.getContext(),
                    fragment.getString(R.string.speech_not_supported));
        }
    }

    public static void showDatePickerDialog(FragmentManager fm, final TextView setDate, Context ctx) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Call some material design APIs here
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.setMonthYearOnly(true);
            newFragment.set_iDatePickerListener((year, month, day) -> {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, day);
                setDate.setText(new SimpleDateFormat("MMM dd yyyy", Locale.getDefault())
                        .format(newDate.getTime()));
            });
            newFragment.show(fm, "datePicker");
            newFragment.setTolimit(true);
            newFragment.setToMinimum(false);
        } else {
            Calendar newCalendar = Calendar.getInstance();
            DatePickerDialog fromDatePickerDialog = new DatePickerDialog(ctx, (view, year, monthOfYear, dayOfMonth) -> {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                setDate.setText(new SimpleDateFormat("MMM dd yyyy", Locale.getDefault())
                        .format(newDate.getTime()));
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            fromDatePickerDialog.show();
            // Implement this feature without material design
        }

    }


    public static void showDatePickerDialog(Context ctx, FragmentManager fm, final TextView setDate
            , String dateFormat, boolean futureDates) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Call some material design APIs here
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.setMonthYearOnly(true);
            newFragment.set_iDatePickerListener((year, month, day) -> {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, day);
                setDate.setText(new SimpleDateFormat(dateFormat, Locale.getDefault())
                        .format(newDate.getTime()));
            });
            newFragment.show(fm, "datePicker");
            newFragment.setTolimit(true);
            newFragment.setToMinimum(futureDates);
        } else {
            Calendar newCalendar = Calendar.getInstance();
            DatePickerDialog fromDatePickerDialog = new DatePickerDialog(ctx, (view, year, monthOfYear, dayOfMonth) -> {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                setDate.setText(new SimpleDateFormat(dateFormat, Locale.getDefault())
                        .format(newDate.getTime()));
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            fromDatePickerDialog.show();
            // Implement this feature without material design
        }

    }

    public static void showTimePickerDialog(Context ctx, final TextView textView) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(ctx, (timePicker, i, i1)
                -> textView.setText(convertTo24(i, i1)), 12, 0, false);
        timePickerDialog.setTitle("");
        timePickerDialog.show();
    }

    private static String convertTo24(int hours, int mins) {

        if (hours >= 12) {
            if (hours > 12)
                hours = hours % 12;
            return String.format("%s:%s pm", appendZero(hours), appendZero(mins));
        } else {
            if (hours == 0)
                hours = 12;
            return String.format("%s:%s am", appendZero(hours), appendZero(mins));
        }
    }

    private static String appendZero(int value) {
        return value < 10 ? "0" + value : "" + value;
    }

    public static void vibrateMobile(Context context, long duration) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (v != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                //deprecated in API 26
                v.vibrate(duration);
            }
        }

    }


}
