package com.mobicash.mobipolice.model.utilities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;


/**
 * Created by Administrator on 12/1/2016.
 */

public class ServiceUtitlity {

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context ctx) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void stopService(Context context, Class serviceClass) {
        Intent i = new Intent(context, serviceClass);
        context.stopService(i);
    }

    public static boolean startService(Context context, Class serviceClass) {
        Intent i = new Intent(context, serviceClass);
        context.startService(i);
        return true;
    }
}
