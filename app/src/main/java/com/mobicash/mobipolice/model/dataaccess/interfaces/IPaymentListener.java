package com.mobicash.mobipolice.model.dataaccess.interfaces;

/**
 * Created by Zeera on 10/22/2017 bt ${File}
 */

public interface IPaymentListener {
    void paymentSuccess();
    void paymentFailed();
}
