package com.mobicash.mobipolice.model.dataaccess.network;

/**
 * Created by Zeera on 6/28/2017 bt ${File}
 */

public interface APIsEndPoints {
    String GET_BREAKING_NEWS = "/api/BreakingNews/GetBreakingNews";
    String GET_ACTIVE_USER = "/api/Users/GetUsersCount";
    String SEND_SOS_TO_POLICE = "/api/SOS/SendAlertToPoliceNetworkGroup";
    String SEND_SOS_TO_FRIENDS = "/api/SOS/SendAlertToFamilyandFriends";
    String SEND_SOS_TO_POLICE_ALL = "/api/SOS/SendAlertToPolice";
    String GET_CRIMES = "/api/ReportCrime/GetReportCrime";
    String GET_CRIMES_BY_LOCATION = "/api/ReportCrime/GetNearByLocationCrimes";
    String INACTIVE_ACCOUNT = "/api/Users/InActiveUser/{Id}";
    String LOGOFF_USER = "/api/Users/LogoutUser/{Id}";
    String UPDATE_MEMBER = "/api/SOS/AddORUpdateFriends";
    String GET_MEMBERS = "/api/SOS/GetFriendsOfUser";
    String SEARCH_FRIENDS = "/api/SOS/SearchFriend";
    String UPDATE_SOS_MESSAGE = "/api/SOS/UpdateSOSAlertMessage";
    String GET_USER_DETAILS = "/api/Users/GetUser/{Id}";
    String NEARBY_PLACES = "place/radarsearch/json";
    String GET_DIRECTIONS = "directions/json";
    String LOG_IN = "/api/Users/Login";
    String GET_GROUP_CHATS = "/api/Chat/GetChatGroups";
    String GET_RECEIVED_MESSAGES = "/api/Chat/GetAllRecievedMessagesOfUser";
    String GET_LAST_MESSAGE = "/api/Chat/GetLastMessagesOfCurrentChat";
    String GET_ALL_USER_MESSAGES = "/api/Chat/GetAllMessagesOfUser";
    String GET_RECEIVED_MESSAGE_GROUPS = "/api/Chat/GetRecievedMessagesOfGroup";
    String REPORT_CRIME = "/api/ReportCrime/Post";
    String POST_MESSAGE = "/api/Chat/PostChatMessage";
    String POST_GROUP_MESSAGE = "/api/Chat/PostGroupChatMessage";
    String GET_RECENT_CHAT_USER = "/api/Chat/GetRecentChatUsers";
    String GET_FAV_USER = "/api/Chat/GetFavoriteUsers";
    String SEARCH_USER ="/api/Chat/SearchUsers";
    String POST_FAV_USER = "/api/Chat/PostFavoriteUser";
    String UPDATE_TOKEN = "/api/Users/UpdateUserDeviceToken/{Id}";
    String POST_USER = "/api/Users/PostUser";
    String GET_CRIME_TYPES = "api/Wanted/GetCrimeTypes";
    String UPLOAD_FILE = "/filesAPI/Files/UploadFile";
    String GET_WANTED = "api/Wanted/GetWanted";
    String POST_TIP = "/api/CrimeTips/PostCrimeTip";
    String POST_MISSING_PERSON = "/api/MissingPerson/PostMissingPerson";

    String GET_MISSING_PERSON = "/api/MissingPerson/GetMissingPerson";
    String POST_MISSING_PERSON_TIP = "/api/MissingPerson/PostMissingPersonTip";
    String GET_MISSING_PERSON_TIP = "/api/MissingPerson/GetMissingPersonTip";
    String MARK_FOUND = "/api/MissingPerson/MissingPersonFound";
    String UPDATE_POLICE_LOCATION = "/api/Users/UpdatePoliceLocation/{Id}";
    String GET_POLICE_LOCATIONS = "/api/Users/GetPoliceLocation";
    String GET_PROVINCE = "getProvince.php";
    String GET_DISTRICTS = "getDistrict.php";
    String CHECK_NUMBER = "check_Client.php";
    String REGISTER_FINANCIAL_USER = "/api/Users/FinancialRegistration";
    String FINANCIAL_LOGIN = "/api/Users/FinancialLogin";
    String GET_BEACON_STATUS = "/api/Assets/GetBeaconsStatus";
}
