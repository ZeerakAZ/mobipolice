package com.mobicash.mobipolice.model.dataaccess.network;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Zeera on 6/28/2017 bt ${File}
 */

public class ApiClient {

    private static Retrofit retrofit = null;
    private static Retrofit retrofitFiniacial = null;
    private static Retrofit retrofitMap = null;
    private static Retrofit retrofitFileUpload = null;
    private static Retrofit retrofitMobiSquid = null;

    public static final String BASE_URL = "http://52.38.75.235:8081";
    public static final String BASE_URL_ECOMMERCE="http://52.38.75.235:8080";
    public static final String BASE_URL_FINIACIAL="http://test.mcash.rw";
    private static final String BASE_URL_MAPS = "https://maps.googleapis.com/maps/api/";
    public static final String BASE_URL_UPLOAD = "http://52.38.75.235:80";

    public static final String BASE_MOBI_CASH_RW = "http://test.mobicash.co.za/bio-api/mobiSquid/";

    final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request.Builder ongoing = chain.request().newBuilder();
                    ongoing.addHeader("Accept", "application/json;versions=1");
                    if (/*isUserLoggedIn()*/true) {
                        ongoing.addHeader("Authorization", " Basic Q1BBUEkwMDE6QVBJQ1BBdXRoMDAx");
                    }
                    return chain.proceed(ongoing.build());
                }
            })
            .build();


    private static Retrofit getClient() {
        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    private static Retrofit getMobiSquidClient() {
        if (retrofitMobiSquid == null) {

            retrofitMobiSquid = new Retrofit.Builder()
                    .baseUrl(BASE_MOBI_CASH_RW)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitMobiSquid;
    }

    public static Retrofit getFiniacialClient(){
        if (retrofitFiniacial == null) {
            retrofitFiniacial = new Retrofit.Builder()
                    .baseUrl(BASE_URL_FINIACIAL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitFiniacial;
    }


    public static Retrofit getMapClient() {
        if (retrofitMap == null) {
            retrofitMap = new Retrofit.Builder()
                    .baseUrl(BASE_URL_MAPS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitMap;
    }

    public static Retrofit getFileUploadClient(){
        if (retrofitFileUpload == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .readTimeout(300, TimeUnit.SECONDS)
                    .connectTimeout(300, TimeUnit.SECONDS)
                    .writeTimeout(300,TimeUnit.SECONDS)
                    .addInterceptor(chain -> {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Accept", "application/json;versions=1");
                        if (/*isUserLoggedIn()*/true) {
                            ongoing.addHeader("Authorization", " Basic Q1BBUEkwMDE6QVBJQ1BBdXRoMDAx");
                        }
                        return chain.proceed(ongoing.build());
                    })
                    ;

            retrofitFileUpload = new Retrofit.Builder()
                    .baseUrl(BASE_URL_UPLOAD)
                    .client(builder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitFileUpload;
    }

    public static IApiInterface getApiClient(){
        return getClient().create(IApiInterface.class);
    }

    public static IApiInterface getMobiSquidApiClient(){
        return getMobiSquidClient().create(IApiInterface.class);
    }
}
