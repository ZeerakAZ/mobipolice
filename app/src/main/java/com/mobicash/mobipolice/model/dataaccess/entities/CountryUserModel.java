package com.mobicash.mobipolice.model.dataaccess.entities;

import android.content.Context;
import android.support.annotation.NonNull;

import com.mobicash.mobipolice.R;


/**
 * Created by Zeera on 1/28/2018 bt ${File}
 */

public class CountryUserModel implements Comparable<CountryUserModel> {
    private String countryFlag;
    private String numberOfUsers;
    private String countryName;

    public CountryUserModel(String countryName, String numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
        this.countryName = countryName;
    }

    public void setCountryFlag(String countryFlag) {
        this.countryFlag = countryFlag;
    }

    public int getCountryFlag(Context context) {
        if(countryFlag==null)
            return R.drawable.close_icon;
        return context.getResources().getIdentifier(countryFlag, "drawable", context.getPackageName());
    }

    public String getCountryName() {
        return countryName;
    }

    public String getNumberOfUsers() {
        return numberOfUsers;
    }

    @Override
    public int compareTo(@NonNull CountryUserModel countryUserModel) {
        int i1 = Integer.parseInt(this.getNumberOfUsers());
        int i2 = Integer.parseInt(countryUserModel.getNumberOfUsers());
        return i2-i1;
    }
}
