package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.IceTipModel;

import java.util.ArrayList;


/**
 * Created by Zeera on 2/11/2018 bt ${File}
 */

public class IceTipAdapter extends RecyclerView.Adapter<IceTipAdapter.CustomViewHolder> {

    private ArrayList<IceTipModel> list;
    private Context context;

    public IceTipAdapter(ArrayList<IceTipModel> list, Context context) {
        this.list = list;
        this.context = context;

    }



    @Override
    public IceTipAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_ice_tip, parent, false);
        return new IceTipAdapter.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(IceTipAdapter.CustomViewHolder holder, int position) {
        holder.bindData(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvheader;

        private ImageView mIvArrow;
        private WebView wvTip;


        CustomViewHolder(View itemView) {
            super(itemView);

            mTvheader = itemView.findViewById(R.id.tv_header);
            mIvArrow = itemView.findViewById(R.id.iv_arrow);
            wvTip = itemView.findViewById(R.id.wv_tip);
        }


        void bindData(final IceTipModel model) {

            mTvheader.setText(model.getHeaderName());
            wvTip.getSettings().setJavaScriptEnabled(true);
            wvTip.loadDataWithBaseURL("", model.getTipHtml(), "text/html", "UTF-8", "");
            wvTip.setVisibility(model.isExpanded()? View.VISIBLE: View.GONE);
            if(model.isExpanded())
                mIvArrow.animate().rotation(180).setDuration(500);
            else
                mIvArrow.setRotation(0);
            mTvheader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    model.setExpanded(!model.isExpanded());
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}
