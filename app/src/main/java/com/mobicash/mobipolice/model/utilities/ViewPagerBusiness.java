package com.mobicash.mobipolice.model.utilities;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;


import com.mobicash.mobipolice.model.dataaccess.adapters.ViewPagerAdapter;
import com.mobicash.mobipolice.model.dataaccess.entities.PagerModel;
import com.mobicash.mobipolice.model.dataaccess.entities.WantedModel;
import com.mobicash.mobipolice.views.fragments.BadDriving;
import com.mobicash.mobipolice.views.fragments.BaseFragment;
import com.mobicash.mobipolice.views.fragments.CrimeSpyFragment;
import com.mobicash.mobipolice.views.fragments.PersonListFragment;
import com.mobicash.mobipolice.views.fragments.WantedFragment;

import java.util.ArrayList;

/**
 * Created by Zeera on 3/5/2017.
 */

public class ViewPagerBusiness {
    private TabLayout tabLayout;
    private ViewPager pager;
    private Context mContext;
    private FragmentManager fm;
    private BaseFragment mCurrentFragment;


    public enum TYPE_PAGER {
        CRIME,HOME,ASSETS,MISSING_PERSON;
    }


    private ArrayList<PagerModel> getFragments() {
        ArrayList<PagerModel> arrayList = new ArrayList<>();
//        arrayList.add(new PagerModel("Emergency", Emergency.newInstance()));
//        arrayList.add(new PagerModel("Status", Status.newInstance()));
        return arrayList;
    }

    private ArrayList<PagerModel> getFragmentsCrime() {
        ArrayList<PagerModel> arrayList = new ArrayList<>();
        arrayList.add(new PagerModel("CrimeSpy", CrimeSpyFragment.newInstance()));
        arrayList.add(new PagerModel("Wanted", WantedFragment.newInstance()));
        arrayList.add(new PagerModel("Bad Driving", new BadDriving()));
        return arrayList;
    }

    private ArrayList<PagerModel> getFragmentsMissingPerson() {
        ArrayList<PagerModel> arrayList = new ArrayList<>();
        arrayList.add(new PagerModel("My Persons", PersonListFragment.
                newInstance(PersonListFragment.TYPE_MINE)));
        arrayList.add(new PagerModel("Others", PersonListFragment.
                newInstance(PersonListFragment.TYPE_OTHERS)));
        return arrayList;
    }

    private ArrayList<PagerModel> getFragmentsAssets() {
        ArrayList<PagerModel> arrayList = new ArrayList<>();
//        arrayList.add(new PagerModel("Register", AsertRegisteer.newInstance()));
//        arrayList.add(new PagerModel("Stolen/Lost", AssertView.newInstance()));
        return arrayList;
    }

    public ViewPagerBusiness(TabLayout tabLayout, ViewPager pager,
                             Context mContext, FragmentManager fm, TYPE_PAGER type) {
        this.tabLayout = tabLayout;
        this.pager = pager;
        this.mContext = mContext;
        this.fm = fm;
        setAdapter(type );
    }
    private ViewPagerAdapter adapter;
    private void setAdapter(TYPE_PAGER type){
        ArrayList<PagerModel> data = null;
        switch (type){
            case HOME:
                data = getFragments();
                break;
            case CRIME:
                data = getFragmentsCrime();
                break;
            case ASSETS:
                data = getFragmentsAssets();
                break;
            case MISSING_PERSON:
                data = getFragmentsMissingPerson();
                break;

        }
        if(data==null)
            return;

        adapter = new ViewPagerAdapter(fm,data);

        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
        mCurrentFragment =adapter.getItem(0);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurrentFragment = adapter.getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Nullable
    public BaseFragment getmCurrentFragment() {
        return mCurrentFragment;
    }

    @Nullable
    public BaseFragment getFragmentAtPosition(int pos){
        return adapter.getItem(pos);
    }
}
