package com.mobicash.mobipolice.model.dataaccess.entities;

/**
 * Created by Zeera on 2/11/2018 bt ${File}
 */

public class IceTipModel {
    private String headerName;
    private String tipHtml;
    private boolean isExpanded;

    public IceTipModel(String headerName, String tipHtml) {
        this.headerName = headerName;
        this.tipHtml = tipHtml;
    }

    public String getHeaderName() {
        return headerName;
    }

    public String getTipHtml() {
        return tipHtml;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
