package com.mobicash.mobipolice.model.dataaccess.interfaces;

import android.view.View;

/**
 * Created by Administrator on 10/21/2016.
 */

public interface IItemClickListener {
    void onClick(View view, int pos);
}
