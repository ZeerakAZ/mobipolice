package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.utilities.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Zeera on 4/17/2017.
 */

public class ChatUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<UserModel> mData;
    private Context mContext;
    private IFavourite mFavListener;
    private IChatUserSelectListener mUserListener;

    public ChatUserAdapter(ArrayList<UserModel> mData, Context mContext, IChatUserSelectListener listener) {
        this.mData = mData;
        this.mContext = mContext;
        mUserListener = listener;
    }

    public ChatUserAdapter setmUserListener(IChatUserSelectListener mUserListener) {
        this.mUserListener = mUserListener;
        return this;
    }

    public void setmFavListener(IFavourite mFavListener) {
        this.mFavListener = mFavListener;
    }

    public void addUsers(ArrayList<UserModel> data){
        if(data!=null){
            if(mData==null)
                mData = new ArrayList<>();
            int start = mData.size();
            mData.addAll(data);
            notifyItemRangeInserted(start,data.size());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat, null);
        ChatUserAdapter.ViewHolder holder = new ChatUserAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserModel item = mData.get(position);
        ChatUserAdapter.ViewHolder holder1 = (ViewHolder) holder;
        holder1.tvUserName.setText(item.getFullname());
        if(item.getProfileurl()!=null&&!item.getProfileurl().isEmpty())
        Picasso.with(mContext).load(item.getProfileurl()).
                placeholder(R.drawable.profile).
                transform(new CircleTransform()).
                into(holder1.ivUserImage);
        holder1.ivAddMember.setVisibility(mFavListener==null?View.GONE:View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void clearData() {
        mData.clear();
        notifyDataSetChanged();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.iv_user_image)
        ImageView ivUserImage;
        @BindView(R.id.iv_member_add)
        ImageView ivAddMember;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ((RelativeLayout) tvUserName.getParent()).setOnClickListener(view -> {
                if (mUserListener != null) {
                    mUserListener.onChatUserSelected(mData.get(getAdapterPosition()));
                }
            });
        }

        @OnClick(R.id.iv_member_add)
        public void addToFavourite(){
            if (getAdapterPosition()!= RecyclerView.NO_POSITION) {
                if(mFavListener!=null)
                    mFavListener.addToFavourite(mData.get(getAdapterPosition()).getUserid());
            }
        }
    }

    public interface IFavourite{
        void addToFavourite(int userId);
    }

    public interface IChatUserSelectListener{
        void onChatUserSelected(UserModel userModel);
    }
}
