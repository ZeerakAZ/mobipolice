package com.mobicash.mobipolice.model.utilities;

import android.support.v7.widget.RecyclerView;

import com.paginate.Paginate;

/**
 * Created by Zeera on 10/4/2017 bt ${File}
 */

public class PagingUtility {
    private int mLastId = 0;
    private boolean isLoadedAll;
    private boolean isLoading;
    private IPagingInterFace mListener;
    private final int LOADING_THRESHOLD = 1;
    private final int NO_OF_ROWS_DEFAULT = 2;

    private int mNoOfRows = NO_OF_ROWS_DEFAULT;

    private Paginate.Callbacks callbacks;

    public PagingUtility(IPagingInterFace mListener) {
        this.mListener = mListener;
        clearState();
    }

    public void clearState(){
        this.mLastId = 0;
        isLoadedAll = false;
        isLoading = false;
    }

    public void registerPagindCallbacks(RecyclerView recyclerView) {
        callbacks = new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                if (mListener != null) {
                    mListener.loadMoreData();
                }
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return isLoadedAll;
            }
        };
        Paginate.with(recyclerView, callbacks)
                .setLoadingTriggerThreshold(LOADING_THRESHOLD)
                .addLoadingListItem(true)
                .build();
    }

    public void setLoadedAll(int count) {
        isLoadedAll = count < mNoOfRows;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setmLastId(int mLastId) {
        this.mLastId = mLastId;
    }

    public int getmLastId() {
        return mLastId;
    }

    public int getmNoOfRows() {
        return mNoOfRows;
    }

    public interface IPagingInterFace {
        void loadMoreData();

    }
}
