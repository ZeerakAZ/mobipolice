package com.mobicash.mobipolice.model.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Zeera on 4/22/2017.
 */

public class DateUtility {
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DISPLAY_FORMAT = "dd MMM, yyy h:mm:ss a";


    public static final String DATE_FORMAT_POST= "yyyy-MM-dd k:mm:ss";

    public static String getDateForDB(String date){
        //date = "Thu, 28 Jul 2016 03:21:44 GMT";
        date = date.replace("T"," ");
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        //
        format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            //Log.e("date",dt1.toString());
            SimpleDateFormat returnTime = new SimpleDateFormat(DATE_FORMAT);
            returnTime.setTimeZone(TimeZone.getTimeZone("UTC"));
            // Log.i("MneyooDb",returnTime.format(dt1));
            return returnTime.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT,Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(c.getTime());
    }

    public static String getGMTDate(String date){
        date = date.replace("T"," ");
        SimpleDateFormat format1 = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dt1 = null;
        try {
            dt1 = format1.parse(date);
            SimpleDateFormat returnTime = new SimpleDateFormat(DISPLAY_FORMAT,Locale.getDefault());
            return returnTime.format(dt1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getServerPostTime(String date){
        String formatInitial = "MMM dd yyyy h:mm a";
        SimpleDateFormat format1 = new SimpleDateFormat(formatInitial, Locale.getDefault());
        SimpleDateFormat returnTime = new SimpleDateFormat(DATE_FORMAT_POST,Locale.getDefault());
       // format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date dateObj = format1.parse(date);
            return returnTime.format(dateObj).replace(" ","T");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
