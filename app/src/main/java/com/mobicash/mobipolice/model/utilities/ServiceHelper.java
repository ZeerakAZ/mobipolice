package com.mobicash.mobipolice.model.utilities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Administrator on 12/1/2016.
 */

public class ServiceHelper {

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context ctx) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void stopService(Context context, Class<?> cls) {
        Intent i = new Intent(context, cls);
        context.stopService(i);
    }

    public static boolean startService(Context context, Class<?> cls, boolean isToRestart) {

        if (isMyServiceRunning(cls, context)&&isToRestart)
            stopService(context,cls);
        else if(isMyServiceRunning(cls,context))
            return true;

            Intent i = new Intent(context, cls);
            context.startService(i);
            return true;
        }


}
