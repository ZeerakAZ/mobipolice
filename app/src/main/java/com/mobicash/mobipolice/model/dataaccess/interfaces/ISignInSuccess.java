package com.mobicash.mobipolice.model.dataaccess.interfaces;

/**
 * Created by Zeera on 7/19/2017 bt ${File}
 */

public interface ISignInSuccess {
    void isProceed(boolean isTo);
}
