package com.mobicash.mobipolice.model.essentials;

import java.util.ArrayList;

/**
 * Created by Zeera on 4/5/2017.
 */

public class Constants {
    public static ArrayList<String> mBeacons = new ArrayList<>();
    public static final String APP_UUID = "ffffffff1234aaaa1a2ba1b2c3d4e5f6";

    static {
        mBeacons.add("E2C56DB5DFFB48D2B060D0F5A71096E0");
        mBeacons.add("74278BDAB64445208f0c720EAF059935");
        mBeacons.add("AB9CA9BA5BE611E7907BA6006AD3DBA0");
    }

    public static final String EXTRA_IS_FROM = "isFrom";
    public static final String EXTRA_BEACON_UUID = "beaconUUID";
    public static final String EXTRA_BEACON_MAJOR = "beaconMajor";
    public static final String EXTRA_BEACON_MINOR = "beaconMinor";
}
