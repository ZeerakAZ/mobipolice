package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobicash.mobipolice.model.dataaccess.network.ApiClient

import java.io.Serializable

/**
 * Created by Zeera on 8/6/2017 bt ${File}
 */

class MissingPersonGetModel : Serializable {
    @SerializedName("PersonId")
    @Expose
    val personId: Int? = null
    @SerializedName("PersonName")
    @Expose
    val personName: String? = null
    @SerializedName("PersonImage")
    @Expose
    private val personImage: String? = null
    @SerializedName("PersonDescription")
    @Expose
    val personDescription: String? = null
    @SerializedName("LastDisappearanceTime")
    @Expose
    val lastDisappearanceTime: String? = null
    @SerializedName("Location")
    @Expose
    val location: String? = null
    @SerializedName("ContactAt")
    @Expose
    val contactAt: String? = null
    @SerializedName("ContactName")
    @Expose
    val contactName: String? = null
    @SerializedName("ContactAddress")
    @Expose
    val contactAddress: String? = null
    @SerializedName("Status")
    @Expose
    val status: String? = null
    @SerializedName("EntHost")
    @Expose
    private val entHost: String? = null
    @SerializedName("EntHostIP")
    @Expose
    private val entHostIP: String? = null
    @SerializedName("EntIMEI")
    @Expose
    private val entIMEI: String? = null
    @SerializedName("PostByUserId")
    @Expose
    val postByUser: Int = 0
    @SerializedName("Latitude")
    @Expose
    private val latitude: String? = null
    @SerializedName("Longitude")
    @Expose
    private val longitude: String? = null

    val disapperaceDate: String
        get() = lastDisappearanceTime!!.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]

    val disapperaceTime: String
        get() = lastDisappearanceTime!!.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]

    fun getPersonImage(): String {
        return ApiClient.BASE_URL + personImage!!
    }

    fun getLatitude(): Double {

        try {
            return java.lang.Double.parseDouble(latitude)
        } catch (e: NumberFormatException) {
            e.printStackTrace()
            return 0.0
        }

    }

    fun getLongitude(): Double {
        try {
            return java.lang.Double.parseDouble(longitude)
        } catch (e: NumberFormatException) {
            e.printStackTrace()
            return 0.0
        }

    }
}

