package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonTipModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.dataaccess.enums.FragmentAnimationType;
import com.mobicash.mobipolice.model.utilities.FragmentUtility;
import com.mobicash.mobipolice.views.activities.BaseActivity;
import com.mobicash.mobipolice.views.fragments.ChatFragment;
import com.mobicash.mobipolice.views.fragments.PersonDetailFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 8/13/2017 bt ${File}
 */

public class MissingPersonTipAdapter extends RecyclerView.Adapter<MissingPersonTipAdapter.ViewHolder> {
    private ArrayList<MissingPersonTipModel> mData;
    private Context mContext;

    public MissingPersonTipAdapter(Context mContext, ArrayList<MissingPersonTipModel> mData) {
        this.mData = mData;
        this.mContext = mContext;
    }

    @Override
    public MissingPersonTipAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_missing_tip, parent, false);
        return new MissingPersonTipAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MissingPersonTipAdapter.ViewHolder holder, int position) {
        MissingPersonTipModel item = mData.get(position);
        holder.tvTip.setText(item.getTip());
        holder.tvPersonName.setText(item.getTipBy());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_tip)
        TextView tvTip;
        @BindView(R.id.tv_submit_by)
        TextView tvPersonName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvPersonName.setOnClickListener(v -> {
                MissingPersonTipModel missingPersonTipModel = mData.get(getAdapterPosition());
                UserModel userModel = new UserModel(missingPersonTipModel.getTipById()
                        ,missingPersonTipModel.getTipBy(),missingPersonTipModel.getTipByUserImage());
                FragmentUtility.withManager(((BaseActivity) mContext).getSupportFragmentManager())
                        .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                        .addToBackStack(PersonDetailFragment.TAG)
                        .addFragment(ChatFragment.newInstance(userModel,
                                ChatFragment.TYPE_ONE_TO_ONE));
            });
        }
    }
}
