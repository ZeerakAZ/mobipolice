package com.mobicash.mobipolice.model.essentials;

import android.content.Context;
import android.location.Location;
import android.support.annotation.Nullable;

import com.mobicash.mobipolice.model.dataaccess.database.DataBasehadlerClass;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel;
import com.mobicash.mobipolice.model.dataaccess.entities.MemberModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;

import java.util.ArrayList;

/**
 * Created by Zeera on 7/18/2017 bt ${File}
 */

public class SessionClass {
    private static final String KEY_USER = "keyUser";

    private static SessionClass mInstance;

    private Location mCurrentUserLocation;
    private ArrayList<MemberModel> oldMembers;
    private UserModel mUserModel;

    private DataBasehadlerClass mDbHelper;
    private CrimeModel crimeModel;

    private SessionClass() {
        //no instance
    }

    public static SessionClass getInstance() {
        if (mInstance == null) {
            mInstance = new SessionClass();
        }
        return mInstance;
        // commented by Wajahat
    }

    public Location getCurrentUserLocation() {
        return mCurrentUserLocation;
    }

    public void setCurrentUserLocation(Location mCurrentUserLocation) {
        this.mCurrentUserLocation = mCurrentUserLocation;
    }

    public boolean isUserLogin(Context context) {
        return getLoginUser(context)!=null;
    }

    public String getMobiUserName(Context context) {
        return "Dummy Name";
    }

    public int getUserId(Context context){
        UserModel loginUser = getLoginUser(context);
        if(loginUser!=null)
            return loginUser.getUserid();
        return -1;
    }



    public void setMobiSquidUserInfo(Context context, Object o) {
        // TODO: 3/17/2018 implementation is required
    }

    public void setLoginUser(Context context,UserModel model){
        SharedPrefManager.getInstance(context).saveObjectInSharedPref(model,KEY_USER);
    }

    @Nullable
    public UserModel getLoginUser(Context context){
        if(mUserModel==null){
            mUserModel = SharedPrefManager.getInstance(context).
                    getObjectFromSharedPref(KEY_USER,UserModel.class);
        }
        return mUserModel;
    }

    public DataBasehadlerClass getmDbHelper(Context context) {
        if (mDbHelper == null)
            mDbHelper = new DataBasehadlerClass(context);
        return mDbHelper;
    }


    public ArrayList<MemberModel> getOldMembers() {
        return oldMembers;
    }

    public void setOldMembers(ArrayList<MemberModel> oldMembers) {
        this.oldMembers = oldMembers;
    }

    public void setCrimeModel(CrimeModel crimeModel) {
        this.crimeModel = crimeModel;
    }

    public CrimeModel getCrimeModel() {
        return crimeModel;
    }
}
