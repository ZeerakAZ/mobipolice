package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Zeera on 5/2/2017.
 */

class CrimeTipModel {

    @SerializedName("UserId")
    @Expose
    private var userId: Int? = null
    @SerializedName("WantedId")
    @Expose
    private var wantedId: Int? = null
    @SerializedName("Tip")
    @Expose
    private var tip: String? = null
    @SerializedName("EntHost")
    @Expose
    private var entHost: String? = null
    @SerializedName("EntHostIP")
    @Expose
    private var entHostIP: String? = null
    @SerializedName("EntOperation")
    @Expose
    private var entOperation: String? = null

    fun setUserId(userId: Int?) {
        this.userId = userId
    }

    fun setWantedId(wantedId: Int?) {
        this.wantedId = wantedId
    }

    fun setTip(tip: String) {
        this.tip = tip
    }

    fun setEntHost(entHost: String) {
        this.entHost = entHost
    }

    fun setEntHostIP(entHostIP: String) {
        this.entHostIP = entHostIP
    }

    fun setEntOperation(entOperation: String) {
        this.entOperation = entOperation
    }


}
