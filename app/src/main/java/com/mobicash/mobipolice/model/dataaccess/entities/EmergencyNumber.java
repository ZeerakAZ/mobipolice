package com.mobicash.mobipolice.model.dataaccess.entities;

/**
 * Created by Zeera on 4/1/2017.
 */

public class EmergencyNumber {
    private String name;
    private String number;
    private int imageResouceId;

    public EmergencyNumber(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public EmergencyNumber(String name, String number, int imageResouceId) {
        this.name = name;
        this.number = number;
        this.imageResouceId = imageResouceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getImageResouceId() {
        return imageResouceId;
    }
}
