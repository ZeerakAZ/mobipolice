package com.mobicash.mobipolice.model.dataaccess.validations;

import android.widget.Spinner;

/**
 * Created by zeerak on 6/9/2018 bt ${File}
 */
public class SpinnerItemSelectValidation implements Validation {
    private Spinner mSpinner;
    private String mErrorMessage;


    public SpinnerItemSelectValidation(String mErrorMessage, Spinner mSpinner) {
        this.mSpinner = mSpinner;
        this.mErrorMessage = mErrorMessage;
    }

    @Override
    public boolean passes() {
        return mSpinner.getSelectedItemPosition()!=0;
    }

    @Override
    public String getMessage() {
        return mErrorMessage;
    }

    @Override
    public String getHeading() {
        return null;
    }
}
