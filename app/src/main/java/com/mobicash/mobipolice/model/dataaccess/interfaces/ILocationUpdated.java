package com.mobicash.mobipolice.model.dataaccess.interfaces;


import android.location.Location;

/**
 * Created by Zeera on 4/30/2017.
 */

public interface ILocationUpdated {
    void onUpdate(Location location);
}
