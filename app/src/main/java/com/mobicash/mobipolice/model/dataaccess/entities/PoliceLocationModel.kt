package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Created by zeerak on 5/18/2018 bt ${File}
 */
data class PoliceLocationModel(
        @SerializedName("UserId") val userId: Int,
        @SerializedName("UserName") val userName: String,
        @SerializedName("Latitude") private val _lat: String?,
        @SerializedName("Longitude") private val _lng: String?,
        @SerializedName("FullName") val fullName:String,
        @SerializedName("Mobile") val mobile:String,
        @SerializedName("UserImage")val userImage:String
) {
    val lat: Double
        get() {

            return if (_lat.isNullOrEmpty() || _lat?.toDoubleOrNull() == null) {
                0.0
            } else
                _lat.toDouble()

        }

    val lng:Double
        get() {

            return if (_lng.isNullOrEmpty() || _lng?.toDoubleOrNull() == null) {
                0.0
            } else
                _lng.toDouble()

        }

}


