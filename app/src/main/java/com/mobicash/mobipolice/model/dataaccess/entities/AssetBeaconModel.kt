package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.SerializedName

data class AssetBeaconModel(
        @SerializedName("Beacon") val beacon: BeaconModel,
        @SerializedName("Status") val status: String,
        @SerializedName("AssetId") val assetId: Int,
        @SerializedName("AssetName") val assetName: String
)