package com.mobicash.mobipolice.model.dataaccess.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ambre on 5/13/2017.
 */

public class UpdateMemberModel {

    @SerializedName("UserId")
    @Expose
    public int userid;
    @SerializedName("FriendsUserIds")
    @Expose
    public Integer[] friendsIds;

    @SerializedName("EntHost")
    @Expose
    public String entHost;
    @SerializedName("EntHostIP")
    @Expose
    public String entHostIP;
    @SerializedName("EntUserId")
    @Expose
    public Integer entUserId;
    @SerializedName("EntIMEI")
    @Expose
    public String entIMEI;
}
