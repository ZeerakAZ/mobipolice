package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AssetModel (
    @SerializedName("Id") val id: Int,
    @SerializedName("Name") val name: String,
    @SerializedName("Description") val description: String,
    @SerializedName("Brand") val brand: String,
    @SerializedName("ModelNo") val modelNo: String,
    @SerializedName("SerialNo") val serialNo: String,
    @SerializedName("PurchaseValue") val purchaseValue: String,
    @SerializedName("PurchaseDate") val purchaseDate: String,
    @SerializedName("SupplierName") val supplierName: String,
    @SerializedName("SupplierAddress") val supplierAddress: String,
    @SerializedName("InvoiceNo") val invoiceNo: String,
    @SerializedName("AssetImageURL") val assetImageURL: String,
    @SerializedName("InvoiceImageURL") val invoiceImageURL: String,
    @SerializedName("Active") val active: Boolean,
    @SerializedName("BeaconUUID") val beaconUUID: String,
    @SerializedName("BeaconMajor") val beaconMajor: Int,
    @SerializedName("BeaconMinor") val beaconMinor: Int,
    @SerializedName("Status") val status: String,
    @SerializedName("FullName") val fullName:String?,
    @SerializedName("Location") val location: String?,
    @SerializedName("Mobile") val mobile: String?,
    @SerializedName("Owner") val owner: Owner,
    @SerializedName("EntHost") val entHost: String,
    @SerializedName("EntHostIP") val entHostIP: String,
    @SerializedName("EntUserId") val entUserId: Int,
    @SerializedName("EntIMEI") val entIMEI: String
): Serializable