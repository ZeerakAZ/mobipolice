package com.mobicash.mobipolice.model.dataaccess.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.interfaces.IMessageReceived;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.model.utilities.PermissionsUtitlty;
import com.mobicash.mobipolice.model.utilities.ServiceUtitlity;
import com.mobicash.mobipolice.presenter.ChatPresenter;
import com.mobicash.mobipolice.views.activities.BaseActivity;
import com.mobicash.mobipolice.views.activities.MainActivity;
import com.mobicash.mobipolice.views.activities.SosActivity;

import java.util.Random;

/**
 * Created by Administrator on 11/29/2016.
 */

public class FirebaseMessaging extends FirebaseMessagingService implements IMessageReceived {

    private String from;
    private String message;
    public static String FROM_NOTIFICATION = "isFromNotification";
    public static final String KEY_LATITUDE = "notiLatitude";
    public static final String KEY_LONGITUDE = "notiLongitude";
    public static final String KEY_USERNAME = "notiName";
    public static final String KEY_ISINDANGER = "isIndanger";
    public static final String KEY_ISMESSAGE = "isMessage";
    public static final String KEY_IS_GROUP = "isMessage";
    public static final String KEY_SENDER_ID = "isMessage";
    public static final String KEY_USER_IMAGE = "userImagePath";
    public static final String KEY_REASON = "Reason";
    public static final String KEY_USER_ID = "SenderUserId";
    private boolean isInDanger;
    private boolean isMessage;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        /*Bundle[{google.sent_time=1495364864023, isGroupMessage=false, from=699811352865,
                google.message_id=0:1495364864030385%2fdac1e3f9fd7ecd, senderUserId=3, message=thanks}]*/

           /* Reason =, isInDanger = True, Latitude = 24.8553205,
                    SenderUserImage = https://api.mobisquid.com/images/profile/157.png,
            SenderUserName=Amir A,
             SenderUserId=15, isGroup=false, Longitude=67.0807735}*/

       /* Bundle[{google.sent_time=1495820345190, isInDanger=True, Latitude=24.9272588,
                SenderUserImage=string, from=699811352865, google.message_id=0:1495820345197467%2fdac1e3f9fd7ecd,
                SenderUserName=Zeerak Mushtaq, SenderUserId=3, isGroup=false, Longitude=67.0855142}]*/

            Log.e("", "Got push");
            this.from = remoteMessage.getFrom();

            if (remoteMessage.getData().size() > 0) {
                if (remoteMessage.getData().containsKey("Latitude")) {
                    this.isMessage = false;
                    String userName = remoteMessage.getData().get("SenderUserName");
                    double lat = Double.parseDouble(remoteMessage.getData().get("Latitude"));
                    double lng = Double.parseDouble(remoteMessage.getData().get("Longitude"));
                    String userImage = remoteMessage.getData().get("SenderUserImage");
                    isInDanger = Boolean.parseBoolean(remoteMessage.getData().get("isInDanger"));
                    String reason = remoteMessage.getData().get(KEY_REASON);
                    int senderUserId = Integer.parseInt(remoteMessage.getData().get(KEY_USER_ID));
                    String expandMessage = isInDanger ?
                            " is in danger!!" : " is safe now";
                    String displayMessage = userName + expandMessage;

                    sendNotification(displayMessage, getSOSIntent(lat, lng, userName, senderUserId,
                            reason, userImage, isInDanger));
                } else {
                    this.isMessage = true;
                    this.message = remoteMessage.getData().get("message");
                    boolean isGroup = Boolean.parseBoolean(remoteMessage.getData().get("isGroupMessage"));
                    int id = Integer.parseInt(remoteMessage.getData().get("senderUserId"));
                    message = AppUtility.getDecodedString(message);
                    sendNotification(message, getMessageIntent(isGroup, id));

                    ChatPresenter chatPresenter = new ChatPresenter(this, null);
                    chatPresenter.getAllMessageFromServer();


                }
            }


       /* if (remoteMessage.getNotification() != null) {
            this.message = remoteMessage.getNotification().getBody();
            sendNotification(message);
        }*/


        }

    public Intent getMessageIntent(boolean isGroup, int senderId) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(FROM_NOTIFICATION, true);
        intent.putExtra(KEY_ISMESSAGE, true);
        intent.putExtra(KEY_IS_GROUP, isGroup);
        intent.putExtra(KEY_SENDER_ID, senderId);
        return intent;
    }

    public Intent getSOSIntent(double lat, double lng, String name, int id,
                               String reason, String imagePath, boolean isInDanger) {
        Intent intent = new Intent(this, SosActivity.class);
        intent.putExtra(FROM_NOTIFICATION, true);
        intent.putExtra(KEY_LATITUDE, lat);
        intent.putExtra(KEY_LONGITUDE, lng);
        intent.putExtra(KEY_USERNAME, name);
        intent.putExtra(KEY_ISINDANGER, isInDanger);
        intent.putExtra(KEY_ISMESSAGE, false);
        intent.putExtra(KEY_USER_IMAGE, imagePath);
        intent.putExtra(KEY_USER_ID, id);
        intent.putExtra(KEY_REASON, reason);
        return intent;
    }


    private void sendNotification(String message, Intent intent) {
        Log.i("push", "Notification Called");
        Random randomGenerator = new Random();


        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                (int) System.currentTimeMillis() /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri path = null;
        if (isMessage) {
            path = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        } else {
            if (isInDanger) {
                path = Uri.parse("android.resource://com.mobicash.crowdpolice/" + R.raw.danger);
            } else {
                path = Uri.parse("android.resource://com.mobicash.crowdpolice/" + R.raw.safe);
            }
        }
        if(PermissionsUtitlty.isAudioPermissionAllowed(this)){
            AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager != null) {
                audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,
                        audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION),
                        0);
            }
        }

        //ServiceUtitlity.stopService(this, VoiceListeningService.class);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSmallIcon(getNotificationIcon())
                .setContentText(message)
                .setSound(path)
                .setContentTitle(getString(R.string.app_name))
                .setColor(ContextCompat.getColor(this, R.color.colorAccent))
                .setAutoCancel(true)

                .setContentIntent(pendingIntent);
        notificationBuilder.setVisibility(1);
        notificationBuilder.setPriority(Notification.PRIORITY_MAX);
        Notification notification = notificationBuilder.build();
        /*notification.sound = path;
        notification.audioStreamType = AudioManager.STREAM_ALARM;*/
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int randomInt = randomGenerator.nextInt(100);
        notificationManager.notify(randomInt,notification );
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.app_icon : R.drawable.app_icon;
    }

    private void broadCastMessagingUpdate(int userId,@BaseActivity.MESSAGE_TYPE int type){
        Intent intent= new Intent();
        intent.setAction(BaseActivity.ACTION_MESSAGE_RECEIVED);
        intent.putExtra(BaseActivity.KEY_USER_ID,userId);
        intent.putExtra(BaseActivity.KEY_MESSAGE_TYPE,type);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onMessageReceived(Integer userId) {
        broadCastMessagingUpdate(userId, BaseActivity.MESSAGE_TYPE.ONE_TO_ONE);
    }

    @Override
    public void onGroupMessageReceived(Integer groupId) {
        broadCastMessagingUpdate(groupId, BaseActivity.MESSAGE_TYPE.GROUP);

    }
}
