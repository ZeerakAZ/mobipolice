package com.mobicash.mobipolice.model.utilities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.mobicash.mobipolice.R;

import java.util.Random;

/**
 * Created by zeerak on 5/27/2018 bt ${File}
 */
public class NotificationUtility {

    private final Context mContext;

    public NotificationUtility(Context context){
        mContext = context;
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher_round : R.mipmap.ic_launcher_round;
    }

    public void sendNotification(String message, Intent intent) {
        Log.i("push", "Notification Called");
        Random randomGenerator = new Random();
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext,
                (int) System.currentTimeMillis() /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri path = Uri.parse("android.resource://com.mobicash.crowdpolice/" + R.raw.danger);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSmallIcon(getNotificationIcon())
                .setContentText(message)
                .setContentTitle(mContext.getString(R.string.app_name))
                .setColor(ContextCompat.getColor(mContext, R.color.colorAccent))
                .setAutoCancel(true)
                .setSound(path)
                .setContentIntent(pendingIntent);
        notificationBuilder.setVisibility(1);
        notificationBuilder.setPriority(Notification.PRIORITY_MAX);
        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        int randomInt = randomGenerator.nextInt(100);
        notificationManager.notify(randomInt, notificationBuilder.build());
    }
}
