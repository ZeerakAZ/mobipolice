package com.mobicash.mobipolice.model.dataaccess.entities


import com.mobicash.mobipolice.views.fragments.BaseFragment

/**
 * Created by Zeera on 3/5/2017.
 */

class PagerModel(val fragmentName: String, val fragment: BaseFragment)
