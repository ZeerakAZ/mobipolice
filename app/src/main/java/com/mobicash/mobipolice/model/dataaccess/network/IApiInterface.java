package com.mobicash.mobipolice.model.dataaccess.network;

import com.mobicash.mobipolice.model.dataaccess.entities.BeaconModel;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeModel;
import com.mobicash.mobipolice.model.dataaccess.entities.CrimeTipModel;
import com.mobicash.mobipolice.model.dataaccess.entities.FinancialLoginModel;
import com.mobicash.mobipolice.model.dataaccess.entities.MessageModel;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonModel;
import com.mobicash.mobipolice.model.dataaccess.entities.MissingPersonTipModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UpdateMemberModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UserDeviceToken;
import com.mobicash.mobipolice.model.dataaccess.entities.UserModel;
import com.mobicash.mobipolice.model.dataaccess.entities.UserRegisterModel;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Zeera on 7/3/2017 bt ${File}
 */

public interface IApiInterface {

    @GET(APIsEndPoints.GET_BREAKING_NEWS)
    Call<ResponseBody> getBreakingNews(@Query("UserType") String userType);

    @GET(APIsEndPoints.GET_ACTIVE_USER)
    Call<ResponseBody> getActiveUser(@Query("UserType") String userType);

    @GET(APIsEndPoints.SEND_SOS_TO_POLICE)
    Call<ResponseBody> sendSoSToGroup(@Query("UserId") int userId,
                                      @Query("GroupId") int groupId, @Query("Latitude") double lat,
                                      @Query("Longitude") double lng);


    @GET(APIsEndPoints.SEND_SOS_TO_FRIENDS)
    Call<ResponseBody> sendSoSToFamily(@Query("UserId") int userId, @Query("Reason") String reason,
                                       @Query("Latitude") double lat, @Query("Longitude") double lng,
                                       @Query("isInDanger") boolean isInDanger);

    @GET(APIsEndPoints.SEND_SOS_TO_POLICE_ALL)
    Call<ResponseBody> sendSoSToPoliceAll(@Query("Reason") String reason,
                                          @Query("Latitude") double lat, @Query("Longitude") double lng,
                                          @Query("isInDanger") boolean isInDanger);

    @GET(APIsEndPoints.GET_CRIMES)
    Call<ResponseBody> getReportedCrimes();

    @GET(APIsEndPoints.GET_CRIMES_BY_LOCATION)
    Call<ResponseBody> getReportedCrimes(@Query("Latitude") double lat, @Query("Longitude") double lng);

    @PUT(APIsEndPoints.INACTIVE_ACCOUNT)
    Call<ResponseBody> inActiveUser(@Path("Id") int id);

    @PUT(APIsEndPoints.LOGOFF_USER)
    Call<ResponseBody> logOffUser(@Path("Id") int id, @Query("DeviceId") String deviceId);

    @POST(APIsEndPoints.UPDATE_MEMBER)
    Call<ResponseBody> addUpdateMember(@Body UpdateMemberModel memberModel);

    @GET(APIsEndPoints.GET_MEMBERS)
    Call<ResponseBody> getMembers(@Query("Userid") int id);

    @GET(APIsEndPoints.SEARCH_FRIENDS)
    Call<ResponseBody> searchFriend(@Query("FriendName") String name);

    @POST(APIsEndPoints.UPDATE_SOS_MESSAGE)
    Call<ResponseBody> updateSosAlertMessage(@Query("userId") int userId,
                                             @Query("Message") String message);

    @GET(APIsEndPoints.GET_USER_DETAILS)
    Call<ResponseBody> getUserDetails(@Path("Id") int id);

    @GET(APIsEndPoints.NEARBY_PLACES)
    Call<ResponseBody> getNearByLocations(@Query("location") String location,
                                          @Query("radius") double radius, @Query("type")
                                                  String type, @Query("key") String key);

    @GET(APIsEndPoints.GET_DIRECTIONS)
    Call<ResponseBody> getMapsPoints(@Query("origin") String origin, @Query("destination")
            String destination, @Query("sensor") boolean isSensor, @Query("optimizeWaypoints") boolean
                                             toOptimize, @Query("alternatives") boolean AlterNativesRoutes, @Query("key") String key);

    @POST(APIsEndPoints.LOG_IN)
    @FormUrlEncoded
    Call<ResponseBody> loginUser(@Field("username") String userName, @Field("password") String password);

    @GET(APIsEndPoints.GET_GROUP_CHATS)
    Call<ResponseBody> getGroups(@Query("UserType") String userType);

    @GET(APIsEndPoints.GET_RECEIVED_MESSAGES)
    Call<ResponseBody> getMessages(@Query("UserId") int userID);

    @GET(APIsEndPoints.GET_LAST_MESSAGE)
    Call<ResponseBody> getMessages(@Query("SenderUserId") int userID,
                                   @Query("RecieverUserId") int receiverId,
                                   @Query("LastChatId") int chatId);

    @GET(APIsEndPoints.GET_ALL_USER_MESSAGES)
    Call<ResponseBody> getAllMesssage(@Query("UserId") Integer userId, @Query("LastMessageId") Integer lastMessageId);

    @GET(APIsEndPoints.GET_RECEIVED_MESSAGE_GROUPS)
    Call<ResponseBody> getAllMessageOfGroup(@Query("GroupId") int groupId, @Query("senderUserId") Integer senderId,
                                            @Query("LastChatId") Integer chatId);

    @POST(APIsEndPoints.REPORT_CRIME)
    Call<ResponseBody>
    postCrime(@Body CrimeModel crimeModel);

    @POST(APIsEndPoints.POST_MESSAGE)
    Call<ResponseBody> sendMessage(@Body MessageModel message);

    @POST(APIsEndPoints.POST_GROUP_MESSAGE)
    Call<ResponseBody> sendGroupMessage(@Body MessageModel message);

    @GET(APIsEndPoints.GET_RECENT_CHAT_USER)
    Call<ResponseBody> getRecentChats(@Query("UserId") int userId, @Query("NoOfRows") int noOfRows);

    @GET(APIsEndPoints.GET_FAV_USER)
    Call<ResponseBody> getFavouritesChats(@Query("UserId") int userId,
                                          @Query("NoOfRows") int noOfRows, @Query("LastUserId") int lastUserId);

    @GET(APIsEndPoints.SEARCH_USER)
    Call<ResponseBody> searchMembers(@Query("Name") String initials, @Query("NoOfRows") int noOfRows,
                                     @Query("LastUserId") int lastUserId, @Query("UserType") String userType);

    @POST(APIsEndPoints.POST_FAV_USER)
    @FormUrlEncoded
    Call<ResponseBody> postFavourite(@Field("UserId") int userId, @Field("FavoriteUserId") int userToFavId);

    @PUT(APIsEndPoints.UPDATE_TOKEN)
    Call<ResponseBody> updateToken(@Path("Id") int userId,
                                   @Body() UserDeviceToken value);

    @POST(APIsEndPoints.POST_USER)
    Call<ResponseBody> postUser(@Body UserModel userModel);

    @GET(APIsEndPoints.GET_CRIME_TYPES)
    Call<ResponseBody> getCrimeTypes();

    @Multipart
    @POST(APIsEndPoints.UPLOAD_FILE)
    Call<ResponseBody> uploadFile(@Part MultipartBody.Part file);

    @GET(APIsEndPoints.GET_WANTED)
    Call<ResponseBody> getWanted();

    @POST(APIsEndPoints.POST_TIP)
    Call<ResponseBody> submitTip(@Body CrimeTipModel model);

    @POST(APIsEndPoints.POST_MISSING_PERSON)
    Call<ResponseBody> postMissingPerson(@Body MissingPersonModel personModel);

    @GET(APIsEndPoints.GET_MISSING_PERSON)
    Call<ResponseBody> getMissingPersons(@Query("NoOfRows") int noOfRows,
                                         @Query("LastPersonId") int lastPersonId);

    @POST(APIsEndPoints.POST_MISSING_PERSON_TIP)
    Call<ResponseBody> postMissingPersonTip(@Body MissingPersonTipModel tip);

    @GET(APIsEndPoints.GET_MISSING_PERSON_TIP)
    Call<ResponseBody> getMissingPersonTip(@Query("PersonId") int personId);

    @POST(APIsEndPoints.MARK_FOUND)
    Call<ResponseBody> markPersonFound(@Query("PersonId") int personId);


    @POST(APIsEndPoints.UPDATE_POLICE_LOCATION)
    Call<ResponseBody> updateLocation(@Path("Id") String id,
                                      @Query("Longitude") double lat, @Query("Latitude") double lang);

    @GET(APIsEndPoints.GET_POLICE_LOCATIONS)
    Call<ResponseBody> getPoliceLocation(@Query("Latitude") double lat, @Query("Longitude") double lng);

    @GET(APIsEndPoints.GET_PROVINCE)
    Call<ResponseBody> getProvince();


    @GET(APIsEndPoints.GET_DISTRICTS)
    Call<ResponseBody> getDistricts(@Query("provinceId") int provinceId);

    @POST(APIsEndPoints.CHECK_NUMBER)
    Call<ResponseBody> checkForNumber(@Query("clientNumber") String number);

    @POST(APIsEndPoints.REGISTER_FINANCIAL_USER)
    Call<ResponseBody> registerFinancialUser (@Body UserRegisterModel userRegisterModel);

    @POST(APIsEndPoints.FINANCIAL_LOGIN)
    Call<ResponseBody> financialLogin(@Body FinancialLoginModel model,@Query("Country") String country);

    @POST(APIsEndPoints.GET_BEACON_STATUS)
    Call<ResponseBody> getBeaconStatus(@Body ArrayList<BeaconModel> beacons);
}
