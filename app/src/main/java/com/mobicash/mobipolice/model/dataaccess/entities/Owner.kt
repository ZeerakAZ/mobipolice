package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Owner(
    @SerializedName("OwnerUserId") val ownerUserId: String,
    @SerializedName("OwnerUserName") val ownerUserName: String,
    @SerializedName("OwnerFullName") val ownerFullName: String,
    @SerializedName("OwnerLocation") val ownerLocation: String,
    @SerializedName("OwnerMobile") val ownerMobile: String,
    @SerializedName("OwnerUserImage") val ownerUserImage: String,
    @SerializedName("OwnerUserCity") val ownerUserCity: String
):Serializable