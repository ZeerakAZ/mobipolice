package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.MenuItem;
import com.mobicash.mobipolice.model.essentials.SessionClass;
import com.mobicash.mobipolice.model.utilities.AppUtility;

import java.util.ArrayList;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.mViewHolder> {
    private final IMenuClickListener listener;
    private ArrayList<MenuItem> list;
    public Context context;
    private int mSelectedPos;

    public DrawerAdapter(ArrayList<MenuItem> list, Context context, IMenuClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        mSelectedPos = 0;
    }

    @Override
    public mViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.row_drawer_adapter,
                parent, false);
        return new mViewHolder(view);
    }

    @Override
    public void onBindViewHolder(mViewHolder holder, int position) {
        MenuItem item = list.get(position);
        holder.name.setText(context.getResources().getString(item.getNameId()));
        if (item.getNameId() == R.string.menu_item_log_out) {
            setVisibilityForRecycler(SessionClass.getInstance().isUserLogin(context), holder.parentView);
        } else {
            setVisibilityForRecycler(true, holder.parentView);
        }
        if (mSelectedPos == position) {
            setColorResources(holder, R.color.blue_color_app, R.color.blue_color_app, item.getIconIdActivated());
        } else {
            setColorResources(holder, R.color.black, R.color.grey, item.getIconIdDeactivated());
        }
    }

    private void setColorResources(mViewHolder holder, int textColor, int saperator_Color, int imageResId) {
        holder.divider.setBackgroundColor(ContextCompat.getColor(context, saperator_Color));
      //  holder.name.setTextColor(ContextCompat.getColor(context, textColor));
        holder.icon.setImageResource(imageResId);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class mViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View divider;
        private ImageView icon;
        private TextView name;
        private LinearLayout parent;
        private View parentView;

        mViewHolder(View itemView) {
            super(itemView);

            parentView = itemView;
            divider = itemView.findViewById(R.id.divider);
            parent = itemView.findViewById(R.id.parent);
            parent.setOnClickListener(this);
            name = itemView.findViewById(R.id.tv_naem);
            icon = itemView.findViewById(R.id.iv_icon);

        }


        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.parent) {
                MenuItem menuItem = list.get(getAdapterPosition());
                if (menuItem.isHideForLogin() && !SessionClass.getInstance().isUserLogin(context)) {
                    AppUtility.showLoginDialog(context);
                } else {
                    listener.onMenuItemClicked(menuItem.getNameId());
                    if (menuItem.getNameId() == R.string.menu_item_log_out)
                        return;
                    mSelectedPos = getAdapterPosition();
                    notifyDataSetChanged();
                }
            }
        }
    }

    public void setVisibilityForRecycler(boolean isVisible, View itemView) {
        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) itemView.getLayoutParams();
      /*  if(param==null)
            return;*/
        if (isVisible) {
            param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            param.width = LinearLayout.LayoutParams.MATCH_PARENT;
            itemView.setVisibility(View.VISIBLE);
        } else {
            itemView.setVisibility(View.GONE);
            param.height = 0;
            param.width = 0;
        }
        itemView.setLayoutParams(param);
    }


    public interface IMenuClickListener {
        void onMenuItemClicked(@StringRes int clickedItem);
    }
}
