package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.SerializedName
class UserRegisterModel {
    @SerializedName("mobile") var mobile: String?=null
    @SerializedName("firstName") var firstName: String?=null
    @SerializedName("lastName") var lastName: String?=null
    @SerializedName("pinCode") var pinCode: String?=null
    @SerializedName("password") var password: String?=null
    @SerializedName("idNo") var idNo: String?=null
    @SerializedName("gender") var gender: String?=null
    @SerializedName("maritalstatus") var maritalstatus: String?=null
    @SerializedName("language") var language: String?=null
    @SerializedName("occupation") var occupation: String?=null
    @SerializedName("dob") var dob: String?=null
    @SerializedName("email") var email: String?=null
    @SerializedName("address") var address: String?=null
    @SerializedName("nokname") var nokname: String?=null
    @SerializedName("nokmobile") var nokmobile: String?=null
    @SerializedName("nokrelation") var nokrelation: String?=null
    @SerializedName("imei") var imei: String?=null
    @SerializedName("location") var location: String?=null
    @SerializedName("idtype") var idtype: String?=null
    @SerializedName("tempresnr") var tempresnr: String?=null
    @SerializedName("idexpiration") var idexpiration: String?=null
    @SerializedName("province") var province: Int?=null
    @SerializedName("city") var city: Int?=null
    @SerializedName("faceImage") var faceImage: String?=null
    @SerializedName("idImage") var idImage: String?=null
    @SerializedName("residencephoto") var residencephoto: String?=null
    @SerializedName("UserToken") var userToken: UserDeviceToken?=null
    @SerializedName("EntHost") var entHost: String?=null
    @SerializedName("EntHostIP") var entHostIP: String?=null
}