package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.dataaccess.entities.SliderModel;
import com.mobicash.mobipolice.views.fragments.FragmentVideo;

import java.util.ArrayList;

/**
 * Created by Zeera on 8/3/2017 bt ${File}
 */

public class VideoPagerAdapter extends FragmentPagerAdapter {

   /* private String urlPaths[] = {"android.resource://com.mobicash.crowdpolice/" + R.raw.home_tips_video
            , "android.resource://com.mobicash.crowdpolice/" + R.raw.liberty_video,
            "android.resource://com.mobicash.crowdpolice/" + R.raw.women_jogging_video,
            "android.resource://com.mobicash.crowdpolice/" + R.raw.chubbs_video,
            "android.resource://com.mobicash.crowdpolice/" + R.raw.gender_based_video,
            "android.resource://com.mobicash.crowdpolice/" + R.raw.outsurance_video};*/

    private int imageRes[] = {R.drawable.banner_hobblet_1,
            R.drawable.banner_hobblet_2,
            R.drawable.banner_hobblet_3,
            R.drawable.banner_hobblet_4,
    };
    private ArrayList<FragmentVideo> fragmentVideos;


    private ArrayList<SliderModel> mSliderModels = new ArrayList<SliderModel>(){
        {
          /*  add(new SliderModel(R.raw.home_tips_video,R.drawable.home_tip_image));
            add(new SliderModel(R.raw.women_jogging_video,R.drawable.woman_jogging_image));
            add(new SliderModel(R.raw.chubbs_video,R.drawable.chubbs_image));
            add(new SliderModel(R.raw.gender_based_video,R.drawable.gender_based_image));*/
            add(new SliderModel(null,R.drawable.banner_hobblet_1));
            add(new SliderModel(null,R.drawable.banner_hobblet_2));
            add(new SliderModel(null,R.drawable.banner_hobblet_3));
            add(new SliderModel(null,R.drawable.banner_hobblet_4));
        }
    };


    public VideoPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentVideos = new ArrayList<>();
        for (SliderModel sliderModel : mSliderModels) {
            fragmentVideos.add(FragmentVideo.newInstance(sliderModel.getVideoPath(), sliderModel.getThumbImageResource()));
        }
    }

    @Override
    public int getCount() {
        return mSliderModels.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentVideos.get(position);
    }

    public void stopVideo() {
        for (FragmentVideo fragmentVideo : fragmentVideos) {
            fragmentVideo.stopVideo();
        }
    }

    public void playVideo(int pos) {
        //fragmentVideos.get(pos).playVideo();
    }
}
