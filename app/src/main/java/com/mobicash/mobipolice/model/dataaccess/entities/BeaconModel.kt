package com.mobicash.mobipolice.model.dataaccess.entities

import com.google.gson.annotations.SerializedName

data class BeaconModel(
    @SerializedName("BeaconUUID") val beaconUUID: String,
    @SerializedName("BeaconMinor") val beaconMinor: Int,
    @SerializedName("BeaconMajor") val beaconMajor: Int
)