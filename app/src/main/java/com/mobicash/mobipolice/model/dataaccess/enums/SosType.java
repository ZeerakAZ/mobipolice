package com.mobicash.mobipolice.model.dataaccess.enums;

import android.content.Context;

import com.mobicash.mobipolice.model.essentials.SharedPrefManager;


/**
 * Created by Zeera on 6/25/2017 bt ${File}
 */

public enum SosType {

    SOS_FAMILY(101, "sosfamily"),
    SOS_FACEBOOK(102, "sosfacebook"),
    SOS_TWITTER(103, "sostwitter"),
    SOS_SMS(104, "sossms"),
    SOS_POLICE(105, "sosPolice"),
    SOS_POLICING(106, "sosPolicing"),
    SOS_EMAIL(107,"sosemail");


    private final int id;
    private final String key;

    SosType(int id, String key) {
        this.id = id;
        this.key = key;
    }

    public int getValue() {
        return id;
    }

    public boolean getCheck(Context context) {
        Boolean checked = SharedPrefManager.getInstance(context).getBooleanByKey(key);
        return checked==null?false:checked;
    }

    public void setCheck(Context context, boolean isChecked) {
        SharedPrefManager.getInstance(context).setBooleanForKey(isChecked, key);
    }


}
