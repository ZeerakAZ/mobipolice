package com.mobicash.mobipolice.model.dataaccess.validations;

/**
 * Created by Administrator on 5/12/2017.
 */

public interface Validation {
    boolean passes();
    String getMessage();
    String getHeading();
}
