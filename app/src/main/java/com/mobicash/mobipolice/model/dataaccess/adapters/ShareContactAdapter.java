package com.mobicash.mobipolice.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.mobicash.mobipolice.R;
import com.mobicash.mobipolice.model.utilities.AppUtility;
import com.mobicash.mobipolice.views.dialogs.ShareContactListDialog;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ambre on 6/4/2017.
 */

public class ShareContactAdapter extends RecyclerView.Adapter<ShareContactAdapter.CustomViewHolder> {

    private HashMap<String, ArrayList<String>> list;
    private Context context;

    public ShareContactAdapter(HashMap<String, ArrayList<String>> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_share_list, parent, false);
        CustomViewHolder holder = new CustomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        String keyName = (String) list.keySet().toArray()[position];
        holder.contactName.setText(keyName);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView contactName;
        private ImageView contactPicture;
        private Button invite;

        public CustomViewHolder(View itemView) {
            super(itemView);
            contactName = itemView.findViewById(R.id.contactname);
            contactPicture = itemView.findViewById(R.id.iv_user_image);
            invite = itemView.findViewById(R.id.invite);
            invite.setOnClickListener(v -> {
                String keyName = (String) list.keySet().toArray()[getAdapterPosition()];
                if (list.get(keyName).size() == 1) {
                    AppUtility.startSmsScreen(list.get(keyName).get(0), context);
                } else
                    new ShareContactListDialog(context, list.get(keyName)).show();
            });
        }
    }
}
